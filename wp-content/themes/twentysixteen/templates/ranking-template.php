<?php
/*
Template Name: Ranking Template Page
*/

get_header();
?>
     <?php if ( have_posts() ) : ?>
          <?php
          // Start the loop.
          while ( have_posts() ) : the_post(); ?>
               <!-- Page Title
               ============================================= -->
               <section id="page-title" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets_temp/images/footer.jpg)">
                    <div class="cover">
                         <div class="container clearfix">
                              <h1><?php echo get_the_title(); ?></h1>
                              <ol class="breadcrumb">
                                   <li><a href="<?php echo esc_url( home_url( '/' ) ); ?>">Home</a></li>
                                   <li class="active"><?php echo get_the_title(); ?></li>
                              </ol>
                         </div>
                    </div>
               </section><!-- #page-title end -->

               <!-- Content
               ============================================= -->
               <div class="content-wrap">
                    <div class="container clearfix">
                         <div class="clear"></div>

                         <!-- Portfolio Items
                         ============================================= -->
                         <div id="portfolio" class="portfolio grid-container clearfix">

                              <?php get_template_part( 'template-parts/blocks/ranking', 'list' ); ?>
                              
                         </div><!-- #portfolio end -->

                    </div>


               </div><!-- #wrapper end -->

          <?php
          // End the loop.
          endwhile;
          ?>
     <?php endif; ?>
<?php get_footer(); ?>