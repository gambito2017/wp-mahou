<?php
/*
Template Name: News Template Page
*/

get_header();
?>
     <?php if ( have_posts() ) : ?>
          <?php
          // Start the loop.
          while ( have_posts() ) : the_post(); ?>

               <div class="content">
                    

                    <div class="container">
                         <?php
                              if( isset($_POST['sendEmail']) ):
                                   echo '<h3 style="color:green;">'.$_POST['sendEmail'].'</h3>';
                              endif;
                         ?>
                    
                         <?php get_template_part( 'template-parts/blocks/block', 'last-news' ); ?>
                    
                         
                         <?php //get_template_part( 'template-parts/blocks/block', 'sponsors-big' ); ?>
                         <?php get_template_part( 'template-parts/blocks/block', 'sponsors' ); ?>

                         <div class="clear"></div>
                         
                         <?php //get_template_part( 'template-parts/blocks/block', 'contact' ); ?>

                         <div class="clear"></div>
                    </div>
               </div>
               <script type="text/javascript">

                    var tpj=jQuery;
                    tpj.noConflict();

                    tpj(document).ready(function() {

                         var apiRevoSlider = tpj('.tp-banner').show().revolution(
                         {
                              sliderType:"standard",
                              jsFileLocation:"<?php echo get_template_directory_uri(); ?>/assets/include/rs-plugin/js/",
                              sliderLayout:"fullwidth",
                              dottedOverlay:"none",
                              delay:9000,
                              navigation: {},
                              responsiveLevels:[1200,992,768,480,320],
                              gridwidth:1140,
                              gridheight:500,
                              lazyType:"none",
                              shadow:0,
                              spinner:"off",
                              autoHeight:"off",
                              disableProgressBar:"on",
                              hideThumbsOnMobile:"off",
                              hideSliderAtLimit:0,
                              hideCaptionAtLimit:0,
                              hideAllCaptionAtLilmit:0,
                              debugMode:false,
                              fallbacks: {
                                   simplifyAll:"off",
                                   disableFocusListener:false,
                              },
                              navigation: {
                                   keyboardNavigation:"off",
                                   keyboard_direction: "horizontal",
                                   mouseScrollNavigation:"off",
                                   onHoverStop:"off",
                                   touch:{
                                        touchenabled:"on",
                                        swipe_threshold: 75,
                                        swipe_min_touches: 1,
                                        swipe_direction: "horizontal",
                                        drag_block_vertical: false
                                   },
                                   arrows: {
                                        style: "ares",
                                        enable: true,
                                        hide_onmobile: false,
                                        hide_onleave: false,
                                        tmp: '<div class="tp-title-wrap">  <span class="tp-arr-titleholder">{{title}}</span> </div>',
                                        left: {
                                             h_align: "left",
                                             v_align: "center",
                                             h_offset: 10,
                                             v_offset: 0
                                        },
                                        right: {
                                             h_align: "right",
                                             v_align: "center",
                                             h_offset: 10,
                                             v_offset: 0
                                        }
                                   }
                              }
                         });

                         apiRevoSlider.bind("revolution.slide.onloaded",function (e) {
                              SEMICOLON.slider.sliderParallaxDimensions();
                         });

                    }); //ready

               </script>
          <?php
          // End the loop.
          endwhile;
          ?>
     <?php endif; ?>

<?php get_footer(); ?>