<?php
/*
Template Name: Home Template Page
*/

get_header();
?>
     <div class="content-wrap">

     	<div class="container">
               <?php
                    if( isset($_POST['sendEmail']) ):
                         echo '<h3 style="color:green;">'.$_POST['sendEmail'].'</h3>';
                    endif;
               ?>
                         
               <?php get_template_part( 'template-parts/blocks/block', 'last-news' ); ?>
               <div class="clear"></div>

               <div class="divider divider-center"><i class="icon-circle-blank"></i></div>

               <?php get_template_part( 'template-parts/blocks/block', 'circuitos' ); ?>
               <div class="clear"></div>
               
          	<?php get_template_part( 'template-parts/blocks/block', 'other-news' ); ?>
               <div class="clear"></div>
               
               <a href="https://solandecabras.es/compromiso/sociedad/gotas-de-solidaridad/#.eBD4YR8174D4vFV" target="_new"><img src="<?php echo get_template_directory_uri(); ?>/assets_temp/images/banner-home.png" alt="Golpe Solidario" class="aligncenter"></a>
               <h3 class="center bottommargin-lg">Da tu golpe más solidario contra el cáncer de mama <b style="color:#e08541 !important;">#gotasdesolidaridad</b></h3>

               <div class="clear"></div>
               <?php //get_template_part( 'template-parts/blocks/block', 'ranking' ); ?>

               <?php if( get_option('home_video_url') ) get_template_part( 'template-parts/blocks/block', 'video' ); ?>
               <div class="clear"></div>

               <div class="divider divider-center"><i class="icon-circle-blank"></i></div>
               <?php get_template_part( 'template-parts/blocks/block', 'contact' ); ?>
               <div class="clear"></div>
               <?php //get_template_part( 'template-parts/blocks/block', 'sponsors-big' ); ?>
               <?php get_template_part( 'template-parts/blocks/block', 'sponsors' ); ?>
               
     		<div class="clear"></div>
     	</div>
     </div>
     <script type="text/javascript">

          var tpj=jQuery;
          tpj.noConflict();

          tpj(document).ready(function() {

               var apiRevoSlider = tpj('.tp-banner').show().revolution(
               {
                    sliderType:"standard",
                    jsFileLocation:"<?php echo get_template_directory_uri(); ?>/assets/include/rs-plugin/js/",
                    sliderLayout:"fullwidth",
                    dottedOverlay:"none",
                    delay:9000,
                    navigation: {},
                    responsiveLevels:[1200,992,768,480,320],
                    gridwidth:1140,
                    gridheight:500,
                    lazyType:"none",
                    shadow:0,
                    spinner:"off",
                    autoHeight:"off",
                    disableProgressBar:"on",
                    hideThumbsOnMobile:"off",
                    hideSliderAtLimit:0,
                    hideCaptionAtLimit:0,
                    hideAllCaptionAtLilmit:0,
                    debugMode:false,
                    fallbacks: {
                         simplifyAll:"off",
                         disableFocusListener:false,
                    },
                    navigation: {
                         keyboardNavigation:"off",
                         keyboard_direction: "horizontal",
                         mouseScrollNavigation:"off",
                         onHoverStop:"off",
                         touch:{
                              touchenabled:"on",
                              swipe_threshold: 75,
                              swipe_min_touches: 1,
                              swipe_direction: "horizontal",
                              drag_block_vertical: false
                         },
                         arrows: {
                              style: "ares",
                              enable: false,
                              hide_onmobile: false,
                              hide_onleave: false,
                              tmp: '<div class="tp-title-wrap">  <span class="tp-arr-titleholder">{{title}}</span> </div>',
                              left: {
                                   h_align: "left",
                                   v_align: "center",
                                   h_offset: 10,
                                   v_offset: 0
                              },
                              right: {
                                   h_align: "right",
                                   v_align: "center",
                                   h_offset: 10,
                                   v_offset: 0
                              }
                         }
                    }
               });

               apiRevoSlider.bind("revolution.slide.onloaded",function (e) {
                    SEMICOLON.slider.sliderParallaxDimensions();
               });

          }); //ready

     </script>
<?php get_footer(); ?>