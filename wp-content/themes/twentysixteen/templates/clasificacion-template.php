<?php
/*
Template Name: Clasificacion Template Page
*/

get_header();
?>
     <?php if ( have_posts() ) : ?>
          <?php
          // Start the loop.
          while ( have_posts() ) : the_post(); ?>
               <!-- Page Title
               ============================================= -->
               <section id="page-title" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets_temp/images/footer.jpg)">
                    <div class="cover">
                         <div class="container clearfix">

                              <?php
                              $type = get_query_var('type');

                              if( !empty($type) ):
                                   $tournament = get_page_by_path( $type, OBJECT, 'torneo' );

                                   $title_torneo = '';
                                   if( isset($tournament->ID) ): 
                                        $title_torneo = ' - '.get_the_title($tournament->ID);
                                   endif;
                                   ?>
                                   <h1><?php echo get_the_title(); ?><?php echo $title_torneo; ?></h1>
                                   <?php
                                   if( isset($tournament->ID) ): 
                                        $fecha_torneo = get_field('fecha_torneo', $tournament->ID);
                                        echo '<h3 style="color:white;">'.$fecha_torneo.'</h3>';
                                   endif;
                              endif;
                              ?>
                              <ol class="breadcrumb">
                                   <li><a href="<?php echo esc_url( home_url( '/' ) ); ?>">Home</a></li>
                                   <li class="active" style="color:white;"><?php echo get_the_title(); ?></li>
                              </ol>
                         </div>
                    </div>
               </section><!-- #page-title end -->

               <!-- Content
               ============================================= -->
               <section id="content">
                    <div class="content-wrap">
                         <div class="container clearfix clasification">

                              <?php 
                              if( empty($type) ):
                                   get_template_part( 'template-parts/sections/clasification', 'page' );
                              else:
                                   get_template_part( 'template-parts/sections/clasification', 'resultado-page' );
                              endif;
                              ?>

                              <?php get_template_part( 'template-parts/blocks/block', 'sponsors' ); ?>
               
                              <div class="clear"></div>

                         </div>


                    </div><!-- #wrapper end -->
               </section><!-- #content end -->

          <?php
          // End the loop.
          endwhile;
          ?>

          <script type="text/javascript">
               var tpj=jQuery;
               tpj.noConflict();

               tpj(document).ready(function() {
                    tpj('.show-cat').on( "click", function() {
                         var cat = tpj(this).data("cat");
                         tpj(".cat-clasification").each(function( index ) {
                           tpj(this).hide('slow');
                           if( tpj(this).data("cat") == cat ){
                              tpj(this).show('slow');
                           }
                         });
                    })
               }); //ready

          </script>
     <?php endif; ?>
<?php get_footer(); ?>