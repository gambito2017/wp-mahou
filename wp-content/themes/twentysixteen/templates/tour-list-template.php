<?php
/*
Template Name: Tournament List Template Page
*/

get_header();
?>
     <?php if ( have_posts() ) : ?>
          <?php
          // Start the loop.
          while ( have_posts() ) : the_post(); ?>

               <div class="content-wrap topmargin-sm">
                    <?php get_template_part( 'template-parts/blocks/block', 'calendar' ); ?>
                    

                    <div class="container clearfix">

                         <?php get_template_part( 'template-parts/blocks/block', 'next-tournaments' ); ?>
                         
                         <?php get_template_part( 'template-parts/blocks/block', 'sponsors' ); ?>
               
                         <div class="clear"></div>
                    </div>

               </div>
          
               <script type="text/javascript">

                    var cal = $( '#calendar' ).calendario( {
                              onDayClick : function( $el, $contentEl, dateProperties ) {

                                   for( var key in dateProperties ) {
                                        console.log( key + ' = ' + dateProperties[ key ] );
                                   }

                              },
                              caldata : canvasEvents
                         } ),
                         $month = $( '#calendar-month' ).html( cal.getMonthName() ),
                         $year = $( '#calendar-year' ).html( cal.getYear() );

                    $( '#calendar-next' ).on( 'click', function() {
                         cal.gotoNextMonth( updateMonthYear );
                    } );
                    $( '#calendar-prev' ).on( 'click', function() {
                         cal.gotoPreviousMonth( updateMonthYear );
                    } );
                    $( '#calendar-current' ).on( 'click', function() {
                         cal.gotoNow( updateMonthYear );
                    } );

                    function updateMonthYear() {
                         $month.html( cal.getMonthName() );
                         $year.html( cal.getYear() );
                    };

               </script>
          <?php
          // End the loop.
          endwhile;
          ?>
     <?php endif; ?>

<?php get_footer(); ?>