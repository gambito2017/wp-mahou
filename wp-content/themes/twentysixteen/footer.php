<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

		</section><!-- #content end -->
		
		<footer id="footer" class="dark" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets_temp/images/footer.jpg)">
			<div class="cover">
				<!-- Copyrights
				============================================= -->
				<div id="copyrights">
					<div class="container clearfix">
						<div class="col_one_fourth nobottommargin">
							<div class="widget noborder notoppadding">

								<a target="_blank" href="https://www.facebook.com/mahou.es" class="social-icon si-small si-dark si-facebook">
									<i class="icon-facebook"></i>
									<i class="icon-facebook"></i>
								</a>

								<a target="_blank" href="https://twitter.com/mahou_es" class="social-icon si-small si-dark si-twitter">
									<i class="icon-twitter"></i>
									<i class="icon-twitter"></i>
								</a>

								<a target="_blank" href="https://www.instagram.com/mahou_es/" class="social-icon si-small si-dark si-instagram">
									<i class="icon-instagram"></i>
									<i class="icon-instagram"></i>
								</a>

								<a target="_blank" href="https://www.youtube.com/user/MahouTV" class="social-icon si-small si-dark si-youtube">
									<i class="icon-youtube"></i>
									<i class="icon-youtube"></i>
								</a>
							</div>
						</div>
						<div class="col_three_fourth nobottommargin col_last">
							<div class="col_one_third center nobottommargin logo-gambito">Organizado por <a href="http://www.gambitogolf.es/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets_temp/images/gambito-blanco.png" alt="gambitogolf"></a></div>
							<div class="col_one_third center nobottommargin">
								<?php
								$contact_emails = explode(',',get_option('contact_email'));
								$field_emails = '';
								foreach( $contact_emails as $emails ):
									$field_emails .= '<a style="color:#fff;" href="mailto:'.$emails.'">'.$emails.'</a> ';
								endforeach;
								?>

								<address>
									<strong>Oficinas</strong><br>
									C/ Mallorca 230<br>
									08008 Barcelona<br>
								</address>
							</div>
							<div class="col_one_third nomargin center">
								<p><strong>Teléfono:</strong> <?php echo get_option('contact_phone');?><br>
								<strong>Email:</strong> <?php echo $field_emails; ?></p>
							</div>
						</div>
						
						<div class="clear"></div>
						<div class="center">
							Copyrights &copy; <?php echo date('Y');?> All Rights Reserved by Mahou San Miguel Golf Club<br>
						</div>
					</div>
				</div><!-- #copyrights end -->
			</div>
		</footer><!-- #footer end -->
		
	</div><!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>

<?php wp_footer(); ?>
</body>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-111506408-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-111506408-1');
</script>

</html>
