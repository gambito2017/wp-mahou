<?php

/**
 * Twenty Sixteen functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @link https://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * {@link https://codex.wordpress.org/Plugin_API}
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */



/**
 * Twenty Sixteen only works in WordPress 4.4 or later.
 */

if ( version_compare( $GLOBALS['wp_version'], '4.4-alpha', '<' ) ) {

	require get_template_directory() . '/inc/back-compat.php';

}



if ( ! function_exists( 'twentysixteen_setup' ) ) :

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 *
 * Create your own twentysixteen_setup() function to override in a child theme.
 *
 * @since Twenty Sixteen 1.0
 */

function twentysixteen_setup() {

	/*

	 * Make theme available for translation.

	 * Translations can be filed in the /languages/ directory.

	 * If you're building a theme based on Twenty Sixteen, use a find and replace

	 * to change 'twentysixteen' to the name of your theme in all the template files

	 */

	load_theme_textdomain( 'twentysixteen', get_template_directory() . '/languages' );



	// Add default posts and comments RSS feed links to head.

	add_theme_support( 'automatic-feed-links' );



	/*

	 * Let WordPress manage the document title.

	 * By adding theme support, we declare that this theme does not use a

	 * hard-coded <title> tag in the document head, and expect WordPress to

	 * provide it for us.

	 */

	add_theme_support( 'title-tag' );



	/*

	 * Enable support for Post Thumbnails on posts and pages.

	 *

	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails

	 */

	add_theme_support( 'post-thumbnails' );

	set_post_thumbnail_size( 1200, 9999 );



	add_image_size( 'thumbnail', 400, 300, true);

	add_image_size( 'news', 380, 250, true );

	add_image_size( 'news_double', 760, 294, true );

	add_image_size( 'news_vertical', 380, 552, true );

	add_image_size( 'news_thumbnail', 400, 300, true);

	add_image_size( 'news_mainpage', 1200, 500, true);

	add_image_size( 'thumb_cartel', 400, 566, true );

	add_image_size( 'thumb_full', 860, 1216, true );

	add_image_size( 'background_tournament', 1200, 400, true );

	add_image_size( 'ad_banner', 720, 90, true );

	add_image_size( 'big_sponsor', 400, 300, true );

	add_image_size( 'little_sponsor', 320, 240, true );

	add_image_size( 'ranking', 440, 586, true );

	add_image_size( 'slider', 1920, 600, true );

	add_image_size( 'admin_thumb', 64, 64, true );

	add_image_size( 'player_thumb', 256, 256, true );

	add_image_size( 'player_detail', 1000, 1000, true );



	// This theme uses wp_nav_menu() in two locations.

	register_nav_menus( array(

		'primary' => __( 'Primary Menu', 'twentysixteen' ),

		'social'  => __( 'Social Links Menu', 'twentysixteen' ),

	) );



	/*

	 * Switch default core markup for search form, comment form, and comments

	 * to output valid HTML5.

	 */

	add_theme_support( 'html5', array(

		'search-form',

		'comment-form',

		'comment-list',

		'gallery',

		'caption',

	) );



	/*

	 * Enable support for Post Formats.

	 *

	 * See: https://codex.wordpress.org/Post_Formats

	 */

	add_theme_support( 'post-formats', array(

		'aside',

		'image',

		'video',

		'quote',

		'link',

		'gallery',

		'status',

		'audio',

		'chat',

	) );



	/*

	 * This theme styles the visual editor to resemble the theme style,

	 * specifically font, colors, icons, and column width.

	 */

	add_editor_style( array( 'css/editor-style.css', twentysixteen_fonts_url() ) );

}

endif; // twentysixteen_setup

add_action( 'after_setup_theme', 'twentysixteen_setup' );



/**
 * Sets the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 *
 * @since Twenty Sixteen 1.0
 */

function twentysixteen_content_width() {

	$GLOBALS['content_width'] = apply_filters( 'twentysixteen_content_width', 840 );

}

add_action( 'after_setup_theme', 'twentysixteen_content_width', 0 );



/**
 * Registers a widget area.
 *
 * @link https://developer.wordpress.org/reference/functions/register_sidebar/
 *
 * @since Twenty Sixteen 1.0
 */

function twentysixteen_widgets_init() {

	register_sidebar( array(

		'name'          => __( 'Sidebar', 'twentysixteen' ),

		'id'            => 'sidebar-1',

		'description'   => __( 'Add widgets here to appear in your sidebar.', 'twentysixteen' ),

		'before_widget' => '<section id="%1$s" class="widget %2$s">',

		'after_widget'  => '</section>',

		'before_title'  => '<h2 class="widget-title">',

		'after_title'   => '</h2>',

	) );



	register_sidebar( array(

		'name'          => __( 'Content Bottom 1', 'twentysixteen' ),

		'id'            => 'sidebar-2',

		'description'   => __( 'Appears at the bottom of the content on posts and pages.', 'twentysixteen' ),

		'before_widget' => '<section id="%1$s" class="widget %2$s">',

		'after_widget'  => '</section>',

		'before_title'  => '<h2 class="widget-title">',

		'after_title'   => '</h2>',

	) );



	register_sidebar( array(

		'name'          => __( 'Content Bottom 2', 'twentysixteen' ),

		'id'            => 'sidebar-3',

		'description'   => __( 'Appears at the bottom of the content on posts and pages.', 'twentysixteen' ),

		'before_widget' => '<section id="%1$s" class="widget %2$s">',

		'after_widget'  => '</section>',

		'before_title'  => '<h2 class="widget-title">',

		'after_title'   => '</h2>',

	) );

}

add_action( 'widgets_init', 'twentysixteen_widgets_init' );



if ( ! function_exists( 'twentysixteen_fonts_url' ) ) :

/**
 * Register Google fonts for Twenty Sixteen.
 *
 * Create your own twentysixteen_fonts_url() function to override in a child theme.
 *
 * @since Twenty Sixteen 1.0
 *
 * @return string Google fonts URL for the theme.
 */

function twentysixteen_fonts_url() {

	$fonts_url = '';

	$fonts     = array();

	$subsets   = 'latin,latin-ext';



	/* translators: If there are characters in your language that are not supported by Merriweather, translate this to 'off'. Do not translate into your own language. */

	if ( 'off' !== _x( 'on', 'Lato font: on or off', 'twentysixteen' ) ) {

		$fonts[] = 'Lato:300,400,400italic,600,700';

	}



	/* translators: If there are characters in your language that are not supported by Montserrat, translate this to 'off'. Do not translate into your own language. */

	if ( 'off' !== _x( 'on', 'Raleway font: on or off', 'twentysixteen' ) ) {

		$fonts[] = 'Raleway:300,400,500,600,700';

	}



	/* translators: If there are characters in your language that are not supported by Inconsolata, translate this to 'off'. Do not translate into your own language. */

	if ( 'off' !== _x( 'on', 'Crete+Round font: on or off', 'twentysixteen' ) ) {

		$fonts[] = 'Crete+Round:400italic';

	}



	if ( $fonts ) {

		$fonts_url = add_query_arg( array(

			'family' => urlencode( implode( '|', $fonts ) ),

			'subset' => urlencode( $subsets ),

		), 'https://fonts.googleapis.com/css' );

	}



	return $fonts_url;

}

endif;



/**
 * Handles JavaScript detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since Twenty Sixteen 1.0
 */

function twentysixteen_javascript_detection() {

	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";

}

add_action( 'wp_head', 'twentysixteen_javascript_detection', 0 );



/**
 * Enqueues scripts and styles.
 *
 * @since Twenty Sixteen 1.0
 */

function twentysixteen_scripts() {

	// Add custom fonts, used in the main stylesheet.

	wp_enqueue_style( 'gambito-fonts', twentysixteen_fonts_url(), array(), null );



	// Add Genericons, used in the main stylesheet.

	wp_enqueue_style( 'bootstrap', get_template_directory_uri(). '/assets/css/bootstrap.css' , false, null, 'all' );

	wp_enqueue_style( 'swiper', get_template_directory_uri(). '/assets/css/swiper.css' , false, null, 'all' );

	wp_enqueue_style( 'dark', get_template_directory_uri(). '/assets/css/dark.css' , false, null, 'all' );

	wp_enqueue_style( 'font-icons', get_template_directory_uri(). '/assets/css/font-icons.css' , false, null, 'all' );

	wp_enqueue_style( 'animate', get_template_directory_uri(). '/assets/css/animate.css' , false, null, 'all' );

	wp_enqueue_style( 'magnific-popup', get_template_directory_uri(). '/assets/css/magnific-popup.css' , false, null, 'all' );

	if( is_page_template('templates/tour-list-template.php') ){

		wp_enqueue_style( 'gambito-calendar', get_template_directory_uri(). '/assets/css/calendar.css' , false, null, 'all' );

	}

	//wp_enqueue_style( 'responsive', get_template_directory_uri(). '/assets/css/responsive.css' , false, null, 'all' );



	wp_enqueue_style( 'rsplugin-settings', get_template_directory_uri(). '/assets/include/rs-plugin/css/settings.css' , false, null, 'all' );

	wp_enqueue_style( 'rsplugin-layers', get_template_directory_uri(). '/assets/include/rs-plugin/css/layers.css' , false, null, 'all' );

	wp_enqueue_style( 'rsplugin-navigation', get_template_directory_uri(). '/assets/include/rs-plugin/css/navigation.css' , false, null, 'all' );



	if( is_front_page() ){

		wp_enqueue_style( 'gambito-front-page', get_template_directory_uri(). '/assets/css/custom/front-page.css' , false, null, 'all' );

	}



	



	// Theme stylesheet.

	wp_enqueue_style( 'gambito-style', get_stylesheet_uri(), false, null );



	// Load JS

	wp_enqueue_script( 'jquery', get_template_directory_uri().'/assets/js/jquery.js', array('jquery'), null, false);

	wp_enqueue_script( 'plugins', get_template_directory_uri().'/assets/js/plugins.js', array('jquery'), null, false);

	if( is_page_template('templates/tour-list-template.php') ){

		wp_enqueue_script( 'calendario', get_template_directory_uri().'/assets/js/jquery.calendario.js', array('jquery'), null, false);

	}

	wp_enqueue_script( 'functions', get_template_directory_uri().'/assets/js/functions.js', array('jquery'), null, false);

	

	$translation_array = array( 'templateUrl' => get_stylesheet_directory_uri() );

	//after wp_enqueue_script

	wp_localize_script( 'functions', 'directory_uri', $translation_array );



	if( is_singular('torneo') ){

		wp_deregister_script('google-maps');

		wp_register_script('google-maps', "//maps.googleapis.com/maps/api/js?v=3&key=AIzaSyDmSzNb2b29J8HJqvFaeqwoX92q7m1LoXA", false, true);

		wp_enqueue_script('google-maps');

		wp_enqueue_script( 'gambito-gmap', get_template_directory_uri().'/assets/js/jquery.gmap.js', array('jquery'), null, false);

	}



	if( is_page_template('templates/multimedia-template.php') ){

		wp_enqueue_script( 'bs-datatable', get_template_directory_uri().'/assets/js/bs-datatable.js', array('jquery'), null, false);

	}



	wp_enqueue_script( 'rsplugin-tools', get_template_directory_uri().'/assets/include/rs-plugin/js/jquery.themepunch.tools.min.js', array('jquery'), null, false);

	wp_enqueue_script( 'rsplugin-revolution', get_template_directory_uri().'/assets/include/rs-plugin/js/jquery.themepunch.revolution.min.js', array('jquery'), null, false);

	

	wp_enqueue_script( 'rsplugin-extension-video', get_template_directory_uri().'/assets/include/rs-plugin/js/extensions/revolution.extension.video.min.js', array('jquery'), null, false);

	wp_enqueue_script( 'rsplugin-extension-slideanims', get_template_directory_uri().'/assets/include/rs-plugin/js/extensions/revolution.extension.slideanims.min.js', array('jquery'), null, false);

	wp_enqueue_script( 'rsplugin-extension-actions', get_template_directory_uri().'/assets/include/rs-plugin/js/extensions/revolution.extension.actions.min.js', array('jquery'), null, false);

	wp_enqueue_script( 'rsplugin-extension-layeranimation', get_template_directory_uri().'/assets/include/rs-plugin/js/extensions/revolution.extension.layeranimation.min.js', array('jquery'), null, false);

	wp_enqueue_script( 'rsplugin-extension-kenburn', get_template_directory_uri().'/assets/include/rs-plugin/js/extensions/revolution.extension.kenburn.min.js', array('jquery'), null, false);

	wp_enqueue_script( 'rsplugin-extension-navigation', get_template_directory_uri().'/assets/include/rs-plugin/js/extensions/revolution.extension.navigation.min.js', array('jquery'), null, false);

	wp_enqueue_script( 'rsplugin-extension-migration', get_template_directory_uri().'/assets/include/rs-plugin/js/extensions/revolution.extension.migration.min.js', array('jquery'), null, false);

	wp_enqueue_script( 'rsplugin-extension-parallax', get_template_directory_uri().'/assets/include/rs-plugin/js/extensions/revolution.extension.parallax.min.js', array('jquery'), null, false);



}

add_action( 'wp_enqueue_scripts', 'twentysixteen_scripts' );



function my_acf_init() {

	

	acf_update_setting('google_api_key', 'AIzaSyDGe3bEwX4N2hsox5l1FWmcscvoLv8gqw8');

}



add_action('acf/init', 'my_acf_init');



/**
 * Adds custom classes to the array of body classes.
 *
 * @since Twenty Sixteen 1.0
 *
 * @param array $classes Classes for the body element.
 * @return array (Maybe) filtered body classes.
 */

function twentysixteen_body_classes( $classes ) {

	// Adds a class of custom-background-image to sites with a custom background image.

	if ( get_background_image() ) {

		$classes[] = 'custom-background-image';

	}



	// Adds a class of group-blog to sites with more than 1 published author.

	if ( is_multi_author() ) {

		$classes[] = 'group-blog';

	}



	// Adds a class of no-sidebar to sites without active sidebar.

	if ( ! is_active_sidebar( 'sidebar-1' ) ) {

		$classes[] = 'no-sidebar';

	}



	// Adds a class of hfeed to non-singular pages.

	if ( ! is_singular() ) {

		$classes[] = 'hfeed';

	}



	return $classes;

}

add_filter( 'body_class', 'twentysixteen_body_classes' );



/**
 * Converts a HEX value to RGB.
 *
 * @since Twenty Sixteen 1.0
 *
 * @param string $color The original color, in 3- or 6-digit hexadecimal form.
 * @return array Array containing RGB (red, green, and blue) values for the given
 *               HEX code, empty array otherwise.
 */

function twentysixteen_hex2rgb( $color ) {

	$color = trim( $color, '#' );



	if ( strlen( $color ) === 3 ) {

		$r = hexdec( substr( $color, 0, 1 ).substr( $color, 0, 1 ) );

		$g = hexdec( substr( $color, 1, 1 ).substr( $color, 1, 1 ) );

		$b = hexdec( substr( $color, 2, 1 ).substr( $color, 2, 1 ) );

	} else if ( strlen( $color ) === 6 ) {

		$r = hexdec( substr( $color, 0, 2 ) );

		$g = hexdec( substr( $color, 2, 2 ) );

		$b = hexdec( substr( $color, 4, 2 ) );

	} else {

		return array();

	}



	return array( 'red' => $r, 'green' => $g, 'blue' => $b );

}



/**
 * Custom template tags for this theme.
 */

require get_template_directory() . '/inc/template-tags.php';



/**
 * Customizer additions.
 */

require get_template_directory() . '/inc/customizer.php';



/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for content images
 *
 * @since Twenty Sixteen 1.0
 *
 * @param string $sizes A source size value for use in a 'sizes' attribute.
 * @param array  $size  Image size. Accepts an array of width and height
 *                      values in pixels (in that order).
 * @return string A source size value for use in a content image 'sizes' attribute.
 */

function twentysixteen_content_image_sizes_attr( $sizes, $size ) {

	$width = $size[0];



	840 <= $width && $sizes = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 1362px) 62vw, 840px';



	if ( 'page' === get_post_type() ) {

		840 > $width && $sizes = '(max-width: ' . $width . 'px) 85vw, ' . $width . 'px';

	} else {

		840 > $width && 600 <= $width && $sizes = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 984px) 61vw, (max-width: 1362px) 45vw, 600px';

		600 > $width && $sizes = '(max-width: ' . $width . 'px) 85vw, ' . $width . 'px';

	}



	return $sizes;

}

add_filter( 'wp_calculate_image_sizes', 'twentysixteen_content_image_sizes_attr', 10 , 2 );



/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for post thumbnails
 *
 * @since Twenty Sixteen 1.0
 *
 * @param array $attr Attributes for the image markup.
 * @param int   $attachment Image attachment ID.
 * @param array $size Registered image size or flat array of height and width dimensions.
 * @return string A source size value for use in a post thumbnail 'sizes' attribute.
 */

function twentysixteen_post_thumbnail_sizes_attr( $attr, $attachment, $size ) {

	if ( 'post-thumbnail' === $size ) {

		is_active_sidebar( 'sidebar-1' ) && $attr['sizes'] = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 984px) 60vw, (max-width: 1362px) 62vw, 840px';

		! is_active_sidebar( 'sidebar-1' ) && $attr['sizes'] = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 1362px) 88vw, 1200px';

	}

	return $attr;

}

add_filter( 'wp_get_attachment_image_attributes', 'twentysixteen_post_thumbnail_sizes_attr', 10 , 3 );



/**
 * Modifies tag cloud widget arguments to have all tags in the widget same font size.
 *
 * @since Twenty Sixteen 1.1
 *
 * @param array $args Arguments for tag cloud widget.
 * @return array A new modified arguments.
 */

function twentysixteen_widget_tag_cloud_args( $args ) {

	$args['largest'] = 1;

	$args['smallest'] = 1;

	$args['unit'] = 'em';

	return $args;

}

add_filter( 'widget_tag_cloud_args', 'twentysixteen_widget_tag_cloud_args' );



/**
 * Implement the Custom Post.
 *
 * @since Whirlpool 1.0
 */

//require get_template_directory() . '/ab-inc/aux-functions.php';



/**
 * Implement the Custom Post.
 *
 * @since Whirlpool 1.0
 */

require get_template_directory() . '/ab-inc/config-theme.php';
require get_template_directory() . '/ab-inc/signed-players-import.php';



/**
 * Implement the Custom Post.
 *
 * @since Whirlpool 1.0
 */

require get_template_directory() . '/ab-inc/custom-hooks.php';



/**
 * Implement the Custom Post.
 *
 * @since Whirlpool 1.0
 */

require get_template_directory() . '/ab-inc/custom-posts.php';



/**
 * Implement the Custom Post.
 *
 * @since Whirlpool 1.0
 */

//require get_template_directory() . '/ab-inc/ab-api-rest.php';



/**
 * Implement the Custom Post.
 *
 * @since Whirlpool 1.0
 */

require get_template_directory() . '/ab-inc/custom-mails.php';



/**
 * Implement the Custom Post.
 *
 * @since Whirlpool 1.0
 */

require get_template_directory() . '/ab-inc/ajax-calls.php';





/**
 * Implement the Custom Post.
 *
 * @since Whirlpool 1.0
 */

require get_template_directory() . '/ab-inc/manage-posts.php';



/**
 * Implement the Custom Post.
 *
 * @since Whirlpool 1.0
 */

require get_template_directory() . '/ab-inc/acf-hook-functions.php';



/**
 * Implement the Custom User Permissions.
 *
 * @since Whirlpool 1.0
 */

require get_template_directory() . '/ab-inc/custom-user-permissions.php';



/**
 * Implement the custom slugs.
 *
 * @since Whirlpool 1.0
 */

require get_template_directory() . '/ab-inc/custom-slugs.php';

/**
 * Implement the Hooks Posts.
 *
 * @since Whirlpool 1.0
 */
require get_template_directory() . '/ab-inc/hooks-posts.php';