<?php
/**
 * Definimos los permisos de los post y los usuarios
 * (está preparado, pero no se implementa)
 *
 *
 * Customer: el más bajo (no se debería crear un perfil con este role)
 * Suscriptor: el más bajo (no se debería crear un perfil con este role)
 *
 * Colaborador: acceso directo a su perfil, comentarios y los custom types definidos (sin acceso a multimedia-medios)
 *
 * Autor: acceso directo a su perfil, comentarios, multimedia-medios y los custom types definidos
 *
 * Editor: acceso directo a su perfil, multimedia-medios, páginas, comentarios, custom types definidos
 *
 * Shop Manager: por defecto tiene acceso a todo lo relacionado con productos, categorias, listado de usuarios, páginas, multimedia-medios, comentarios, los custom types definidos, configuración whirlpool, configuración de ACF y configuración básica de la web
 *
 * Administrador: acceso a todo
 *
 *
 * @param string role
 * @param string custom post
 * @param string type add or remove
 */
function add_permissions_role($role,$post,$type){

    $caps = array();
    $caps['torneo'] = array(
        'edit_torneo',
        'edit_torneos',
        'edit_other_torneos',
        'publish_torneos',
        'read_torneo',
        'delete_torneo',
        'delete_torneos',
    );

    $caps['slider'] = array(
        'edit_slider',
        'edit_sliders',
        'edit_other_sliders',
        'publish_sliders',
        'read_slider',
        'delete_slider',
        'delete_sliders',
    );

    $caps['hotel'] = array(
        'edit_hotel',
        'edit_hotels',
        'edit_other_hotels',
        'publish_hotels',
        'read_hotel',
        'delete_hotel',
        'delete_hotels',
    );

    $caps['patrocinador'] = array(
        'edit_patrocinador',
        'edit_patrocinadors',
        'edit_other_patrocinadors',
        'publish_patrocinadors',
        'read_patrocinador',
        'delete_patrocinador',
        'delete_patrocinadors',
    );

    $caps['player'] = array(
        'edit_player',
        'edit_players',
        'edit_other_players',
        'publish_players',
        'read_player',
        'delete_player',
        'delete_players',
    );

    $caps['livescoring'] = array(
        'edit_livescoring',
        'edit_livescorings',
        'edit_other_livescorings',
        'publish_livescorings',
        'read_livescoring',
        'delete_livescoring',
        'delete_livescorings',
    );

    $role_object = get_role( $role );

    if( $post != '' ){    
        foreach($caps[$post] as $valCaps){
            if( $type == 'add' ) $role_object->add_cap( $valCaps ); 
            if( $type == 'remove' ) $role_object->remove_cap( $valCaps ); 
        }
    }

    /*if( $role=='editor' || $role == 'shop_manager'){
        if( function_exists('acf_set_options_page_capability') ){
            acf_set_options_page_capability( 'manage_options' );
            $role_object->add_cap( 'manage_options' ); 
        }
    }*/

    
}

// Definimos los persmisos de cada role
function add_permissions_role_user() {
    // gets the administrator role
    $roles = array(
        array(
            'role' => 'administrator',
            'custom_post' => 'torneo',
            'type' => 'add',
        ),
        array(
            'role' => 'administrator',
            'custom_post' => 'slider',
            'type' => 'add',
        ),
        array(
            'role' => 'administrator',
            'custom_post' => 'hotel',
            'type' => 'add',
        ),
        array(
            'role' => 'administrator',
            'custom_post' => 'player',
            'type' => 'add',
        ),
        array(
            'role' => 'administrator',
            'custom_post' => 'patrocinador',
            'type' => 'add',
        ),
        array(
            'role' => 'administrator',
            'custom_post' => 'livescoring',
            'type' => 'add',
        ),
        // array(
        //  'role' => 'editor',
        //  'custom_post' => '',
        //  'type' => '',
        // ),
        // array(
        //  'role' => 'administrator',
        //  'custom_post' => 'tienda',
        //  'type' => 'add',
        // ),
        // array(
        //  'role' => 'administrator',
        //  'custom_post' => 'mosaico',
        //  'type' => 'add',
        // ),
    );

    foreach( $roles as $the_role => $custom_post_rule ){
        add_permissions_role($custom_post_rule['role'],$custom_post_rule['custom_post'],$custom_post_rule['type']);
    }

    
}
add_action('admin_init','add_permissions_role_user',998);
