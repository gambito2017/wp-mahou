<?php
/**
 * API REST Assa Abloy
 *
 * @subpackage assaabloy
 * @since Assa Abloy 1.0
 */


/**
 * Grab latest post title by an author!
 *
 * @param array $data Options for the function.
 * @return string|null Post title for the latest,  * or null if none.
 */


function get_home_posts( $data ) {

    $args = array(
        'posts_per_page' => '-1'
    );
    $posts = get_posts($args);

    if ( empty( $posts ) ) {
        return null;
    }

    $apiArray = array();
    $arrayNormal = [];
    $arrayDestacados = [];
    foreach( $posts as $post ){

        $mainImage = get_field('main_image', $post->ID);
        $terms = get_the_terms($post->ID,'category');
        $auxTerm = [];
        foreach($terms as $term){
            $auxTerm[] = array(
                'name' => $term->name,
                'slug' => $term->slug,
                'total' => $term->count,
                'id' => $term->term_id,
            );
        }

        $aux = array(
            'id' => $post->ID,
            'title' => $post->post_title,
            'slug' => $post->post_name,
            'date' => date('jS M Y',strtotime($post->post_date)),
            'content' => $post->post_content,
            //'link' => get_permalink($post->ID),
            'short_desc' => get_field('short_description', $post->ID),
            'image' => array(
                'original' => $mainImage['url'],
                'thumbnail' => $mainImage['sizes']['thumbnail'],
                'news' => $mainImage['sizes']['news'],
                'news_double' => $mainImage['sizes']['news_double'],
                'news_vertical' => $mainImage['sizes']['news_vertical'],
                'newscrop' => $mainImage['sizes']['newscrop'],
                'news_doublecrop' => $mainImage['sizes']['news_doublecrop'],
                'news_verticalcrop' => $mainImage['sizes']['news_verticalcrop'],
            ),
            'category' => $auxTerm
        );
        if( get_field('images_gallery', $post->ID) ){
            $imagesGallery = [];
            foreach( get_field('images_gallery', $post->ID) as $image ){
                $imagesGallery[] = array(
                    'original' => $image['url'],
                    'thumbnail' => $image['sizes']['thumbnail'],
                    'news' => $image['sizes']['news'],
                    'news_double' => $image['sizes']['news_double'],
                    'news_vertical' => $image['sizes']['news_vertical'],
                    'newscrop' => $image['sizes']['newscrop'],
                    'news_doublecrop' => $image['sizes']['news_doublecrop'],
                    'news_verticalcrop' => $image['sizes']['news_verticalcrop'],
                );
            }
            $aux['gallery'] = $imagesGallery;
        }

        if( get_field('featured', $post->ID) ){
            $arrayDestacados[] = $aux;
        }
        else{
            $arrayNormal[] = $aux;
        }
    }
    $apiArray['destacados'] = $arrayDestacados;
    $apiArray['others'] = $arrayNormal;
    //$apiArray = array_merge($arrayDestacados,$arrayNormal);

    $api['noticias'] = [$apiArray];
    return $api;
}

add_action( 'rest_api_init', function () {
    register_rest_route( 'get', '/home/posts', array(
        'methods' => 'GET',
        'callback' => 'get_home_posts',
    ) );
} );

/*********************************************/
function get_news_posts( $data ) {

    $args = array(
        'posts_per_page' => '-1'
    );
    $posts = get_posts($args);

    if ( empty( $posts ) ) {
        return null;
    }

    $apiArray = [];
    $arrayNormal = [];
    $arrayDestacados = [];
    foreach( $posts as $post ){

        $mainImage = get_field('main_image', $post->ID);
        $terms = get_the_terms($post->ID,'category');
        $auxTerm = [];
        foreach($terms as $term){
            $auxTerm[] = array(
                'name' => $term->name,
                'slug' => $term->slug,
                'total' => $term->count,
                'id' => $term->term_id,
            );
        }

        $aux = array(
            'id' => $post->ID,
            'title' => $post->post_title,
            'date' => date('jS M Y',strtotime($post->post_date)),
            'content' => $post->post_content,
            //'link' => get_permalink($post->ID),
            'slug' => $post->post_name,
            'short_desc' => get_field('short_description', $post->ID),
            'image' => array(
                'original' => $mainImage['url'],
                'thumbnail' => $mainImage['sizes']['thumbnail'],
                'news' => $mainImage['sizes']['news'],
                'news_double' => $mainImage['sizes']['news_double'],
                'news_vertical' => $mainImage['sizes']['news_vertical'],
                'newscrop' => $mainImage['sizes']['newscrop'],
                'news_doublecrop' => $mainImage['sizes']['news_doublecrop'],
                'news_verticalcrop' => $mainImage['sizes']['news_verticalcrop'],
            ),
            'category' => $auxTerm
        );
        if( get_field('images_gallery', $post->ID) ){
            $imagesGallery = [];
            foreach( get_field('images_gallery', $post->ID) as $image ){
                $imagesGallery[] = array(
                    'original' => $image['url'],
                    'thumbnail' => $image['sizes']['thumbnail'],
                    'news' => $image['sizes']['news'],
                    'news_double' => $image['sizes']['news_double'],
                    'news_vertical' => $image['sizes']['news_vertical'],
                    'newscrop' => $image['sizes']['newscrop'],
                    'news_doublecrop' => $image['sizes']['news_doublecrop'],
                    'news_verticalcrop' => $image['sizes']['news_verticalcrop'],
                );
            }
            $aux['gallery'] = $imagesGallery;
        }

        if( get_field('featured', $post->ID) ){
            $arrayDestacados[] = $aux;
        }
        
        foreach($terms as $term){
            $apiArray['others'][$term->slug]['category'] = array(
                'name' => $term->name,
                'slug' => $term->slug,
                'total' => $term->count,
                'id' => $term->term_id,
            );
            $apiArray['others'][$term->slug]['posts'][] = $aux;
        }
    }
    $apiArray['destacados'] = $arrayDestacados;

    $api['noticias'] = [$apiArray];
    return $api;
}

add_action( 'rest_api_init', function () {
    register_rest_route( 'get', '/news/posts', array(
        'methods' => 'GET',
        'callback' => 'get_news_posts',
    ) );
} );
/*********************************************/
function get_all_posts_by_category( $data ) {
    $term = get_term_by('slug',$data['id'],'category');
    //die('<pre>'.print_r($term,true).'</pre>');
    $args = array(
        'posts_per_page' => '-1',
        'category' => $term->term_id,
    );
    $posts = get_posts( $args );

    if ( empty( $posts ) ) {
        return null;
    }

    $apiArray = [];

    //$term = get_term($data['id'],'category');
    $apiArray['category'] = array(
        'name' => $term->name,
        'slug' => $term->slug,
        'total' => $term->count,
        'id' => $term->term_id,
    );
    foreach( $posts as $post ){

        $mainImage = get_field('main_image', $post->ID);
        $terms = get_the_terms($post->ID,'category');
        $auxTerm = [];
        foreach($terms as $term){
            $auxTerm[] = array(
                'name' => $term->name,
                'slug' => $term->slug,
                'total' => $term->count,
                'id' => $term->term_id,
            );
        }

        $aux = array(
            'id' => $post->ID,
            'title' => $post->post_title,
            'date' => date('jS M Y',strtotime($post->post_date)),
            'content' => $post->post_content,
            //'link' => get_permalink($post->ID),
            'slug' => $post->post_name,
            'short_desc' => get_field('short_description', $post->ID),
            'image' => array(
                'original' => $mainImage['url'],
                'thumbnail' => $mainImage['sizes']['thumbnail'],
                'news' => $mainImage['sizes']['news'],
                'news_double' => $mainImage['sizes']['news_double'],
                'news_vertical' => $mainImage['sizes']['news_vertical'],
                'newscrop' => $mainImage['sizes']['newscrop'],
                'news_doublecrop' => $mainImage['sizes']['news_doublecrop'],
                'news_verticalcrop' => $mainImage['sizes']['news_verticalcrop'],
            ),
            'category' => $auxTerm
        );
        if( get_field('images_gallery', $post->ID) ){
            $imagesGallery = [];
            foreach( get_field('images_gallery', $post->ID) as $image ){
                $imagesGallery[] = array(
                    'original' => $image['url'],
                    'thumbnail' => $image['sizes']['thumbnail'],
                    'news' => $image['sizes']['news'],
                    'news_double' => $image['sizes']['news_double'],
                    'news_vertical' => $image['sizes']['news_vertical'],
                    'newscrop' => $image['sizes']['newscrop'],
                    'news_doublecrop' => $image['sizes']['news_doublecrop'],
                    'news_verticalcrop' => $image['sizes']['news_verticalcrop'],
                );
            }
            $aux['gallery'] = $imagesGallery;
        }

        $apiArray['posts'][] = $aux;
    }

    $api['noticias'] = [$apiArray];
    return $api;
}

add_action( 'rest_api_init', function () {
    register_rest_route( 'get', '/category/posts/(?P<id>\D+)', array(
        'methods' => 'GET',
        'callback' => 'get_all_posts_by_category',
    ) );
} );
/*********************************************/
function get_info_post( $data ) {
    $post = get_posts(array(
            'name' => $data['id']
    ));

    //$term = get_term_by('slug',$data['id'],'category');
    //die('<pre>'.print_r($post,true).'</pre>');

    if ( empty( $post ) ) {
        return null;
    }

    $post = $post[0];

    $apiArray = [];

    //$term = get_term($data['id'],'category');
    /*$apiArray['category'] = array(
        'name' => $term->name,
        'slug' => $term->slug,
        'total' => $term->count,
        'id' => $term->term_id,
    );
    foreach( $posts as $post ){*/

        $mainImage = get_field('main_image', $post->ID);
        $terms = get_the_terms($post->ID,'category');
        $auxTerm = [];
        foreach($terms as $term){
            $auxTerm[] = array(
                'name' => $term->name,
                'slug' => $term->slug,
                'total' => $term->count,
                'id' => $term->term_id,
            );
        }

        $aux = array(
            'id' => $post->ID,
            'title' => $post->post_title,
            'date' => date('jS M Y',strtotime($post->post_date)),
            'content' => $post->post_content,
            //'link' => get_permalink($post->ID),
            'slug' => $post->post_name,
            'short_desc' => get_field('short_description', $post->ID),
            'image' => array(
                'original' => $mainImage['url'],
                'thumbnail' => $mainImage['sizes']['thumbnail'],
                'news' => $mainImage['sizes']['news'],
                'news_double' => $mainImage['sizes']['news_double'],
                'news_vertical' => $mainImage['sizes']['news_vertical'],
                'newscrop' => $mainImage['sizes']['newscrop'],
                'news_doublecrop' => $mainImage['sizes']['news_doublecrop'],
                'news_verticalcrop' => $mainImage['sizes']['news_verticalcrop'],
            ),
            'category' => $auxTerm
        );
        if( get_field('images_gallery', $post->ID) ){
            $imagesGallery = [];
            foreach( get_field('images_gallery', $post->ID) as $image ){
                $imagesGallery[] = array(
                    'original' => $image['url'],
                    'thumbnail' => $image['sizes']['thumbnail'],
                    'news' => $image['sizes']['news'],
                    'news_double' => $image['sizes']['news_double'],
                    'news_vertical' => $image['sizes']['news_vertical'],
                    'newscrop' => $image['sizes']['newscrop'],
                    'news_doublecrop' => $image['sizes']['news_doublecrop'],
                    'news_verticalcrop' => $image['sizes']['news_verticalcrop'],
                );
            }
            $aux['gallery'] = $imagesGallery;
        }

        $apiArray['post'] = $aux;
    //}

    $api['noticias'] = [$apiArray];
    return $api;
}

add_action( 'rest_api_init', function () {
    register_rest_route( 'get', '/post/(?P<id>\D+)', array(
        'methods' => 'GET',
        'callback' => 'get_info_post',
    ) );
} );
/*********************************************/





function get_variable_info_web ( $data ) {

    $variable_list_array = array();

    // brochure
    if( $data->get_params()['page'] == 'brochure' ) {
        $variable_list_array['brochure_pdf'] = get_option('brochure_pdf');
    }

    // main
    if( $data->get_params()['page'] == 'main' ) {
        $variable_list_array['front_web_title'] = get_option('front_web_title');
        $variable_list_array['favicon'] = get_option('favicon');
        $variable_list_array['logo_web'] = get_option('logo_web');
        $variable_list_array['join_now_activate'] = get_option('join_now_activate');
        $variable_list_array['title_join_now'] = get_option('title_join_now');
        //$variable_list_array['icon_join_now'] = get_option('icon_join_now');
        $variable_list_array['google_analytics_script'] = get_option('google_analytics_script');
    }

    // FAQ
    if( $data->get_params()['page'] == 'faqs' ) {
        $variable_list_array['title_page_faqs'] = get_option('title_page_faqs');
        $variable_list_array['subtitle_page_faqs'] = get_option('subtitle_page_faqs');
        $variable_list_array['email_banner_page_faqs'] = get_option('email_banner_page_faqs');
        $variable_list_array['image_page_faqs'] = get_option('image_page_faqs');
        $variable_list_array['image_page_faqs_mobile'] = get_option('image_page_faqs_mobile');

        $arrayFaqs = array();
        $loop = new WP_Query(
            array(
                'post_type' => 'faq',
                'post_status' => 'publish',
                'orderby' => 'menu_order',
                'nopaging' => true
            )
        );

        while ( $loop->have_posts() ) : $loop->the_post();

            global $post;
            $arrayFaqs[] = array(
                'title' => html_entity_decode(get_the_title()),
                'desciption' => get_the_content(),
                'slug' => $post->post_name,
                'id' => $post->ID,
            );

        endwhile;

        $variable_list_array['faqs'] = $arrayFaqs;
    }

    // Register
    if( $data->get_params()['page'] == 'register' ) {
        $variable_list_array['title_page_register'] = get_option('title_page_register');
        $variable_list_array['subtitle_page_register'] = get_option('subtitle_page_register');
        $variable_list_array['image_page_register'] = get_option('image_page_register');
        $variable_list_array['image_page_register_mobile'] = get_option('image_page_register_mobile');

        $variable_list_array['step_one'] = array(
            'title' => get_option('title_step_one_register'),
            'subtitle' => get_option('subtitle_step_one_register'),
            'copy' => get_option('copy_step_full_register')
        );

        $variable_list_array['step_mail'] = array(
            'copy' => get_option('subtitle_step_mail_register')
        );

        $variable_list_array['success_full_form'] = array(
            'title' => get_option('title_success_full_register'),
            'subtitle' => get_option('subtitle_success_full_register'),
            //'icon' => get_option('icon_success_full_register')
        );

        $variable_list_array['success_mail_form'] = array(
            'title' => get_option('title_success_mail_register'),
            'subtitle' => get_option('subtitle_success_mail_register'),
            //'icon' => get_option('icon_success_mail_register')
        );

        $variable_list_array['yes_button'] = get_option('yes_button_title_register');
        $variable_list_array['no_button'] = get_option('no_button_title_register');

        $variable_list_array['join_mail'] = get_option('mail_form_join_button_text');
        $variable_list_array['join_full'] = get_option('full_form_join_button_text');

        $variable_list_array['text_required_field'] = get_option('text_required_field');

    }



    return $variable_list_array;
}

add_action( 'rest_api_init', function () {
    register_rest_route( 'get', '/info', array(
        'methods' => 'GET',
        'callback' => 'get_variable_info_web',
    ) );
} );

/*********************************************/

function get_list_faqs () {

    $list_array = array();

    $loop = new WP_Query(
        array(
            'post_type' => 'faq',
            'post_status' => 'publish',
            'orderby' => 'menu_order',
            'nopaging' => true
        )
    );

    while ( $loop->have_posts() ) : $loop->the_post();

        global $post;
        $list_array[] = array(
            'title' => html_entity_decode(get_the_title()),
            'desciption' => get_the_content(),
            'slug' => $post->post_name,
            'id' => $post->ID,
        );

    endwhile;

    wp_reset_query();


    return $list_array;
}

add_action( 'rest_api_init', function () {
    register_rest_route( 'get', '/list/faqs', array(
        'methods' => 'GET',
        'callback' => 'get_list_faqs',
    ) );
} );

/*********************************************/

function post_register_web ( $data ) {

    $arrayInfo = array();
    global $wpdb;

    $sendEmail = false;
    if( $data->get_params()['type'] == 'full' ){
        $searchQuerySQL = "
          SELECT pid
          FROM ab_person
          WHERE 1
          and pemail = '".$data->get_params()['email']."' ";
        $searchQuery = $wpdb->get_row( $searchQuerySQL );

        if( $searchQuery !== null ){
            $arrayInfo['status'] = 'error';
        }
        else{
            $wpdb->insert(
                'ab_person',
                array(
                    'pname' => $data->get_params()['name'],
                    'ptitle_name' => $data->get_params()['title_name'],
                    'psurname' => $data->get_params()['surname'],
                    'pstreet' => $data->get_params()['street'],
                    'pstreet_number' => $data->get_params()['street_number'],
                    'pcity' => $data->get_params()['city'],
                    'pemail' => $data->get_params()['email'],
                    'pzip' => $data->get_params()['zip'],
                    'pphone' => $data->get_params()['phone'],
                    'pdate' => date('H:i:s d-m-Y', strtotime('+2 hours') )
                )
            );
            $arrayInfo['status'] = 'ok';
            $sendEmail = true;
        }
    }

    if( $data->get_params()['type'] == 'mail' ){
        $searchQuerySQL = "
          SELECT eid
          FROM ab_email
          WHERE 1
          and email = '".$data->get_params()['email']."' ";
        $searchQuery = $wpdb->get_row( $searchQuerySQL );

        if( $searchQuery !== null ){
            $arrayInfo['status'] = 'error';
        }
        else{
            $wpdb->insert(
                'ab_email',
                array(
                    'email' => $data->get_params()['email'],
                    'edate' => date('H:i:s d-m-Y', strtotime('+2 hours') )
                )
            );
            $arrayInfo['status'] = 'ok';
            $sendEmail = true;
        }
    }

    if( get_option('emails_register') && $sendEmail ) {

        // usuario
        if( $data->get_params()['type'] == 'mail' ){
            $emailContent = _ab_mail_html_mail_form();
            $subject = get_option('confirm_mail_subject');
            if( $sendEmail ) _ab_send_mail($data->get_params()['email'],$emailContent,$subject,'user');
        }

        // admin y usuario
        if( $data->get_params()['type'] == 'full' ) {

            if ($sendEmail){
                $emailContent = _ab_mail_html_full_form($data);
                $subject = get_option('confirm_full_subject');
                _ab_send_mail($data->get_params()['email'], $emailContent, $subject, 'user');

                $emailContent = _ab_mail_html_admin_form($data);
                $subject = get_option('admin_mail_subject');
                _ab_send_mail(get_option('emails_register'), $emailContent, $subject, 'admin');
            }
        }
    }


    return $arrayInfo;
}

function _ab_send_mail($to,$content,$subject,$type){

    require_once get_template_directory() . '/ab-inc/phpmailer/class.phpmailer.php';
    $mail = new PHPMailer();
    // $mail->IsSendmail();
    $mail->IsSMTP();
    $mail->SMTPDebug = 0;
    $mail->CharSet = "UTF-8";

    /*$mail->SMTPAuth = true;
    $mail->SMTPSecure = "ssl";
    $mail->Host = "mail.mijndomein.nl";
    $mail->Port = 465;                    // set the SMTP port for the GMAIL server
    $mail->Username = "ab@veiligmetab.nl"; // SMTP account username
    $mail->Password = "ABSiteMail01";        // SMTP account password
    */

    $mail->SMTPAuth = true;
    $mail->SMTPSecure = "tls";
    $mail->Host = "smtp.mandrillapp.com";
    $mail->Port = 587;                    // set the SMTP port for the GMAIL server
    $mail->Username = "ktc digital agency"; // SMTP account username
    $mail->Password = "qzrbJCcpJF9riw40_7impw";        // SMTP account password

    if( $type == 'user' ){
        $mail->AddAddress(trim($to));
    }
    else {
        $emailTo = explode(',', $to);
        $isFirst = true;
        foreach ($emailTo as $keyE => $valE) {
            if ($isFirst) {
                $mail->AddAddress(trim($valE));
                $isFirst = false;
            } else $mail->AddCC(trim($valE));
        }
    }

    $mail->SetFrom('ab@veiligmetab.nl', utf8_decode('AB Simpel|Handig|Veilig'));
    $mail->AddReplyTo('ab@veiligmetab.nl');

    $mail->Subject = utf8_decode($subject);
    $mail->IsHTML(true);


    $mail->Body = $content;
    $mail->Send();

    unset($mail);
}

// usuario mail form
function _ab_mail_html_mail_form(){

    $content = '<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta name="viewport" content="width=device-width"/>
    </head>

    <body>
      <table border="0" cellpadding="0" cellspacing="0" width="600" style="width:600px;margin:0 auto;">
        <tr>
          <td valign="top">';
                $content .= '<div style="font-size: 16px;line-height:20px;letter-spacing: 0.3px;color: #29303a;">';
                    $content .= '<div style="background-color: #e63b5f;text-align:center;">';
                        $content .= '<img src="'.get_option('logo_web').'"/>';
                    $content .= '</div>';

                    $content .= '<div style="background-color: #ffffff;margin:50px;">';

                    $content .= get_option('confirm_mail_text');

                    $content .= '<br/><p>Vriendelijke groet,<br/>Het Ab team</p>';

                $content .= '</div>';


                $content .= '<div style="background-color: #e63b5f;height:10px"></div>';
                $content .= '</div>';
          $content .= '</td>
        </tr>
      </table>
    </body>
    </html>';

    return $content;
}

// usuario mail form
function _ab_mail_html_full_form($data){

    $content = '<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta name="viewport" content="width=device-width"/>
    </head>

    <body>
      <table border="0" cellpadding="0" cellspacing="0" width="600" style="width:600px;margin:0 auto;">
        <tr>
          <td valign="top">';
            $content .= '<div style="font-size: 16px;line-height:20px;letter-spacing: 0.3px;color: #29303a;">';
            $content .= '<div style="background-color: #e63b5f;text-align:center;">';
            $content .= '<img src="'.get_option('logo_web').'"/>';
            $content .= '</div>';

            $content .= '<div style="background-color: #ffffff;margin:50px;">';
                $content .= '<p>Beste '.$data->get_params()['name'].', </p>';
                $content .= get_option('confirm_full_text');

            $content .= '<br/><p>Vriendelijke groet,<br/>Het Ab team</p>';

            $content .= '</div>';


            $content .= '<div style="background-color: #e63b5f;height:10px"></div>';
            $content .= '</div>';
            $content .= '</td>
        </tr>
      </table>
    </body>
    </html>';

    return $content;
}

// admin mail
function _ab_mail_html_admin_form($data){

    $content = '<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta name="viewport" content="width=device-width"/>
    </head>
    <body>
      <table border="0" cellpadding="0" cellspacing="0" width="600" style="width:600px;margin:0 auto;">
        <tr>
          <td valign="top">';
        $content .= '<div style="font-size: 16px;line-height:20px;letter-spacing: 0.3px;color: #29303a;">';
            $content .= '<div style="background-color: #e63b5f;text-align:center;">';
                $content .= '<img src="'.get_option('logo_web').'"/>';
            $content .= '</div>';

            $content .= '<div style="background-color: #ffffff;margin:50px;">';

                $content .= '<p>Er is een nieuwe aanmelding voor de Ab pilot</p>';

                $content .= '<p>Voornaam: <b style="color:#e63b5f;">'.$data->get_params()['name'].'</b></p>';
                $content .= '<p>Tussenvoegsel: <b style="color:#e63b5f;">'.$data->get_params()['title_name'].'</b></p>';
                $content .= '<p>Achternaam: <b style="color:#e63b5f;">'.$data->get_params()['surname'].'</b></p>';
                $content .= '<p>Straat: <b style="color:#e63b5f;">'.$data->get_params()['street'].'</b></p>';
                $content .= '<p>Postcode: <b style="color:#e63b5f;">'.$data->get_params()['zip'].'</b></p>';
                $content .= '<p>Huisnummer: <b style="color:#e63b5f;">'.$data->get_params()['street_number'].'</b></p>';
                $content .= '<p>Plaats: <b style="color:#e63b5f;">'.$data->get_params()['city'].'</b></p>';
                $content .= '<p>Email: <b style="color:#e63b5f;">'.$data->get_params()['email'].'</b></p>';
                $content .= '<p>Telefoon: <b style="color:#e63b5f;">'.$data->get_params()['phone'].'</b></p>';

                $content .= '<br/><br/><p><a style="text-decoration:none;color:#e63b5f;padding:15px 20px;border:solid 1px #e63b5f;border-radius:30px;" target="_blank" href="'.admin_url('/admin.php?page=theme-panel&tab=peoplejoined', 'http').'">Bekijk aanmeldingen</a></p>';

                $content .= '<br/><br/>';
                $content .= '<br/><p>Vriendelijke groet,<br/>Het Ab team</p>';

            $content .= '</div>';


            $content .= '<div style="background-color: #e63b5f;height:10px"></div>';
        $content .= '</div>';
    $content .= '</td>
        </tr>
      </table>
    </body>
    </html>';

    return $content;

}

// participation mail
function _ab_mail_participation_mail_form($pid){

    global $wpdb;
    $registerSql = "
        SELECT pid, pname, ptitle_name, psurname, pstreet, pzip, pstreet_number, pcity, pemail, pphone, pdate, pflag
        FROM ab_person
        WHERE pid = ".$pid."  ";

    $register = $wpdb->get_results($registerSql);

    $content = '<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta name="viewport" content="width=device-width"/>
    </head>
    <body>
      <table border="0" cellpadding="0" cellspacing="0" width="600" style="width:600px;margin:0 auto;">
        <tr>
          <td valign="top">';
            $content .= '<div style="font-size: 16px;line-height:20px;letter-spacing: 0.3px;color: #29303a;">';
                $content .= '<div style="background-color: #e63b5f;text-align:center;">';
                $content .= '<img src="'.get_option('logo_web').'"/>';
                $content .= '</div>';

                $content .= '<div class="body" style="background-color: #ffffff;margin:50px;">';

                    $content .= '<p>Beste '.$register[0]->pname.', </p>';
                    $content .= get_option('participation_mail_text');

                    $content .= '<br/><p>Vriendelijke groet,<br/>Het Ab team</p>';

                $content .= '</div>';


                $content .= '<div style="background-color: #e63b5f;height:10px"></div>';
            $content .= '</div>';
    $content .= '</td>
        </tr>
      </table>
    </body>
    </html>';

    return $content;

}

add_action( 'rest_api_init', function () {
    register_rest_route( 'post', '/register', array(
        'methods' => 'POST',
        'callback' => 'post_register_web',
    ) );
} );

/*********************************************/

function get_list_sections ( $data ) {

    $list_array = array();

    $MetaQuerySection = array( array( 'key' => 'section_page', 'value' => $data->get_params()['section'], 'compare' => 'LIKE' ) );
    $loop = new WP_Query(
        array(
            'post_type' => 'section',
            'post_status' => 'publish',
            'nopaging' => true,
            'orderby' => 'menu_order',
            'meta_query' => $MetaQuerySection
        )
    );

    while ( $loop->have_posts() ) : $loop->the_post();

        global $post;

        $arrayAux = array();

        $post_id = $post->ID;
        $type = get_field('section_type', $post_id);

        $arrayAux['type'] = $type;
        $arrayAux['slug'] = $post->post_name;
        $arrayAux['id'] = $post->ID;
        $arrayAux['title'] = get_field('page_title', $post_id);
        if( get_field('block_hash', $post_id) ){
            if( get_field('hidden_block', $post_id) ){
                $arrayAux['hash_hidden'] = get_field('block_hash', $post_id);
            }
            else{
                $arrayAux['hash'] = get_field('block_hash', $post_id);
            }
        }
        $arrayAux['is_hide'] = (get_field('hidden_block', $post_id) ) ? true : false;



        switch( $type ){

            // Banner
            case 'banner':

                $arrayAux['type'] = 'hero-b';
                $arrayAux['subtitle'] = get_field('page_subtitle', $post_id);

                $img_back = get_field('background_image_desktop', $post_id);
                $arrayAux['background_img_desktop'] = $img_back['url'];

                $img_back = get_field('background_image_mobile', $post_id);
                $arrayAux['background_img_mobile'] = $img_back['url'];
                break;

            // Banner Image
            case 'banner_image':
                $arrayAux['type'] = 'banner-image';
                $arrayAux['text'] = get_field('page_subtitle', $post_id);
                $arrayAux['background_color'] = get_field('background_color', $post_id);
                $arrayAux['title_color'] = get_field('page_title_color', $post_id);
                $arrayAux['text_color'] = get_field('page_text_color', $post_id);

                $img_back = get_field('central_image_desktop', $post_id);
                $arrayAux['img_desktop'] = $img_back['url'];

                $img_back = get_field('central_image_mobile', $post_id);
                $arrayAux['img_mobile'] = $img_back['url'];

                $arrayAux['img_type'] = get_field('image_type', $post_id);

                break;

            // Banner with button
            case 'banner_button':
                $arrayAux['type'] = 'hero-a';
                $arrayAux['page'] = $data->get_params()['section'];

                $arrayAux['subtitle'] = get_field('page_subtitle', $post_id);

                $img_back = get_field('background_image_desktop', $post_id);
                $arrayAux['background_img_desktop'] = $img_back['url'];

                $img_back = get_field('background_image_mobile', $post_id);
                $arrayAux['background_img_mobile'] = $img_back['url'];

                $arrayAux['button'] = array(
                    'button_title' => get_field('button_title', $post_id),
                    'button_link' => get_field('button_link', $post_id),
                    'button_hash' => ( get_field('button_hash', $post_id) ) ? get_field('button_hash', $post_id) : ''
                );
                break;

            // Block Title
            case 'block_title':
                $arrayAux['type'] = 'title';
                $arrayAux['content_sides_flag'] = (get_field('content_sides_flag', $post_id) ) ? true : false;
                break;

            // Call To Action Block
            case 'call_to_action':
                $arrayAux['type'] = 'call-to-action';
                $arrayAux['page'] = $data->get_params()['section'];
                $arrayAux['background_color'] = get_field('background_color', $post_id);
                $arrayAux['title_color'] = get_field('page_title_color', $post_id);

                $arrayAux['button'] = array(
                    'button_title' => get_field('button_title', $post_id),
                    'button_link' => get_field('button_link', $post_id),
                    'button_hash' => ( get_field('button_hash', $post_id) ) ? get_field('button_hash', $post_id) : ''
                );

                break;

            // Circle Images
            case 'circle_imgs':
                $arrayAux['type'] = 'hero-c';
                $img_back = get_field('background_image_desktop', $post_id);
                $arrayAux['background_img_desktop'] = $img_back['url'];

                $img_back = get_field('background_image_mobile', $post_id);
                $arrayAux['background_img_mobile'] = $img_back['url'];

                $arrayAux['subtitle'] = get_field('page_subtitle', $post_id);

                $arrayAux['circles'] = array();
                foreach( get_field('circle_images', $post_id) as $keyC => $valC ){
                    $arrayAux['circles'][] = $valC['url'];
                }
                break;

            // Columns
            case 'columns':
                $arrayAux['type'] = 'columns-a';

                $arrayAux['background_color'] = get_field('background_color', $post_id);

                $arrayAux['columns'] = array();
                foreach( get_field('columns', $post_id) as $keyC => $valC ){
                    $arrayAux['columns'][] = array(
                        'title' => $valC['column_title'],
                        'text' => $valC['column_text'],
                        'icon' => $valC['column_icon']['url']
                    );
                }
                break;

            // Content Sides
            case 'content_sides':
                $arrayAux['type'] = 'center-image';

                $arrayAux['title'] = '';
                $arrayAux['background_color'] = get_field('background_color', $post_id);

                $img_back = get_field('central_image_desktop', $post_id);
                $arrayAux['central_img_desktop'] = $img_back['url'];

                $img_back = get_field('central_image_mobile', $post_id);
                $arrayAux['central_img_mobile'] = $img_back['url'];

                $arrayAux['sides'] = array();
                foreach( get_field('content_sides', $post_id) as $keyC => $valC ){
                    if( $valC['side_type'] == 'list' ){

                        $arrayList = array();
                        foreach( $valC['item_list'] as $keyL => $valL ){
                            $arrayList[] = array(
                                'icon' => $valL['list_icon'],
                                'title' => $valL['list_title'],
                                'text' => $valL['list_text']
                            );
                        }

                        $arrayAux['sides'][] = array(
                            'type' => 'list',
                            'item_list' => $arrayList
                        );
                    }
                    if( $valC['side_type'] == 'text' ){

                        $arrayAux['sides'][] = array(
                            'type' => 'text',
                            'title' => $valC['page_title'],
                            'text' => $valC['block_text']
                        );
                    }
                }
                break;

            // Features List
            case 'features_list':

                $arrayAux['title'] = '';
                $arrayAux['type'] = 'features';

                $arrayAux['background_color_start'] = get_field('background_color_start', $post_id);
                $arrayAux['background_color_end'] = get_field('background_color_end', $post_id);
                if( get_field('background_patern', $post_id) ){
                    $img_back = get_field('background_patern', $post_id);
                    $arrayAux['background_patern'] = $img_back['url'];
                }
                $arrayAux['text_color'] = get_field('page_text_color', $post_id);

                $img_back = get_field('central_image_desktop', $post_id);
                $arrayAux['img_desktop'] = $img_back['url'];

                $img_back = get_field('central_image_mobile', $post_id);
                $arrayAux['img_mobile'] = $img_back['url'];


                $arrayAux['list'] = array();
                foreach( get_field('block_text_list', $post_id) as $keyC => $valC ){

                    $arrayAux['list'][] = array(
                        'text' => $valC['item_list']
                    );

                }
                break;

            // Image + Text Sides
            case 'small_center_text_sides':
                $arrayAux['type'] = 'half-small-center';

                $arrayAux['title'] = '';

                $valueField = get_field('image_text_block', $post_id);
                $arrayAux['img_desktop'] = $valueField[0]['central_image_desktop']['url'];
                $arrayAux['img_mobile'] = $valueField[0]['central_image_mobile']['url'];

                $arrayText = array();
                foreach( $valueField[0]['text'] as $keyT => $valT ){
                    $arrayText[] = $valT['block_text'];
                }
                $arrayAux['text'] = $arrayText;
                break;

            // Left/Right Block v1
            case 'left_right':
                $arrayAux['type'] = 'content-sides';
                $arrayAux['page'] = $data->get_params()['section'];
                $arrayAux['small_text'] = false;

                $arrayAux['background_color'] = get_field('background_color', $post_id);

                $arrayAux['menu_item'] = array();
                $arrayAux['title'] = '';

                foreach( get_field('left_right', $post_id) as $keyC => $valC ){
                    $position = ($keyC == 0) ? 'left' : 'right';
                    if( $valC['side_type'] == 'image' ){
                        $arrayAux['menu_item'][1] = array(
                            'type' => 'image',
                            'position' => $position,
                            'image_desktop' => $valC['background_image_desktop']['url'],
                            'image_mobile' => $valC['background_image_mobile']['url']
                        );
                    }
                    if( $valC['side_type'] == 'text' ){

                        $arrayButton = array();
                        if( $valC['button_title']!='' ){
                            $arrayButton = array(
                                'button_title' => $valC['button_title'],
                                'button_link' => $valC['button_link'],
                                'button_hash' => ( $valC['button_hash'] ) ? $valC['button_hash'] : ''
                            );
                        }

                        $arrayAux['menu_item'][0] = array(
                            'type' => 'text',
                            'position' => $position,
                            'title' => $valC['page_title'],
                            'text' => $valC['block_text'],
                            'button' => $arrayButton
                        );
                    }
                }
                break;

            // Left/Right Block v2
            case 'left_right_v2':
                $arrayAux['type'] = 'content-sides';
                $arrayAux['page'] = $data->get_params()['section'];
                $arrayAux['small_text'] = true;

                $arrayAux['background_color'] = get_field('background_color', $post_id);

                $arrayAux['menu_item'] = array();
                $arrayAux['title'] = '';

                foreach( get_field('left_right', $post_id) as $keyC => $valC ){
                    $position = ($keyC == 0) ? 'left' : 'right';
                    if( $valC['side_type'] == 'image' ){
                        $arrayAux['menu_item'][1] = array(
                            'type' => 'image',
                            'position' => $position,
                            'image_desktop' => $valC['background_image_desktop']['url'],
                            'image_mobile' => $valC['background_image_mobile']['url']
                        );
                    }
                    if( $valC['side_type'] == 'text' ){

                        $arrayButton = array();
                        if( $valC['button_title']!='' ){
                            $arrayButton = array(
                                'button_title' => $valC['button_title'],
                                'button_link' => $valC['button_link'],
                                'button_hash' => $valC['button_hash']
                            );
                        }

                        $arrayAux['menu_item'][0] = array(
                            'type' => 'text',
                            'position' => $position,
                            'title' => $valC['page_title'],
                            'text' => $valC['block_text'],
                            'button' => $arrayButton
                        );
                    }
                }
                break;

            // Logos Block
            case 'logos_block':
                $arrayAux['type'] = 'logos-block';
                $arrayAux['background_color_start'] = get_field('background_color_start', $post_id);
                $arrayAux['background_color_end'] = get_field('background_color_end', $post_id);

                $arrayAux['logos'] = array();
                foreach( get_field('circle_images', $post_id) as $keyC => $valC ){
                    $arrayAux['logos'][] = $valC['url'];
                }

                break;

            // Specifications Module + Image
            case 'specifications_modules':

                $arrayAux['type'] = 'specifications';
                $img_back = get_field('central_image_desktop', $post_id);
                $arrayAux['img_desktop'] = $img_back['url'];

                $img_back = get_field('central_image_mobile', $post_id);
                $arrayAux['img_mobile'] = $img_back['url'];


                $arrayAux['list'] = array();
                foreach( get_field('block_text_list', $post_id) as $keyC => $valC ){

                    $arrayAux['list'][] = array(
                        'text' => $valC['item_list']
                    );

                }
                break;

            // Squares
            case 'squares':
                $arrayAux['type'] = 'squares';

                $arrayAux['title'] = '';

                $arrayAux['sides'] = array();
                foreach( get_field('squares_block', $post_id) as $keyC => $valC ){

                    $arrayAux['sides'][] = array(
                        'title' => $valC['page_title'],
                        'subtitle' => $valC['page_subtitle'],
                        'background_color' => $valC['background_color'],
                        'text' => $valC['block_text']
                    );

                }
                break;

            // Sticky Menu
            case 'sticky_menu':
                $arrayAux['menu_item'] = array();
                $arrayAux['title'] = '';
                $arrayAux['type'] = 'sticky-menu';
                $arrayAux['page'] = $data->get_params()['section'];
                foreach( get_field('menu_item', $post_id) as $keyC => $valC ){
                    $arrayAux['menu_item'][] = array(
                        'icon' => $valC['menu_item_icon'],
                        'title' => $valC['menu_item_title'],
                        'hash' => $valC['menu_item_hash']
                    );
                }
                break;

            // Text
            case 'text':
                $arrayAux['type'] = 'banner-text';

                $arrayAux['title_color'] = get_field('page_title_color', $post_id);
                $arrayAux['text_color'] = get_field('page_text_color', $post_id);
                $arrayAux['subtitle'] = get_field('page_subtitle', $post_id);
                $arrayAux['background_color'] = get_field('background_color', $post_id);
                break;

            // Text with background color degr
            case 'text_degr':
                $arrayAux['type'] = 'banner-simple-text';
                $arrayAux['background_color_start'] = get_field('background_color_start', $post_id);
                $arrayAux['background_color_end'] = get_field('background_color_end', $post_id);
                $arrayAux['title_color'] = get_field('page_title_color', $post_id);
                $arrayAux['text_color'] = get_field('page_text_color', $post_id);
                //die('<pre>'.print_r(get_field('left_right', $post_id),true).'</pre>');
                break;

        }

        $list_array[] = $arrayAux;

    endwhile;

    wp_reset_query();


    return $list_array;
}

add_action( 'rest_api_init', function () {
    register_rest_route( 'get', '/list/sections', array(
        'methods' => 'GET',
        'callback' => 'get_list_sections',
    ) );
} );
