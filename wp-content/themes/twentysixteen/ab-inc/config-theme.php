<?php

/**
 * Definimos la página de configuración Whirlpool
 *
 */
function theme_settings_page_gambito(){
    ?>
        <div class="wrap">
        <h1>Configuración General</h1>

        <?php
            $active_tab = isset( $_GET[ 'tab' ] ) ? $_GET[ 'tab' ] : 'general';
        ?>
         
        <h2 class="nav-tab-wrapper">
            <a href="?page=theme-panel&tab=general" class="nav-tab <?php echo $active_tab == 'general' ? 'nav-tab-active' : ''; ?>">General</a>
            <a href="?page=theme-panel&tab=home" class="nav-tab <?php echo $active_tab == 'home' ? 'nav-tab-active' : ''; ?>">Home</a>
            <a href="?page=theme-panel&tab=premios" class="nav-tab <?php echo $active_tab == 'premios' ? 'nav-tab-active' : ''; ?>">Reparto de Premios</a>
        </h2>

        <form method="post" action="options.php">
            <?php
                if( $active_tab == 'general' ) {
                    settings_fields("section");
                    do_settings_sections("theme-options"); 
                }
                if( $active_tab == 'home' ) {
                    settings_fields("section-home");
                    do_settings_sections("theme-options-home"); 
                }
                if( $active_tab == 'premios' ) {
                    settings_fields("section-premios");
                    do_settings_sections("theme-options-premios"); 
                }
            ?>

            <?php
            submit_button(); 
            ?>      
        </form>
        </div>
    <?php
}


/**
 * Añadimos el enlace en el menú de administración
 *
 */
function add_theme_menu_item(){
    add_menu_page("Gambito Settings", "Gambito Settings", "manage_options", "theme-panel", "theme_settings_page_gambito", null, 5);
}
add_action("admin_menu", "add_theme_menu_item");

/**
 * Elementos Tab General
 *
 */
    function display_contact_phone_element(){
        ?>
            <input type="text" size="50" name="contact_phone" id="contact_phone" value="<?php echo get_option('contact_phone'); ?>" />
            <span class='description'>Teléfono/s</span>
        <?php
    }

    function display_contact_email_element(){
        ?>
            <input type="text" size="50" name="contact_email" id="contact_email" value="<?php echo get_option('contact_email'); ?>" />
            <span class='description'>Email/s separados por ",".</span>
        <?php
    }

    function display_standard_logo_element() {
        ?>
            <label for="standard_logo">
            <input id="standard_logo" type="text" size="36" name="standard_logo" value="<?php echo get_option('standard_logo');?>" />
            <input id="standard_logo_button" type="button" value="Upload Image" />
            Enter an URL or upload an image for the banner.
            </label><br/>
            <?php if( get_option('standard_logo') ) : ?>
                <img width="300px" src="<?php echo get_option('standard_logo');?>" />
            <?php endif; ?>
            <br/><span class='description'>Tamaño mínimo: 500x500 px (y con fondo transparente)</span>
        <?php
    }

    function display_retina_logo_element() {
        ?>
            <label for="retina_logo">
            <input id="retina_logo" type="text" size="36" name="retina_logo" value="<?php echo get_option('retina_logo');?>" />
            <input id="retina_logo_button" type="button" value="Upload Image" />
            Enter an URL or upload an image for the banner.
            </label><br/>
            <?php if( get_option('retina_logo') ) : ?>
                <img width="300px" src="<?php echo get_option('retina_logo');?>" />
            <?php endif; ?>
            <br/><span class='description'>Tamaño mínimo: 500x500 px (y con fondo transparente)</span>
        <?php
    }

/**
 * Elementos Tab Home
 *
 */
    function display_home_video_title_element(){
        ?>
            <input type="text" size="50" name="home_video_title" id="home_video_title" value="<?php echo get_option('home_video_title'); ?>" />
            <span class='description'>Título del bloque del video</span>
        <?php
    }

    function display_home_video_url_element(){
        ?>
            <input type="text" size="50" name="home_video_url" id="home_video_url" value="<?php echo get_option('home_video_url'); ?>" />
            <span class='description'>URL de youtube del video</span>
        <?php
    }

/**
 * Elementos Tab Premios
 *
 */

    function display_prize_percent_element($args){
        $option = 'prize_percent_'.$args['id'];
        $getOption = get_option($option);
        ?>
            <input size="10" type="text" name="<?php echo $option; ?>" id="<?php echo $option; ?>" value="<?php echo $getOption; ?>" />
            <span class='description'>% (Ej: 17.50)
            <?php
            if( $getOption ):
                $totalPrize = get_option('prize_total_quantity');
                $percent = str_replace(',','.',$getOption);
                $prize = (float)(($totalPrize*$percent)/100);
                $prize = number_format($prize, 2, ',', '.');
                echo ' - ' . $prize.'€';
            endif;
            ?>
            </span>
        <?php
    }

    function display_number_prizes_total(){
        ?>
            <select id="number_prizes_total" name="number_prizes_total">
                <option disabled value="default">-Selecciona un número-</option>
            <?php
                for($i=1;$i<=100;$i++){
                    ( get_option('number_prizes_total') == $i ) ? $selected = 'selected' : $selected = '' ;
                    echo '<option '.$selected.' value="'.$i.'">'.$i.'</option>';
                }
            ?>
            </select>
            <span class='description'>Número de premios totales por torneo</span>
            
        <?php
    }

    function display_prize_total_quantity(){
        ?>
            <input type="text" name="prize_total_quantity" id="prize_total_quantity" value="<?php echo get_option('prize_total_quantity'); ?>" />
            <span class='description'>€ (Ej: 70000) </span><hr/>
        <?php
    }


/**
 * Definimos todos los campos de configuración Whirlpool
 *
 */
function display_theme_panel_fields(){
    
    // Tab General
        add_settings_section("section", "General", null, "theme-options");
        add_settings_section("section-home", "Configuración Home", null, "theme-options-home");
        add_settings_section("section-contact", "Configuración Formulario Contacto", null, "theme-options-contact");
        add_settings_section("section-smtpcontact", "Configuración SMTP Formulario Contacto", null, "theme-options-smtpcontact");
        
        add_settings_field('contact_phone',  'Teléfono/s de contacto', 'display_contact_phone_element', 'theme-options', 'section');
        add_settings_field('contact_email',  'Email/s de contacto', 'display_contact_email_element', 'theme-options', 'section');
        add_settings_field('standard_logo',  'Logo Standard version', 'display_standard_logo_element', 'theme-options', 'section');
        add_settings_field('retina_logo',  'Logo Retina version', 'display_retina_logo_element', 'theme-options', 'section');

        register_setting("section", "contact_phone");
        register_setting("section", "contact_email");
        register_setting("section", "standard_logo");
        register_setting("section", "retina_logo");

    // Tab Home
        add_settings_field('home_video_title',  'Título Video Home', 'display_home_video_title_element', 'theme-options-home', 'section-home');
        add_settings_field('home_video_url',  'URL Video Home', 'display_home_video_url_element', 'theme-options-home', 'section-home');
        register_setting("section-home", "home_video_title");
        register_setting("section-home", "home_video_url");

    // Tab Contacto
    add_settings_section("section-premios", "Configuración del reparto de premios", null, "theme-options-premios");
    
    add_settings_field("number_prizes_total", "Nº Premios", "display_number_prizes_total", "theme-options-premios", "section-premios");
    add_settings_field("prize_total_quantity", "Cantidad total", "display_prize_total_quantity", "theme-options-premios", "section-premios");
    register_setting("section-premios", "number_prizes_total");
    register_setting("section-premios", "prize_total_quantity");

    $contOp = 1;
    if( get_option('number_prizes_total') ){
        $limitOp = get_option('number_prizes_total');
        while( $contOp <= $limitOp ){
            $option = 'prize_percent_'.$contOp;
            add_settings_field($option, "Porcentaje de Premio nº".$contOp, "display_prize_percent_element", "theme-options-premios", "section-premios", array('id'=>$contOp) );
            register_setting("section-premios", $option);

            $contOp++;
        }
    }

}
add_action("admin_init", "display_theme_panel_fields");



/**
 * Cargamos el js para cargar imágenes en cualquiera de las secciones
 *
 */
function gambito_settings_admin_scripts() {

    wp_enqueue_script('wptuts-upload', get_template_directory_uri() . '/ab-inc/js/wptuts-upload.js', array( 'jquery' ), '20141010', true);
    wp_enqueue_script('jquery');
 
    wp_enqueue_script('thickbox');
    wp_enqueue_style('thickbox');

    wp_enqueue_script('media-upload');
    wp_enqueue_script('wptuts-upload');
}
add_action('admin_print_scripts', 'gambito_settings_admin_scripts');


/**
 * Cargamos los estilos para el hook anterior
 *
 */
function gambito_settings_admin_styles() {
    wp_enqueue_style('thickbox');
}
add_action('admin_print_styles', 'gambito_settings_admin_styles');



/**
 * Creamos la página de configuración Whirlpool
 *
 */
function setup_theme_admin_menus() {
    add_submenu_page( 'theme-panel', 'Configuración General', 'Configuración General', 'manage_options', 'theme-panel');
}
add_action("admin_menu", "setup_theme_admin_menus");


