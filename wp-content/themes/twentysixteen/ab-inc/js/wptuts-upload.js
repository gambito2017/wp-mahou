jQuery(document).ready(function() {

	// standard logo
	jQuery('#standard_logo_button').click(function() {
		formfield = jQuery('#standard_logo').attr('name');
		tb_show('', 'media-upload.php?type=image&amp;TB_iframe=true');
		window.send_to_editor = function(html) {
			hrefurl = jQuery(html).attr('href');
			jQuery('#standard_logo').val(hrefurl);
			tb_remove();
		}
		return false;
	});

	// retina logo
	jQuery('#retina_logo_button').click(function() {
		formfield = jQuery('#retina_logo').attr('name');
		tb_show('', 'media-upload.php?type=image&amp;TB_iframe=true');
		window.send_to_editor = function(html) {
			hrefurl = jQuery(html).attr('href');
			jQuery('#retina_logo').val(hrefurl);
			tb_remove();
		}
		return false;
	});

	// clasification_file
	jQuery('#clasification_file_button').click(function() {
		formfield = jQuery('#clasification_file').attr('name');
		tb_show('', 'media-upload.php?type=image&amp;TB_iframe=true');
		window.send_to_editor = function(html) {
			hrefurl = jQuery(html).attr('href');
			jQuery('#clasification_file').val(hrefurl);
			tb_remove();
		}
		return false;
	});

	// brochure pdf
	jQuery('#brochure_pdf_button').click(function() {
		formfield = jQuery('#brochure_pdf').attr('name');
		tb_show('', 'media-upload.php?type=image&amp;TB_iframe=true');
		window.send_to_editor = function(html) {
			hrefurl = jQuery(html).attr('href');
			jQuery('#brochure_pdf').val(hrefurl);
			tb_remove();
		}
		return false;
	});

	// favicon
	jQuery('#favicon_button').click(function() {
		formfield = jQuery('#favicon').attr('name');
		tb_show('', 'media-upload.php?type=image&amp;TB_iframe=true');
		window.send_to_editor = function(html) {
			imgurl = jQuery('img',html).attr('src');
			jQuery('#favicon').val(imgurl);
			tb_remove();
		}
		return false;
	});

	// logo_web
	jQuery('#logo_web_button').click(function() {
		formfield = jQuery('#logo_web').attr('name');
		tb_show('', 'media-upload.php?type=image&amp;TB_iframe=true');
		window.send_to_editor = function(html) {
			imgurl = jQuery('img',html).attr('src');
			jQuery('#logo_web').val(imgurl);
			tb_remove();
		}
		return false;
	});


	// pagina de faqs desktop
	jQuery('#image_page_faqs_button').click(function() {
	 formfield = jQuery('#image_page_faqs').attr('name');
	 tb_show('', 'media-upload.php?type=image&amp;TB_iframe=true');
		window.send_to_editor = function(html) {
			imgurl = jQuery('img',html).attr('src');
			jQuery('#image_page_faqs').val(imgurl);
			tb_remove();
		}
	 return false;
	});

	// pagina de faqs mobile
	jQuery('#image_page_faqs_mobile_button').click(function() {
		formfield = jQuery('#image_page_faqs_mobile').attr('name');
		tb_show('', 'media-upload.php?type=image&amp;TB_iframe=true');
		window.send_to_editor = function(html) {
			imgurl = jQuery('img',html).attr('src');
			jQuery('#image_page_faqs_mobile').val(imgurl);
			tb_remove();
		}
		return false;
	});


	// pagina de registro desktop
	jQuery('#image_page_register_button').click(function() {
		formfield = jQuery('#image_page_register').attr('name');
		tb_show('', 'media-upload.php?type=image&amp;TB_iframe=true');
		window.send_to_editor = function(html) {
			imgurl = jQuery('img',html).attr('src');
			jQuery('#image_page_register').val(imgurl);
			tb_remove();
		}
		return false;
	});

	// pagina de registro mobile
	jQuery('#image_page_register_mobile_button').click(function() {
		formfield = jQuery('#image_page_register_mobile').attr('name');
		tb_show('', 'media-upload.php?type=image&amp;TB_iframe=true');
		window.send_to_editor = function(html) {
			imgurl = jQuery('img',html).attr('src');
			jQuery('#image_page_register_mobile').val(imgurl);
			tb_remove();
		}
		return false;
	});


});

