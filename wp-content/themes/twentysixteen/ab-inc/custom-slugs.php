<?php

add_filter('query_vars', 'add_state_var', 0, 1);
function add_state_var($vars){
    $vars[] = 'type';
    return $vars;
}

function custom_rewrite_tag() {
  add_rewrite_tag('%multimedia%', '([^&]+)');
  add_rewrite_tag('%clasificaciones%', '([^&]+)');
}
add_action('init', 'custom_rewrite_tag', 10, 0);

function custom_rewrite_rule_multimedia() {
	$page = get_page_by_title( 'Multimedia' );
	if( isset($page->ID) ) add_rewrite_rule('^multimedia/([^/]*)/?','index.php?page_id='.$page->ID.'&type=$matches[1]','top');
	else{
		$pages = get_pages(array(
		    'meta_key' => '_wp_page_template',
		    'meta_value' => 'templates/multimedia-template.php'
		));

		add_rewrite_rule('^multimedia/([^/]*)/?','index.php?page_id='.$pages[0]->ID.'&type=$matches[1]','top');
	}
}
add_action('init', 'custom_rewrite_rule_multimedia', 10, 0);

function custom_rewrite_rule_clasificacion() {
	$page = get_page_by_title( 'Clasificaciones' );
	if( isset($page->ID) ) add_rewrite_rule('^clasificacion/([^/]*)/?','index.php?page_id='.$page->ID.'&type=$matches[1]','top');
	else{
		$pages = get_pages(array(
		    'meta_key' => '_wp_page_template',
		    'meta_value' => 'templates/clasificacion-template.php'
		));

		add_rewrite_rule('^clasificacion/([^/]*)/?','index.php?page_id='.$pages[0]->ID.'&type=$matches[1]','top');
	}
}
add_action('init', 'custom_rewrite_rule_clasificacion', 10, 0);