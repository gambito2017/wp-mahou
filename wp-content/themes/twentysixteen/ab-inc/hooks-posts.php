<?php

function generate_fields_auto_livescoring($post_ID){

	$thispost = get_post($post_ID);

	if (isset($thispost->post_status) && 'auto-draft' == $thispost->post_status ) {
		return;
	}

	if( 'trash' == $thispost->post_status ){
		return;
	}

	if( $thispost->post_type == 'livescoring' ){
		$tournament_asociate = get_field('tournament_asociate', $post_ID);
		$jornadas_totales = get_field('jornadas_totales', $post_ID);
		$clasificacion = get_field('clasificacion', $post_ID);

	    if( !$clasificacion && $jornadas_totales ) create_clasificacion_tournament($tournament_asociate,$jornadas_totales,$post_ID);
	    else{
	    	$errors = false;
			$message = 'El torneo <b>"'.get_the_title($post_ID).'"</b> ya tiene creado "live scoring" o no tiene asignado el número de jornadas.</a>';
			$errors .= $message;
		    update_option('my_admin_errors', $errors);
	    }
	}

    return;
}

add_action( 'save_post', 'generate_fields_auto_livescoring');


function create_orden_salida_tournament(){

}

function create_clasificacion_tournament($torneo,$jornadas,$id_livescoring){
	$errors = false;
	$keyClasificacion = 'field_593c1d7ce20ef';
	$keyPosJugador = 'field_593c1da1e20f0';
	$keyJugador = 'field_593c1ddce20f1';
	$keyPuntuacionTotal = 'field_593c1f19e20f3';
	$keyPuntuacionJornada = 'field_593c1f05e20f2';
	$keyPuntuacion = 'field_593c1f32e20f4';

	$arrayJornadasPuntuacion = array();
	for( $i=1; $i<=$jornadas; $i++){
		$arrayJornadasPuntuacion[] = array($keyPuntuacion => '-');
	}

	$listado_jugadores_inscritos = get_field('sign_players', $torneo);
	if( $listado_jugadores_inscritos ){
		//$count = 0;
		foreach( $listado_jugadores_inscritos as $keyJugadores => $jugador):

			$clasification = array(
			    $keyPosJugador => '--',
			    $keyJugador => get_the_title($jugador),
			    $keyPuntuacionTotal => '--',
			    $keyPuntuacionJornada => $arrayJornadasPuntuacion
			);
			 
			add_row($keyClasificacion, $clasification, $id_livescoring);
			//$count++;
			//if( $count == 3 ) break;
		endforeach;
	}
	else{
		$message = 'El torneo <b>"'.get_the_title($torneo).'"</b> no tiene jugadores inscritos. <a href="'.get_edit_post_link($torneo).'">Añadir jugadores</a>';
		$errors .= $message;
	    update_option('my_admin_errors', $errors);
	}

}



/********************************************/
// Display any errors
function wpse_5102_admin_notice_handler() {

    $errors = get_option('my_admin_errors');

    if($errors) {

        echo '<div class="error"><p>' . $errors . '</p></div>';

    }   

}
add_action( 'admin_notices', 'wpse_5102_admin_notice_handler' );


// Clear any errors
function wpse_5102_clear_errors() {

    update_option('my_admin_errors', '');

}
add_action( 'admin_footer', 'wpse_5102_clear_errors' );