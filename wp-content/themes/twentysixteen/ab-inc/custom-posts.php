<?php
//New "Related Products" function for WooCommerce

// Si genero un nuevo custom type, debo ir a "Ajustes -> Enlaces permanente" y darle al botón de guardar
// esto me permite borrar la cache y 
// que se vean las páginas de custom type (content-type.php y single-type.php)


// Post torneo
function torneo_custom_init(){
  $labels = array(
    'name' => _x('Torneo', 'post type general name'),
    'singular_name' => _x('Torneo', 'post type singular name'),
    'add_new' => _x('Add', 'torneo'),
    'add_new_item' => __('Add new torneo'),
    'edit_item' => __('Edit torneo'),
    'new_item' => __('New torneo'),
    'view_item' => __('View torneo'),
    'search_items' => __('Search torneos'),
    'not_found' =>  __('No torneos founded.'),
    'not_found_in_trash' => __('No torneos founded in trash'),
    'menu_name' => 'Torneos'

  );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'query_var' => true,
    'rewrite' => array('slug' => 'torneo'),
    'capability_type' => 'torneo',
    'capabilities' => array(
        'publish_posts' => 'publish_torneos',
        'edit_posts' => 'edit_torneos',
        'edit_others_posts' => 'edit_others_torneos',
        'read_private_posts' => 'read_private_torneos',
        'edit_post' => 'edit_torneo',
        'delete_post' => 'delete_torneo',
        'read_post' => 'read_torneo',
    ),
    'has_archive' => true,
    'hierarchical' => false,
    'menu_position' => null,
    'menu_icon' => 'dashicons-location',
    'supports' => array('title','editor')
  );
  register_post_type('torneo',$args);
}
add_action('init', 'torneo_custom_init');

//añadir filtro para asegurarnos que el texto Receta, o producto, aparece cuando el usuario actualiza un Receta
function torneo_updated_messages( $messages ) {
  global $post, $post_ID;

  $messages['torneo'] = array(
    0 => '', // Sin uso. Los mensajes comienzan con el índice 1.
    1 => sprintf( __('Torneo updated.'), esc_url( get_permalink($post_ID) ) ),
    2 => __('Custom field updated.'),
    3 => __('Custom field updated.'),
    4 => __('Torneo updated.'),
    5 => isset($_GET['revision']) ? sprintf( __('Torneo restaurado a revisión desde el %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
    6 => sprintf( __('Torneo published.'), esc_url( get_permalink($post_ID) ) ),
    7 => __('Torneo recorded.'),
    8 => sprintf( __('Torneo sent.'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
    9 => sprintf( __('Torneo scheduled to: <strong>%1$s</strong>.'),
      // Para formatos de fecha ver http://php.net/date
      date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
    10 => sprintf( __('Borrador de Torneo actualizada.'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
  );

  return $messages;
}
add_filter('post_updated_messages', 'torneo_updated_messages');

/*********************************************************************/

// Post slider
function slider_custom_init(){
  $labels = array(
    'name' => _x('Slider', 'post type general name'),
    'singular_name' => _x('Slider', 'post type singular name'),
    'add_new' => _x('Add', 'slider'),
    'add_new_item' => __('Add new slider'),
    'edit_item' => __('Edit slider'),
    'new_item' => __('New slider'),
    'view_item' => __('View slider'),
    'search_items' => __('Search sliders'),
    'not_found' =>  __('No sliders founded.'),
    'not_found_in_trash' => __('No sliders founded in trash'),
    'menu_name' => 'Slider Home'

  );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'query_var' => true,
    'rewrite' => array('slug' => 'slider'),
    'capability_type' => 'slider',
    'capabilities' => array(
        'publish_posts' => 'publish_sliders',
        'edit_posts' => 'edit_sliders',
        'edit_others_posts' => 'edit_others_sliders',
        'read_private_posts' => 'read_private_sliders',
        'edit_post' => 'edit_slider',
        'delete_post' => 'delete_slider',
        'read_post' => 'read_slider',
    ),
    'has_archive' => true,
    'hierarchical' => false,
    'menu_position' => null,
    'menu_icon' => 'dashicons-images-alt2',
    'supports' => array('title','editor','page-attributes')
  );
  register_post_type('slider',$args);
}
add_action('init', 'slider_custom_init');

//añadir filtro para asegurarnos que el texto Receta, o producto, aparece cuando el usuario actualiza un Receta
function slider_updated_messages( $messages ) {
  global $post, $post_ID;

  $messages['slider'] = array(
    0 => '', // Sin uso. Los mensajes comienzan con el índice 1.
    1 => sprintf( __('Slider updated.'), esc_url( get_permalink($post_ID) ) ),
    2 => __('Custom field updated.'),
    3 => __('Custom field updated.'),
    4 => __('Slider updated.'),
    5 => isset($_GET['revision']) ? sprintf( __('Slider restaurada a revisión desde el %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
    6 => sprintf( __('Slider published.'), esc_url( get_permalink($post_ID) ) ),
    7 => __('Slider recorded.'),
    8 => sprintf( __('Slider sent.'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
    9 => sprintf( __('Slider scheduled to: <strong>%1$s</strong>.'),
      // Para formatos de fecha ver http://php.net/date
      date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
    10 => sprintf( __('Borrador de Slider actualizada.'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
  );

  return $messages;
}
add_filter('post_updated_messages', 'slider_updated_messages');

/*********************************************************************/

// Post slider
function hotel_custom_init(){
  $labels = array(
    'name' => _x('Hotel', 'post type general name'),
    'singular_name' => _x('Hotel', 'post type singular name'),
    'add_new' => _x('Add', 'hotel'),
    'add_new_item' => __('Add new hotel'),
    'edit_item' => __('Edit hotel'),
    'new_item' => __('New hotel'),
    'view_item' => __('View hotel'),
    'search_items' => __('Search hotels'),
    'not_found' =>  __('No hotels founded.'),
    'not_found_in_trash' => __('No hotels founded in trash'),
    'menu_name' => 'Hoteles'

  );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'query_var' => true,
    'rewrite' => array('slug' => 'hotel'),
    'capability_type' => 'hotel',
    'capabilities' => array(
        'publish_posts' => 'publish_hotels',
        'edit_posts' => 'edit_hotels',
        'edit_others_posts' => 'edit_others_hotels',
        'read_private_posts' => 'read_private_hotels',
        'edit_post' => 'edit_hotel',
        'delete_post' => 'delete_hotel',
        'read_post' => 'read_hotel',
    ),
    'has_archive' => true,
    'hierarchical' => false,
    'menu_position' => null,
    'menu_icon' => 'dashicons-store',
    'supports' => array('title','editor',)
  );
  register_post_type('hotel',$args);
}
add_action('init', 'hotel_custom_init');

//añadir filtro para asegurarnos que el texto Receta, o producto, aparece cuando el usuario actualiza un Receta
function hotel_updated_messages( $messages ) {
  global $post, $post_ID;

  $messages['hotel'] = array(
    0 => '', // Sin uso. Los mensajes comienzan con el índice 1.
    1 => sprintf( __('Hotel updated.'), esc_url( get_permalink($post_ID) ) ),
    2 => __('Custom field updated.'),
    3 => __('Custom field updated.'),
    4 => __('Hotel updated.'),
    5 => isset($_GET['revision']) ? sprintf( __('Hotel restaurada a revisión desde el %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
    6 => sprintf( __('Hotel published.'), esc_url( get_permalink($post_ID) ) ),
    7 => __('Hotel recorded.'),
    8 => sprintf( __('Hotel sent.'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
    9 => sprintf( __('Hotel scheduled to: <strong>%1$s</strong>.'),
      // Para formatos de fecha ver http://php.net/date
      date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
    10 => sprintf( __('Borrador de Hotel actualizada.'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
  );

  return $messages;
}
add_filter('post_updated_messages', 'hotel_updated_messages');

/*********************************************************************/

// Post slider
function player_custom_init(){
  $labels = array(
    'name' => _x('Jugador', 'post type general name'),
    'singular_name' => _x('Jugador', 'post type singular name'),
    'add_new' => _x('Add', 'jugador'),
    'add_new_item' => __('Add new jugador'),
    'edit_item' => __('Edit jugador'),
    'new_item' => __('New jugador'),
    'view_item' => __('View jugador'),
    'search_items' => __('Search jugadores'),
    'not_found' =>  __('No jugadores founded.'),
    'not_found_in_trash' => __('No jugadores founded in trash'),
    'menu_name' => 'Jugadores'

  );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'query_var' => true,
    'rewrite' => array('slug' => 'player'),
    'capability_type' => 'player',
    'capabilities' => array(
        'publish_posts' => 'publish_players',
        'edit_posts' => 'edit_players',
        'edit_others_posts' => 'edit_others_players',
        'read_private_posts' => 'read_private_players',
        'edit_post' => 'edit_player',
        'delete_post' => 'delete_player',
        'read_post' => 'read_player',
    ),
    'has_archive' => true,
    'hierarchical' => false,
    'menu_position' => null,
    'menu_icon' => 'dashicons-universal-access',
    'supports' => array('title','editor',)
  );
  register_post_type('player',$args);
}
add_action('init', 'player_custom_init');

//añadir filtro para asegurarnos que el texto Receta, o producto, aparece cuando el usuario actualiza un Receta
function player_updated_messages( $messages ) {
  global $post, $post_ID;

  $messages['player'] = array(
    0 => '', // Sin uso. Los mensajes comienzan con el índice 1.
    1 => sprintf( __('Jugador updated.'), esc_url( get_permalink($post_ID) ) ),
    2 => __('Custom field updated.'),
    3 => __('Custom field updated.'),
    4 => __('Jugador updated.'),
    5 => isset($_GET['revision']) ? sprintf( __('Jugador restaurada a revisión desde el %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
    6 => sprintf( __('Jugador published.'), esc_url( get_permalink($post_ID) ) ),
    7 => __('Jugador recorded.'),
    8 => sprintf( __('Jugador sent.'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
    9 => sprintf( __('Jugador scheduled to: <strong>%1$s</strong>.'),
      // Para formatos de fecha ver http://php.net/date
      date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
    10 => sprintf( __('Borrador de Jugador actualizada.'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
  );

  return $messages;
}
add_filter('post_updated_messages', 'player_updated_messages');

/*********************************************************************/


// Patrocinadores
function patrocinador_custom_init(){
  $labels = array(
    'name' => _x('Patrocinador', 'post type general name'),
    'singular_name' => _x('Patrocinador', 'post type singular name'),
    'add_new' => _x('Add', 'patrocinador'),
    'add_new_item' => __('Add new patrocinador'),
    'edit_item' => __('Edit patrocinador'),
    'new_item' => __('New patrocinador'),
    'view_item' => __('View patrocinador'),
    'search_items' => __('Search patrocinadores'),
    'not_found' =>  __('No patrocinadores founded.'),
    'not_found_in_trash' => __('No patrocinadores founded in trash'),
    'menu_name' => 'Patrocinadores'

  );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'query_var' => true,
    'rewrite' => array('slug' => 'patrocinador'),
    'capability_type' => 'patrocinador',
    'capabilities' => array(
        'publish_posts' => 'publish_patrocinadors',
        'edit_posts' => 'edit_patrocinadors',
        'edit_others_posts' => 'edit_others_patrocinadors',
        'read_private_posts' => 'read_private_patrocinadors',
        'edit_post' => 'edit_patrocinador',
        'delete_post' => 'delete_patrocinador',
        'read_post' => 'read_patrocinador',
    ),
    'has_archive' => true,
    'hierarchical' => false,
    'menu_position' => null,
    'menu_icon' => 'dashicons-visibility',
    'supports' => array('title','page-attributes')
  );
  register_post_type('patrocinador',$args);
}
add_action('init', 'patrocinador_custom_init');

//añadir filtro para asegurarnos que el texto Receta, o producto, aparece cuando el usuario actualiza un Receta
function patrocinador_updated_messages( $messages ) {
  global $post, $post_ID;

  $messages['patrocinador'] = array(
    0 => '', // Sin uso. Los mensajes comienzan con el índice 1.
    1 => sprintf( __('Patrocinador updated.'), esc_url( get_permalink($post_ID) ) ),
    2 => __('Custom field updated.'),
    3 => __('Custom field updated.'),
    4 => __('Patrocinador updated.'),
    5 => isset($_GET['revision']) ? sprintf( __('Patrocinador restaurada a revisión desde el %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
    6 => sprintf( __('Patrocinador published.'), esc_url( get_permalink($post_ID) ) ),
    7 => __('Patrocinador recorded.'),
    8 => sprintf( __('Patrocinador sent.'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
    9 => sprintf( __('Patrocinador scheduled to: <strong>%1$s</strong>.'),
      // Para formatos de fecha ver http://php.net/date
      date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
    10 => sprintf( __('Borrador de Patrocinador actualizada.'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
  );

  return $messages;
}
add_filter('post_updated_messages', 'patrocinador_updated_messages');

/*********************************************************************/

// Live Scoring
function livescoring_custom_init(){
  $labels = array(
    'name' => _x('Live Scoring', 'post type general name'),
    'singular_name' => _x('Live Scoring', 'post type singular name'),
    'add_new' => _x('Add', 'live scoring'),
    'add_new_item' => __('Add new live scoring'),
    'edit_item' => __('Edit live scoring'),
    'new_item' => __('New live scoring'),
    'view_item' => __('View live scoring'),
    'search_items' => __('Search patrocinadores'),
    'not_found' =>  __('No patrocinadores founded.'),
    'not_found_in_trash' => __('No patrocinadores founded in trash'),
    'menu_name' => 'Live Scorings'

  );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'query_var' => true,
    'rewrite' => array('slug' => 'livescoring'),
    'capability_type' => 'livescoring',
    'capabilities' => array(
        'publish_posts' => 'publish_livescorings',
        'edit_posts' => 'edit_livescorings',
        'edit_others_posts' => 'edit_others_livescorings',
        'read_private_posts' => 'read_private_livescorings',
        'edit_post' => 'edit_livescoring',
        'delete_post' => 'delete_livescoring',
        'read_post' => 'read_livescoring',
    ),
    'has_archive' => true,
    'hierarchical' => false,
    'menu_position' => null,
    'menu_icon' => 'dashicons-backup',
    'supports' => array('title')
  );
  register_post_type('livescoring',$args);
}
add_action('init', 'livescoring_custom_init');

//añadir filtro para asegurarnos que el texto Receta, o producto, aparece cuando el usuario actualiza un Receta
function livescoring_updated_messages( $messages ) {
  global $post, $post_ID;

  $messages['livescoring'] = array(
    0 => '', // Sin uso. Los mensajes comienzan con el índice 1.
    1 => sprintf( __('Live Scoring updated.'), esc_url( get_permalink($post_ID) ) ),
    2 => __('Custom field updated.'),
    3 => __('Custom field updated.'),
    4 => __('Live Scoring updated.'),
    5 => isset($_GET['revision']) ? sprintf( __('Live Scoring restaurada a revisión desde el %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
    6 => sprintf( __('Live Scoring published.'), esc_url( get_permalink($post_ID) ) ),
    7 => __('Live Scoring recorded.'),
    8 => sprintf( __('Live Scoring sent.'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
    9 => sprintf( __('Live Scoring scheduled to: <strong>%1$s</strong>.'),
      // Para formatos de fecha ver http://php.net/date
      date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
    10 => sprintf( __('Borrador de Live Scoring actualizada.'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
  );

  return $messages;
}
add_filter('post_updated_messages', 'livescoring_updated_messages');

/*********************************************************************/