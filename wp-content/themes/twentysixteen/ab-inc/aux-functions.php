<?php


function get_user_role($role) {
	global $current_user;

	$user_roles = $current_user->roles;
	$user_role = array_shift($user_roles);

	if( $user_role == $role ) return true;
	else return false;
	// return $user_role;
}

// tiny URL
function get_tiny_url($url)  {  
	$ch = curl_init();  
	$timeout = 5;  
	curl_setopt($ch,CURLOPT_URL,'http://tinyurl.com/api-create.php?url='.$url);  
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);  
	curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);  
	$data = curl_exec($ch);  
	curl_close($ch);  
	return $data;  
}

/* returns the shortened url */
function get_bitly_short_url($url) {

	$connectURL = 'https://api-ssl.bitly.com/v3/shorten?access_token=1d9fc8e3b373b1acbff0512808c7c14630223558&longUrl='.urlencode($url).'&format=txt';
	$connectUrlTxt = file_get_contents($connectURL);

	return $connectUrlTxt;

}


// Distancia geodesica para las tiendas
function distanciaGeodesica($lat1, $long1, $lat2, $long2){

  $degtorad = 0.01745329;
  $radtodeg = 57.29577951;

  $dlong = ($long1 - $long2);
  $dvalue = (sin($lat1 * $degtorad) * sin($lat2 * $degtorad))
  + (cos($lat1 * $degtorad) * cos($lat2 * $degtorad)
  * cos($dlong * $degtorad));

  $dd = acos($dvalue) * $radtodeg;

  $miles = ($dd * 69.16);
  $km = ($dd * 111.302);

  return $km;
} 
