<?php
// Hook para mandar emails tras rellenar cualquier formulario distinto al comparador
function gambito_send_email() {
    // contacto
    if( isset($_POST['type']) && $_POST['type']=='contact' ){
        require_once get_template_directory() . '/ab-inc/phpmailer/PHPMailerAutoload.php';
        $toemails = array();
        $toemails[] = array(
                        'email' => 'm.oyola@mipiace.es,comunicacion@galeragroup.com', // Your Email Address
                        'name' => 'Gambito Golf' // Your Name
                    );

        // Form Processing Messages
        $message_success = 'Hemos recibido tu mensaje <strong>correctamente</strong>, te responderemos lo antes posible. Muchas gracias.';
        // Add this only if you use reCaptcha with your Contact Forms
        $recaptcha_secret = ''; // Your reCaptcha Secret
        $mail = new PHPMailer();
        if( $_POST['template-contactform-email'] != '' ) {

            $name = isset( $_POST['template-contactform-name'] ) ? $_POST['template-contactform-name'] : '';
            $email = isset( $_POST['template-contactform-email'] ) ? $_POST['template-contactform-email'] : '';
            $phone = isset( $_POST['template-contactform-phone'] ) ? $_POST['template-contactform-phone'] : '';
            $service = isset( $_POST['template-contactform-service'] ) ? $_POST['template-contactform-service'] : '';
            $subject = isset( $_POST['template-contactform-subject'] ) ? $_POST['template-contactform-subject'] : '';
            $message = isset( $_POST['template-contactform-message'] ) ? $_POST['template-contactform-message'] : '';

            $subject = isset($subject) ? $subject : 'Nuevo mensaje desde la web de Gambito';

            $botcheck = $_POST['template-contactform-botcheck'];

            if( $botcheck == '' ) {

                $mail->SetFrom( $email , $name );
                $mail->AddReplyTo( $email , $name );
                foreach( $toemails as $toemail ) {
                    $mail->AddAddress( $toemail['email'] , $toemail['name'] );
                }
                $mail->Subject = $subject;

                $name = isset($name) ? "Name: $name<br><br>" : '';
                $email = isset($email) ? "Email: $email<br><br>" : '';
                $phone = isset($phone) ? "Phone: $phone<br><br>" : '';
                $service = isset($service) ? "Service: $service<br><br>" : '';
                $message = isset($message) ? "Message: $message<br><br>" : '';

                $referrer = $_SERVER['HTTP_REFERER'] ? '<br><br><br>This Form was submitted from: ' . $_SERVER['HTTP_REFERER'] : '';

                $body = "$name $email $phone $service $message $referrer";

                // Runs only when File Field is present in the Contact Form
                if ( isset( $_FILES['template-contactform-file'] ) && $_FILES['template-contactform-file']['error'] == UPLOAD_ERR_OK ) {
                    $mail->IsHTML(true);
                    $mail->AddAttachment( $_FILES['template-contactform-file']['tmp_name'], $_FILES['template-contactform-file']['name'] );
                }

                $mail->MsgHTML( $body );
                $sendEmail = $mail->Send();

                $_POST['sendEmail'] = $message_success;

                return true;
            } 
        }
    }


}
add_action( 'init', 'gambito_send_email' );


