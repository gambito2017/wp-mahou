<?php


// Máximo de 4 productos complementarios
function _acf_limit_multiple_value() {
	
	?>
	<script type="text/javascript">
	(function($) {
		
		jQuery(document).ready(function($){

            // hoteles
            // $('select#acf-field_58fe495cf9ed5').chosen();

            // jugadores incritos torneo
            // $('select#acf-field_58fe4a9515e1b').chosen();


		});
		
	})(jQuery);	
	</script>
	<?php
		
}
add_action('admin_footer', '_acf_limit_multiple_value');


// libreria chosen para los campos de tipo select
function _chosen_css_js( $hook ) {
    wp_enqueue_script( 'gambito-chosen-js', get_template_directory_uri() . '/assets/js/chosen.jquery.js', array( 'jquery' ), '20141010', true);
    wp_enqueue_style( 'gambito-chosen-css', get_template_directory_uri() . '/assets/css/chosen.css', false, '1.0.0' );
}
add_action('admin_enqueue_scripts', '_chosen_css_js');


// Opciones en el campo hotel oficial de torneo
function acf_load_tournaments_hotels_field_choices( $field ) {

    // reset choices
    $field['choices'] = array();

    $choices = array();

    global $wpdb;
    $searchQuerySQL = "
      SELECT p.ID as post_id, p.post_title as post_title
      FROM wp_posts as p
      where 1
      and p.post_status = 'publish'
      and p.post_type = 'hotel'
      group by p.ID
      order by p.post_title ASC
      ";

    $searchQuery = $wpdb->get_results( $searchQuerySQL );
    foreach( $searchQuery as $keyV => $valV ):
        $localizacion = get_field('localizacion', $valV->post_id);
        $choices[$valV->post_id] = $valV->post_title.' ('.$localizacion.')';
    endforeach;

    // loop through array and add to field 'choices'
    if( is_array($choices) ) {
        foreach( $choices as $keyC => $valC ) {
            $field['choices'][ $keyC ] = $valC;
        }
    }
    wp_reset_query();

    // return the field
    return $field;
}
add_filter('acf/load_field/name=official_hotel', 'acf_load_tournaments_hotels_field_choices');


// Opciones en el campo jugadores inscritos de torneo
function acf_load_sign_players_field_choices( $field ) {

    // reset choices
    $field['choices'] = array();

    $choices = array();

    global $wpdb;
    $searchQuerySQL = "
      SELECT p.ID as post_id, p.post_title as post_title
      FROM wp_posts as p
      where 1
      and p.post_status = 'publish'
      and p.post_type = 'player'
      group by p.ID
      order by p.post_title ASC
      ";

    $searchQuery = $wpdb->get_results( $searchQuerySQL );
    foreach( $searchQuery as $keyV => $valV ):
        $nacionalidad = get_field('nacionalidad', $valV->post_id);
        $choices[$valV->post_id] = $valV->post_title.' ('.$nacionalidad.')';
    endforeach;

    // loop through array and add to field 'choices'
    if( is_array($choices) ) {
        foreach( $choices as $keyC => $valC ) {
            $field['choices'][ $keyC ] = $valC;
        }
    }
    wp_reset_query();

    // return the field
    return $field;
}
add_filter('acf/load_field/name=sign_players', 'acf_load_sign_players_field_choices');

// Opciones en el campo jugadores inscritos de torneo
function acf_load_tournament_asociate_field_choices( $field ) {

    // reset choices
    $field['choices'] = array();

    $choices = array();

    global $wpdb;
    $searchQuerySQL = "
      SELECT p.ID as post_id, p.post_title as post_title
      FROM wp_posts as p
      where 1
      and p.post_status = 'publish'
      and p.post_type = 'torneo'
      group by p.ID
      order by p.post_title ASC
      ";

    $searchQuery = $wpdb->get_results( $searchQuerySQL );
    foreach( $searchQuery as $keyV => $valV ):
        $fecha_inicio = get_field('fecha_inicio', $valV->post_id);
        $choices[$valV->post_id] = $valV->post_title.' ('.$fecha_inicio.')';
    endforeach;

    // loop through array and add to field 'choices'
    if( is_array($choices) ) {
        foreach( $choices as $keyC => $valC ) {
            $field['choices'][ $keyC ] = $valC;
        }
    }
    wp_reset_query();

    // return the field
    return $field;
}
add_filter('acf/load_field/name=tournament_asociate', 'acf_load_tournament_asociate_field_choices');