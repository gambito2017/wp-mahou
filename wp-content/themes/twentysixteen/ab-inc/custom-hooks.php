<?php
/*****************    ACCIONES CON POSTS    ******************/
/*
function check_status_post($post_ID, $post_after, $post_before){
    // si no estamos eliminando o restaurando, podemos jugar con los status

    if( $post_before->post_status == 'draft' && $post_after->post_status != 'publish' ){
        // creamos un post
    }

    if( $post_before->post_status == 'publish' && $post_after->post_status != 'publish' ){
        // actualizamos un post
    }

    if( $post_before->post_status == 'draft' && $post_after->post_status != 'publish' ){
        // pasamos de borrador a publico un post
    }
}

add_action( 'post_updated', 'check_status_post', 10, 3 );
*/

/*****************    GENERAL    ******************/


    // Cargamos la libreria CSS de Font Awesome
    function font_admin_init() {
       wp_enqueue_style('font-awesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css'); 
    }
    add_action('admin_init', 'font_admin_init');

    // Remove the html filtering
    remove_filter( 'pre_term_description', 'wp_filter_kses' );
    remove_filter( 'term_description', 'wp_kses_data' );

    // Editamos el editor de texto
    function customizeEditor($in) {
      $in['remove_linebreaks']=false;
      $in['remove_redundant_brs'] = false;
      $in['wpautop']=false;
      $in['convert_newlines_to_brs']=true;

      return $in;
    }
    add_filter('tiny_mce_before_init', 'customizeEditor');


    // Editar wysiwyg
    // https://codex.wordpress.org/Plugin_API/Filter_Reference/mce_buttons,_mce_buttons_2,_mce_buttons_3,_mce_buttons_4
    function my_mce_buttons($buttons) {   
        $buttons = array('bold,italic,underline');
        //$buttons = array();
        return $buttons;
    }
    function my_mce_buttons_2($buttons) {   
        $buttons = array();
        return $buttons;
    }
    //add_filter('mce_buttons', 'my_mce_buttons');
    //add_filter('mce_buttons_2', 'my_mce_buttons_2');



    // Editar los WYSIWYG
    add_filter( 'acf/fields/wysiwyg/toolbars' , 'my_toolbars'  );
    function my_toolbars( $toolbars ){

        // Add a new toolbar called "Very Simple"
        // - this toolbar has only 1 row of buttons
        $toolbars['Very Simple' ] = array();
        $toolbars['Very Simple' ][1] = array('bold');

        $toolbars['Basic' ] = array();
        $toolbars['Basic' ][1] = array('bold' , 'italic' , 'underline', 'bullist', 'numlist' );

        // Edit the "Full" toolbar and remove 'code'
        // - delet from array code from http://stackoverflow.com/questions/7225070/php-array-delete-by-value-not-key
        if( ($key = array_search('code' , $toolbars['Full' ][2])) !== false )
        {
            //unset( $toolbars['Full' ][2][$key] );
        }

        // remove the 'Basic' toolbar completely
        //unset( $toolbars['Basic' ] );

        // return $toolbars - IMPORTANT!
        return $toolbars;
    }

    // Javascript General
    function ab_admin_cms() {
        wp_enqueue_script('ab-ajax-admin', get_template_directory_uri() . '/ab-inc/js/admin-ab.js', array( 'jquery' ), '20141010', true);
        wp_localize_script('ab-ajax-admin', 'mylocalizedscript', array('myurl' => get_template_directory_uri()));
    }
    add_action('edit_form_advanced', 'ab_admin_cms');


    // Edito el TinyMCE para FAQs
    function disabled_tinymce_editor_fields_faqs( $settings, $editor_id ) {
        if ( $editor_id === 'content' && get_current_screen()->post_type === 'faq' ) {
            $settings['tinymce']['toolbar1'] = 'bold,italic,link,unlink';
            $settings['tinymce']['teeny'] = true;
        }
        return $settings;
    }
    add_filter( 'wp_editor_settings', 'disabled_tinymce_editor_fields_faqs', 10, 2 );


remove_action( 'wp_head', '_wp_render_title_tag', 1 );
/**
 * meta tags y description de la web
 *
 */
function add_meta_tags() {
    global $post;

    // home
    if( is_front_page() ){
        $description_seo = strip_tags(get_option('desc_home'));
        $title_seo = get_bloginfo('name').' | '.get_bloginfo('description');
    }

    // 404
    if( is_404() ){
        $description_seo = strip_tags(get_option('desc_home'));
        $title_seo = '404 | '.get_bloginfo('name');
    }

    // tag
    if( is_tag() ){
        $term_slug = get_query_var( 'term' );
        $taxonomyName = get_query_var( 'taxonomy' );
        $current_term = get_term_by( 'slug', $term_slug, $taxonomyName );

        $description_seo = strip_tags(get_option('desc_home'));
        $title_seo = 'Tag: '.single_term_title('',false).' | '.get_bloginfo('name');
    }

    // cualquier tipo de página o plantilla
    if( is_page() ){
        $title_seo = $post->post_title.' | '.get_bloginfo('name');

        $meta = strip_shortcodes( $post->post_content );
        $meta = str_replace( array("\n", "\r", "\t"), ' ', $meta );
        $meta = substr( $meta, 0, 250 );
        $description_seo = strip_tags($meta);
    }



    // torneo o jugador
    if ( is_singular( 'torneo' ) || is_singular( 'player' ) || is_single() ) {

        $meta = strip_shortcodes( $post->post_content );
        $meta = str_replace( array("\n", "\r", "\t"), ' ', $meta );
        $meta = substr( $meta, 0, 250 );
        $description_seo = strip_tags($meta);

        $title_seo = $post->post_title.' | '.get_bloginfo('name');

        // Twitter
        echo '<meta name="twitter:card" content="product" />' . "\n";
        echo '<meta name="twitter:site" content="@GambitoGolf" />' . "\n";
        echo '<meta name="twitter:title" content="'.$title_seo.'" />' . "\n";
        echo '<meta name="twitter:description" content="'.$description_seo.'" />' . "\n";
        if( get_field('cartel_torneo', $post->ID) ) $imagePr = get_field('cartel_torneo', $post->ID);
        if( get_field('player_image', $post->ID) ) $imagePr = get_field('player_image', $post->ID);
        if( get_field('main_image', $post->ID) ) $imagePr = get_field('main_image', $post->ID);
        echo '<meta name="twitter:image" content="'.$imagePr['url'].'" />' . "\n";
        echo '<meta name="twitter:domain" content="https://gambitogolf.es/" />' . "\n";

        // Facebook
        echo '<meta name="og:title" content="'.$title_seo.'" />' . "\n";
        echo '<meta name="og:type" content="product" />' . "\n";
        echo '<meta name="og:image" content="'.$imagePr['url'].'" />' . "\n";
        echo '<meta name="og:url" content="'.get_the_permalink($post->ID).'" />' . "\n";
        echo '<meta name="og:description" content="'.$description_seo.'" />' . "\n";
        echo '<meta name="og:site_name" content="'.get_bloginfo('name').'" />' . "\n";
    }


    echo '<title>'.$title_seo.'</title>';
    if( $description_seo!='' ) echo '<meta name="description" content="'.$description_seo.'" />' . "\n";
}
add_action( 'wp_head', 'add_meta_tags' , 2 );