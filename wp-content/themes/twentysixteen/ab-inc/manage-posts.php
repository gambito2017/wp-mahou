<?php

/**
 * Columnas en la página de administración de banners en el CMS
 *
 * @param array columnas
 * @return array columnas
 */
add_filter( "manage_edit-post_columns",          '_post_custom_columns_admin_list_page' );
add_filter( "manage_post_posts_custom_column",         '_post_fields_admin_list_page', 10, 2);
function _post_custom_columns_admin_list_page( $new_columns ) {
    $new_columns = array(
        'cb' => '<input type="checkbox" />',
        'title'   => 'Título',
        'main_image' => 'Imagen',
        'featured' => 'Noticia Destacada',
        'categories'  => 'Categoría',
        'tags'  => 'Etiquetas',
        'date'  => 'Fecha',
        'author'  => 'Autor'
    );

    return $new_columns;
}

/**
 * Definir los valores de las columnas customizadas
 */
function _post_fields_admin_list_page( $column_name, $post_id ) {
    switch($column_name){

        case 'featured': $featured = get_field('featured', $post_id);
            $featured = ( $featured ) ? 'Destacada' : '--';
            echo $featured;
            break;

        case 'main_image':
            if( get_field('main_image', $post_id) ) $main_image = get_field('main_image', $post_id);
            $img = $main_image['sizes']['admin_thumb'];
            echo '<img src="' . $img . '" />';
            break;
    }
}


/**
 * Columnas en la página de administración de banners en el CMS
 *
 * @param array columnas
 * @return array columnas
 */
add_filter( "manage_edit-torneo_columns",          '_torneo_custom_columns_admin_list_page' );
add_filter( "manage_torneo_posts_custom_column",         '_torneo_fields_admin_list_page', 10, 2);
function _torneo_custom_columns_admin_list_page( $new_columns ) {
    $new_columns = array(
        'cb' => '<input type="checkbox" />',
        'title'   => 'Título',
        'status_torneo' => 'Activo',
        'tournament_type' => 'Tipo',
        'cartel_torneo' => 'Cartel',
        'fecha_inicio' => 'Fecha Inicio',
        'date'  => 'Fecha',
        'author'  => 'Autor'
    );

    return $new_columns;
}

/**
 * Definir los valores de las columnas customizadas
 */
function _torneo_fields_admin_list_page( $column_name, $post_id ) {
    switch($column_name){
        case 'status_torneo': $status_torneo = get_field('status_torneo', $post_id);
            echo ($status_torneo) ? 'Activo' : 'Desactivado';
            break;

        case 'tournament_type': $tournament_type = get_field('tournament_type', $post_id);
            echo $tournament_type;
            break;

        case 'cartel_torneo':
            if( get_field('cartel_torneo', $post_id) ) $cartel_torneo = get_field('cartel_torneo', $post_id);
            $img = $cartel_torneo['sizes']['admin_thumb'];
            echo '<img src="' . $img . '" />';
            break;

        case 'fecha_inicio': $fecha_inicio = get_field('fecha_inicio', $post_id);
            echo $fecha_inicio;
            break;
    }
}


/**
 * Columnas en la página de administración de mosaicos en el CMS
 *
 * @param array columnas
 * @return array columnas
 */
add_filter( "manage_edit-slider_columns",          '_slider_custom_columns_admin_list_page' );
add_filter( "manage_slider_posts_custom_column",         '_slider_fields_admin_list_page', 10, 2);
function _slider_custom_columns_admin_list_page( $new_columns ) {
    $new_columns = array(
        'cb' => '<input type="checkbox" />',
        'title'   => 'Título',
        'order_post' => 'Orden',
        'imagen_slider' => 'Imagen',
        'pretitle' => 'Pretítulo',
        'subtitle' => 'Subtítulo',
        'info_extra' => 'Información Extra',
        'date'  => 'Fecha',
        'author'  => 'Autor'
    );

    return $new_columns;
}

/**
 * Definir los valores de las columnas customizadas
 */
function _slider_fields_admin_list_page( $column_name, $post_id ) {
    switch($column_name){
        case 'order_post': $order_post = get_post_field( 'menu_order', $post_id);
            echo $order_post;
            break;

        case 'tipo_mosaico': $zona_accion = get_field('zona_accion', $post_id);
            echo ucfirst($zona_accion);
            break;

        case 'imagen_slider': $imagen_slider = get_field('imagen_slider', $post_id);
            $img = $imagen_slider['sizes']['admin_thumb'];
            echo '<img src="' . $img . '" />';
            break;

        case 'pretitle': $pre_titulo_slider = get_field('pretitle', $post_id);
            $pre_titulo_slider = ( $pre_titulo_slider ) ? $pre_titulo_slider : '--';
            echo $pre_titulo_slider;
            break;

        case 'subtitle': $subtitulo_slider = get_field('subtitulo_slider', $post_id);
            $subtitulo_slider = ( $subtitulo_slider ) ? $subtitulo_slider : '--';
            echo $subtitulo_slider;
            break;

        case 'info_extra': $info_extra = get_field('info_slider', $post_id);
            $info_extra = ( $info_extra ) ? $info_extra : '--';
            echo $info_extra;
            break;
    }
}


/**
 * Columnas en la página de administración de jugadores en el CMS
 *
 * @param array columnas
 * @return array columnas
 */
add_filter( "manage_edit-hotel_columns",          '_hotel_custom_columns_admin_list_page' );
add_filter( "manage_hotel_posts_custom_column",         '_hotel_fields_admin_list_page', 10, 2);
function _hotel_custom_columns_admin_list_page( $new_columns ) {
    $new_columns = array(
        'cb' => '<input type="checkbox" />',
        'title'   => 'Título',
        'hotel_image' => 'Imagen',
        'localizacion' => 'Localizacion',
        'date'  => 'Fecha',
        'author'  => 'Autor'
    );

    return $new_columns;
}

/**
 * Definir los valores de las columnas customizadas
 */
function _hotel_fields_admin_list_page( $column_name, $post_id ) {
    switch($column_name){

        case 'hotel_image': $hotel_image = get_field('hotel_image', $post_id);
            $img = $hotel_image['sizes']['admin_thumb'];
            echo '<img src="' . $img . '" />';
            break;

        case 'localizacion': $localizacion = get_field('localizacion', $post_id);
            echo $localizacion;
            break;
    }
}



/**
 * Columnas en la página de administración de jugadores en el CMS
 *
 * @param array columnas
 * @return array columnas
 */
add_filter( "manage_edit-player_columns",          '_player_custom_columns_admin_list_page' );
add_filter( "manage_player_posts_custom_column",         '_player_fields_admin_list_page', 10, 2);
function _player_custom_columns_admin_list_page( $new_columns ) {
    $new_columns = array(
        'cb' => '<input type="checkbox" />',
        'title'   => 'Título',
        'player_image' => 'Imagen',
        'nacionalidad' => 'Nacionalidad',
        'date'  => 'Fecha',
        'author'  => 'Autor'
    );

    return $new_columns;
}

/**
 * Definir los valores de las columnas customizadas
 */
function _player_fields_admin_list_page( $column_name, $post_id ) {
    switch($column_name){

        case 'player_image': $player_image = get_field('player_image', $post_id);
            $img = $player_image['sizes']['admin_thumb'];
            echo '<img src="' . $img . '" />';
            break;

        case 'nacionalidad': $nacionalidad = get_field('nacionalidad', $post_id);
            echo $nacionalidad;
            break;
    }
}

/**
 * Añadimos un enlace en el menú de jugadores para exportarlos
 *
 * @param array views
 * @return array views
 */
function _add_menu_link_export_player( $views ) {
    global $current_screen;
    $views['export_player'] = '<a href="'.admin_url( 'admin-post.php?action=export_player' ).'">Exportar jugadores (.xls)</a>';
    return $views;
}
add_filter( "views_edit-player" , '_add_menu_link_export_player', 10, 1);

/**
 * Función para exportar comentarios
 */
function players_export_xls(){
    if ( ! current_user_can( 'manage_options' ) )
        return;

    header('Content-type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename=jugadores.xls');
    header('Pragma: no-cache');

    global $wpdb;
    global $post;
    $args = array( 
        'post_type' => 'player', 
        'post_status' => 'publish',
        'nopaging' => true
    );

    $loopRanking = new WP_Query( $args );
    $arrayRanking = array();
    while ( $loopRanking->have_posts() ) : $loopRanking->the_post();
      $arrayRanking[] = array(
          'id'=>$post->ID,
          'ranking'=> (get_field('ranking_actual', $post->ID)) ? get_field('ranking_actual', $post->ID) : '---'
      );
    endwhile;
    wp_reset_query();

    usort($arrayRanking,function($a,$b){
        $a = $a['ranking'];
        $b = $b['ranking'];

        $a = str_replace('.','',$a);
        $a = str_replace('€','',$a);
        $b = str_replace('.','',$b);
        $b = str_replace('€','',$b);
        return ($a*10) - ($b*10);
    });
    $arrayRanking = array_reverse($arrayRanking);

    //die($arrayRanking);

    echo 'ID' . "\t" . 'Nombre' . "\t" . 'URL' . "\t" . 'Imagen' . "\t" . 'Nacionalidad' . "\t" .
        'Ranking' . "\t" . 'Fecha nacimiento' . "\t" .
        'Lugar nacimiento' . "\t" . 'Club' . "\t" . 'Talla' . "\n";

    foreach ( $arrayRanking as $keyR => $valueR ) : 
        $count = $keyR+1;
        $title = utf8_decode(get_the_title($valueR['id']));
        $url = get_the_permalink($valueR['id']);
        $image = get_field('player_image', $valueR['id']);
        $nacionalidad = utf8_decode(get_field('nacionalidad', $valueR['id']));

        $ranking_actual = utf8_decode(get_field('ranking_actual', $valueR['id']));
        $ranking_actual = str_replace('?','',$ranking_actual);
        $birth_date = get_field('birth_date', $valueR['id']);
        $birth_place = utf8_decode(get_field('birth_place', $valueR['id']));
        $player_club = utf8_decode(get_field('player_club', $valueR['id']));
        $player_size = get_field('player_size', $valueR['id']);

        echo $valueR['id'] . "\t" . $title . "\t" . $url . "\t" . $image['url'] . "\t" . $nacionalidad . "\t" .
            $ranking_actual . "\t" . $birth_date . "\t" .
            $birth_place . "\t" . $player_club . "\t" . $player_size . "\n";

        $count++;
    endforeach;


    exit;
}
add_action( 'admin_post_export_player', 'players_export_xls' );

/**
 * Columnas en la página de administración de jugadores en el CMS
 *
 * @param array columnas
 * @return array columnas
 */
add_filter( "manage_edit-patrocinador_columns",          '_patrocinador_custom_columns_admin_list_page' );
add_filter( "manage_patrocinador_posts_custom_column",         '_patrocinador_fields_admin_list_page', 10, 2);
function _patrocinador_custom_columns_admin_list_page( $new_columns ) {
    $new_columns = array(
        'cb' => '<input type="checkbox" />',
        'title'   => 'Título',
        'patrocinador_category' => 'Categoría',
        'imagen_patrocinador_grey' => 'Imagen Grises',
        'imagen_patrocinador_color' => 'Imagen Colores',
        'menu_order' => 'Orden',
        'date'  => 'Fecha',
        'author'  => 'Autor'
    );

    return $new_columns;
}

/**
 * Definir los valores de las columnas customizadas
 */
function _patrocinador_fields_admin_list_page( $column_name, $post_id ) {
    switch($column_name){

        case 'imagen_patrocinador_grey': $imagen_patrocinador_grey = get_field('imagen_patrocinador_grey', $post_id);
            $img = $imagen_patrocinador_grey['sizes']['admin_thumb'];
            echo '<img src="' . $img . '" />';
            break;

        case 'imagen_patrocinador_color': $imagen_patrocinador_color = get_field('imagen_patrocinador_color', $post_id);
            $img = $imagen_patrocinador_color['sizes']['admin_thumb'];
            echo ( $imagen_patrocinador_color ) ? '<img src="' . $img . '" />' : '--';
            break;

        case 'patrocinador_category': $patrocinador_category = get_field('patrocinador_category', $post_id);
            echo $patrocinador_category;
            break;

        case 'menu_order': $thispost = get_post($post_id);
            echo $thispost->menu_order;
            break;
    }
}



/**
 * Columnas en la página de administración de live scoring en el CMS
 *
 * @param array columnas
 * @return array columnas
 */
add_filter( "manage_edit-livescoring_columns",          '_livescoring_custom_columns_admin_list_page' );
add_filter( "manage_livescoring_posts_custom_column",         '_livescoring_fields_admin_list_page', 10, 2);
function _livescoring_custom_columns_admin_list_page( $new_columns ) {
    $new_columns = array(
        'cb' => '<input type="checkbox" />',
        'title'   => 'Título',
        'torneo'   => 'Torneo',
        'status'   => 'Estado',
    );

    return $new_columns;
}

/**
 * Definir los valores de las columnas customizadas
 */
function _livescoring_fields_admin_list_page( $column_name, $post_id ) {
    switch($column_name){

        case 'torneo': $tournament_asociate = get_field('tournament_asociate', $post_id);
            $thispost = get_post($tournament_asociate);
            echo $thispost->post_title;
            break;

        case 'status': $status_live_scoring = get_field('status_live_scoring', $post_id);
            echo $status_live_scoring;
            break;
    }
}


/**
 * Añado un filtro en la página de administración del contenido en el CMS
 *
 */
function restrict_post_listings_by_fields() {
    global $typenow;
    global $wp_query;

    // Custom Post "Torneo"
    if ($typenow=='torneo') {

        $arrayZonas = array('Profesionales','Premium','Mahou','Meliá');
        echo "<select name='tournament_type' id='tournament_type' class='postform'>";
        echo "<option value=''>Todos los tipos</option>";
        foreach( $arrayZonas as $keyT => $valT ):
            $selected = '';
            if( isset($_GET['tournament_type']) && $_GET['tournament_type']==$valT ) $selected = 'selected="selected"';
            echo '<option value="'. $valT .'" '.$selected.'>' . ucfirst($valT) .'</option>';
        endforeach;
        echo "</select>";

        $arrayStatus = array('Desactivado','Activado');
        echo "<select name='status_torneo' id='status_torneo' class='postform'>";
        echo "<option value=''>Todos los status</option>";
        foreach( $arrayStatus as $keyT => $valT ):
            $selected = '';
            if( isset($_GET['status_torneo']) && $_GET['status_torneo']==$keyT ) $selected = 'selected="selected"';
            echo '<option value="'. $keyT .'" '.$selected.'>' . ucfirst($valT) .'</option>';
        endforeach;
        echo "</select>";
    }

    // Custom Post "Torneo"
    if ($typenow=='post') {

        $arrayDest = array('No destacadas','Destacadas');
        echo "<select name='featured' id='featured' class='postform'>";
        echo "<option value=''>Todos las noticias</option>";
        foreach( $arrayDest as $keyT => $valT ):
            $selected = '';
            if( isset($_GET['featured']) && $_GET['featured']==$keyT ) $selected = 'selected="selected"';
            echo '<option value="'. $keyT .'" '.$selected.'>' . ucfirst($valT) .'</option>';
        endforeach;
        echo "</select>";
    }

    // Custom Post "Patrocinador"
    if ($typenow=='patrocinador') {

        $arrayDest = array('Birdie','Par','Colaboradores');
        echo "<select name='patrocinador_category' id='patrocinador_category' class='postform'>";
        echo "<option value=''>Todos las categorias</option>";
        foreach( $arrayDest as $keyT => $valT ):
            $selected = '';
            if( isset($_GET['patrocinador_category']) && $_GET['patrocinador_category']==$valT ) $selected = 'selected="selected"';
            echo '<option value="'. $valT .'" '.$selected.'>' . ucfirst($valT) .'</option>';
        endforeach;
        echo "</select>";
    }

}
add_action('restrict_manage_posts','restrict_post_listings_by_fields');


/**
 * Buscar en post por otros parámetros
 *
 * @param array query info
 */
function custom_search_query( $query ) {

    if( is_admin() ){
        if( $query->query['post_type'] == 'torneo' ){

            $meta_query = array('relation' => 'AND');
            if ( isset($_GET['tournament_type']) ) {
                
                array_push($meta_query, array(
                    'key' => 'tournament_type',
                    'value' => $_GET['tournament_type'],
                    'compare' => 'LIKE'
                ));

                
            };

            if ( isset($_GET['status_torneo']) ) {
                array_push($meta_query, array(
                    'key' => 'status_torneo',
                    'value' => $_GET['status_torneo'],
                    'compare' => 'LIKE'
                ));
            };

            $query->set("meta_query", $meta_query);

        }

        if( $query->query['post_type'] == 'post' ){

            if ( isset($_GET['featured']) ) {
                $meta_query = array('relation' => 'AND');
                array_push($meta_query, array(
                    'key' => 'featured',
                    'value' => $_GET['featured'],
                    'compare' => 'LIKE'
                ));

                $query->set("meta_query", $meta_query);
            };

        }

        if( $query->query['post_type'] == 'patrocinador' ){

            if ( isset($_GET['patrocinador_category']) ) {
                $meta_query = array('relation' => 'AND');
                array_push($meta_query, array(
                    'key' => 'patrocinador_category',
                    'value' => $_GET['patrocinador_category'],
                    'compare' => 'LIKE'
                ));

                $query->set("meta_query", $meta_query);
            };

        }
    }

}
add_filter( "pre_get_posts", "custom_search_query");
