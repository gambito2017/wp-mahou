<?php
/**
 * Filtro de productos
 *
 * @request page int número de página
 * @request filters array con los filtros seleccionados
 * @request cat_slug string texto con el slug de la categoria o subcategoria
 *
 * @return html con los productos filtrados
 */
add_action( 'wp_ajax_nopriv_test_ajax', 'test_ajax' );
add_action( 'wp_ajax_test_ajax', 'test_ajax' );
function test_ajax() {

	$pagination = get_option('pagination_web_load_more');
	$offset = 4;
	if( $_REQUEST['page'] == '1' ){
		$paged = 1;
	}
	else{
		$paged = $_REQUEST['page'];
		$offset = $offset+($pagination*($paged-1));
	}

	$htmlFilter = 'hola';


	if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
		echo $htmlFilter;
		wp_reset_postdata();
		die();
	}
}




/**
 * Cargamos los archivos necesarios para llamadas AJAX
 *
 * (se carga el archivo js)
 * (se crea una variable "ajax_url" con la url del archivo ajax de WordPress, es esencial para hacer llamadas por AJAX)
 *
 */
add_action( 'wp_enqueue_scripts', 'gambito_ajax_enqueue_scripts' );
function gambito_ajax_enqueue_scripts() {

	wp_localize_script( 'functions', 'gambitoAjax', array(
		'ajax_url' => admin_url( 'admin-ajax.php' )
	));

}


