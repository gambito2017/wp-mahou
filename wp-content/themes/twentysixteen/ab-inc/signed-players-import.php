<?php

/**
 * Definimos la página de configuración Whirlpool
 *
 */
function clasifications_import_theme_settings_page_mahou(){

    if( get_option('tournament_list_import') && get_option('clasification_file') && get_option('clasification_type_category') ){
        $torneo = get_option('tournament_list_import');
        $category = get_option('clasification_type_category');
        $file = get_option('clasification_file');
        add_clasifications_to_tournament($torneo,$file,$category);
    }

    ?>
        <div class="wrap">
        <h1>Importación de Clasificaciones a un torneo</h1>

        <?php
            $active_tab = isset( $_GET[ 'tab' ] ) ? $_GET[ 'tab' ] : 'clasifications';
        ?>
         

        <form method="post" action="options.php">
            <?php
                    settings_fields("section-clasifications");
                    do_settings_sections("theme-options-clasifications");
            ?>

            <?php
            submit_button(); 
            ?>      
        </form>
        </div>
    <?php

    
}


/**
 * Añadimos el enlace en el menú de administración
 *
 */
function add_theme_menu_item_player_import(){
    add_menu_page("Importar Clasificaciones", "Importar Clasificaciones", "manage_options", "theme-panel-clasifications-import", "clasifications_import_theme_settings_page_mahou", null, 5);
}
add_action("admin_menu", "add_theme_menu_item_player_import");


    function display_tournament_list_import_element(){
        ?>
        <select id="tournament_list_import" name="tournament_list_import">
            <option selected disabled value="default">-Selecciona un torneo-</option>
            <?php
            global $post;
            $args = array( 
                'post_type' => 'torneo', 
                'post_status' => 'publish',
                'orderby' => 'post_title',
                'order' => 'ASC'
            );
            $args['nopaging'] = true;

            $loopTorneos = new WP_Query( $args );
            while ( $loopTorneos->have_posts() ) : $loopTorneos->the_post();
                $type = get_field('tournament_type', $post->ID);
                $fecha_inicio_original = get_field('fecha_inicio', $post->ID);
                $fechaStr = strtotime($fecha_inicio_original);

                $selected = ( get_option('tournament_list_import') == $post->ID ) ? 'selected' : '' ;

                echo '<option '.$selected.' value="'.$post->ID.'">'.$post->post_title.' - '.$type.' ('.date('d-m-y',$fechaStr).')</option>';
            endwhile;
            ?>
        </select><br/>
        <span class='description'>Listado de torneos.</span>

        <?php
    }

    function display_clasification_type_category_element(){

        ?>
        <select id="clasification_type_category" name="clasification_type_category">
            <option selected disabled value="default">-Selecciona una categoria-</option>
            <option <?php echo ( get_option('clasification_type_category') == 'primera_cat_masc' ) ? 'selected': ''; ?> value="primera_cat_masc">1 Cat Masculina</option>
            <option <?php echo ( get_option('clasification_type_category') == 'seg_cat_masc' ) ? 'selected': ''; ?> value="seg_cat_masc">2 Cat Masculina</option>
            <option <?php echo ( get_option('clasification_type_category') == 'cat_femenina' ) ? 'selected': ''; ?> value="cat_femenina">Cat Femenina</option>
            <option <?php echo ( get_option('clasification_type_category') == 'cat_inv_mahou' ) ? 'selected': ''; ?> value="cat_inv_mahou">Cat Invitados</option>
        </select>

        <?php

    }

    function display_clasification_file_element(){

        ?>
        <input id="clasification_file" type="text" size="36" name="clasification_file" value="<?php echo get_option('clasification_file');?>" />
        <input id="clasification_file_button" type="button" value="Upload File" />
        Enter an URL or upload a file for the banner.
        </label><br/>

        <br/><span class='description'>Formato .xls o .xlsx</span>
        <br/><span class='description'><b>Eliminar cualquier fila en blanco.</b></span>
        <br/><span class='description'>El formato de las columnas debe de ser el siguiente: <b>POSITION | NAME | TOTAL</b></span>
        <hr/>

        <?php

    }

    


/**
 * Definimos todos los campos de configuración Whirlpool
 *
 */
function clasification_import_display_theme_panel_fields(){


    // Tab Players
    add_settings_section("section-clasifications", "Importación de clasificaciones en los torneos", null, "theme-options-clasifications");

    add_settings_field("tournament_list_import", "Torneos", "display_tournament_list_import_element", "theme-options-clasifications", "section-clasifications");
    add_settings_field("clasification_type_category", "Categoría Clasificación", "display_clasification_type_category_element", "theme-options-clasifications", "section-clasifications");
    add_settings_field("clasification_file", "Archivo", "display_clasification_file_element", "theme-options-clasifications", "section-clasifications");
    register_setting("section-clasifications", "tournament_list_import");
    register_setting("section-clasifications", "clasification_type_category");
    register_setting("section-clasifications", "clasification_file");



}
add_action("admin_init", "clasification_import_display_theme_panel_fields");



/**
 * Cargamos el js para cargar imágenes en cualquiera de las secciones
 *
 */
function clasifications_import_mahou_settings_admin_scripts() {

    wp_enqueue_script('wptuts-upload', get_template_directory_uri() . '/ab-inc/js/wptuts-upload.js', array( 'jquery' ), '20141010', true);
    wp_enqueue_script('jquery');
 
    wp_enqueue_script('thickbox');
    wp_enqueue_style('thickbox');

    wp_enqueue_script('media-upload');
    wp_enqueue_script('wptuts-upload');
}
add_action('admin_print_scripts', 'clasifications_import_mahou_settings_admin_scripts');


/**
 * Cargamos los estilos para el hook anterior
 *
 */
function clasifications_import_mahou_settings_admin_styles() {
    wp_enqueue_style('thickbox');
}
add_action('admin_print_styles', 'clasifications_import_mahou_settings_admin_styles');



function _js_config_theme_clasifications_import_mahou() {
    
    ?>
    <script type="text/javascript">
    (function($) {
        
        jQuery(document).ready(function($){

            // hoteles
            $('select#tournament_list_import').chosen();


        });
        
    })(jQuery); 
    </script>
    <?php
        
}
add_action('admin_footer', '_js_config_theme_clasifications_import_mahou');

function _chosen_css_js_player_inscription( $hook ) {
    wp_enqueue_script( 'gambito-chosen-js', get_template_directory_uri() . '/assets/js/chosen.jquery.js', array( 'jquery' ), '20141010', true);
    wp_enqueue_style( 'gambito-chosen-css', get_template_directory_uri() . '/assets/css/chosen.css', false, '1.0.0' );
}
add_action('admin_enqueue_scripts', '_chosen_css_js_player_inscription');

function _styles_config_theme_gambito_player_inscription() {
    ?>
    <style>
    </style>
    <?php
}
add_action( 'admin_head', '_styles_config_theme_gambito_player_inscription' );

function add_clasifications_to_tournament($idTorneo, $archivoxls, $categoria){

    require_once get_template_directory() . '/ab-inc/phpexcel/Classes/PHPExcel/IOFactory.php';

    //echo $idTorneo.'<br/>';
    //echo $archivoxls.'<br/>';
    //echo $categoria.'<br/>';

    if( get_field($categoria, $idTorneo) ){
        echo '<h2 style="color:red;">Error: El torneo ya tiene cargados clasificaciones en la categoría seleccionada.</h2>';
        delete_option('tournament_list_import');
        delete_option('clasification_file');
        delete_option('clasification_type_category');
        return true;
    }

    $ext = pathinfo($archivoxls, PATHINFO_EXTENSION);
    if( $ext != 'xls' && $ext != 'xlsx' ){
        echo '<h2 style="color:red;">Error: El formato del archivo es incorrecto. Debe de ser ".xls" o ".xlsx"</h2>';
        delete_option('tournament_list_import');
        delete_option('clasification_file');
        delete_option('clasification_type_category');
        return true;
    }


    // generamos un xls auxiliar
    file_put_contents('excel_aux.xls', file_get_contents($archivoxls) );
    $objTpl = PHPExcel_IOFactory::load("excel_aux.xls");
    $objTpl->setActiveSheetIndex(0);
    $sheetData = $objTpl->getActiveSheet();

    $highestRow = $sheetData->getHighestRow(); 
    $highestColumn = $sheetData->getHighestColumn(); 
    $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn); 

    // https://stackoverflow.com/questions/25056030/phpexcel-how-can-i-read-the-excel-sheet-row-by-row
    // FIRST ROW: HEADER

    $col_pos = $sheetData->getCellByColumnAndRow(0, 1)->getValue();
    $col_name = $sheetData->getCellByColumnAndRow(1, 1)->getValue();
    $col_total = $sheetData->getCellByColumnAndRow(2, 1)->getValue();

    if( $col_pos != 'POSITION' || $col_name != 'NAME' || $col_total != 'TOTAL' ){
        echo '<h2 style="color:red;">Error: El archivo con las clasificaciones no tiene la estructura determinada para la importación.</h2>';
        delete_option('tournament_list_import');
        delete_option('clasification_file');
        delete_option('clasification_type_category');
        return true;
    }

    // INFO CLAS
    //echo '<p>F: '.$highestRow.'</p>';
    //echo '<p>C: '.$highestColumnIndex.'</p>';
    //echo '<table>';
    $arrayPremio = [];
    //$highestRow = 4;
    for ($row = 2; $row <= $highestRow; ++$row) {
        //echo '<tr>';

        $arrayAux = [];
        for ($col = 0; $col <= 2; ++$col) {
            switch($col){
                case 0: $key = 'posicion_jugador';break;
                case 1: $key = 'nombre_jugador';break;
                case 2: $key = 'puntos_jugador';break;
            }
            $value = ( $sheetData->getCellByColumnAndRow($col, $row)->getValue() != '' ) ? $sheetData->getCellByColumnAndRow($col, $row)->getValue() : '--';
            $arrayAux[$key] = $value;
            //echo '<td>' . $sheetData->getCellByColumnAndRow($col, $row)->getValue() . '</td>';
        }
        $arrayAux['acf_fc_layout'] = 'jugador';
        $arrayPremio[] = $arrayAux;

        //echo '</tr>';
    }

    
    //echo '</table>';
    //echo '<pre>'.print_r($arrayPremio,true).'</pre>';

    $participantes_primera_cat_masc = 'field_59523f08c8082';
    $participantes_seg_cat_masc = 'field_5952405a8f7e2';
    $participantes_cat_femenina = 'field_5952413b77cde';
    $participantes_cat_inv_mahou = 'field_595241d99c4de';

    switch($categoria){
        case 'primera_cat_masc': $idFieldACF = 'field_59523f08c8082';break;
        case 'seg_cat_masc': $idFieldACF = 'field_5952405a8f7e2';break;
        case 'cat_femenina': $idFieldACF = 'field_5952413b77cde';break;
        case 'cat_inv_mahou': $idFieldACF = 'field_595241d99c4de';break;
    }

    //echo '<p>ACF: '.$idFieldACF.'</p>';

    update_field($idFieldACF, $arrayPremio, $idTorneo);

    delete_option('tournament_list_import');
    delete_option('clasification_file');
    delete_option('clasification_type_category');

    /*$new_meta = get_field($categoria, $idTorneo);
    if(!$new_meta){
     $new_meta = array();
    }
    $update = array(
        'posicion_jugador' => '1',
        'nombre_jugador' => 'test',
        "acf_fc_layout" => 'jugador'
    );
    array_push($new_meta, $update);
    update_field( $idFieldACF, $new_meta, $idTorneo );*/

    echo '<h2 style="color:green;">Importación realizada con éxito.</h2>';
    return true;
    
}
