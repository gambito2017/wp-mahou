<?php

/**

 * The template for displaying 404 pages (not found)

 *

 * @package WordPress

 * @subpackage Twenty_Sixteen

 * @since Twenty Sixteen 1.0

 */



get_header();

?>



         <div class="content-wrap">

              <div class="container topmargin-sm">

                  <h1 class="page-title">Página no encontrada</h1>

                  <h3>Lo sentimos. La página que buscabas no existe.</h3>

                  <img src="<?php echo get_template_directory_uri(); ?>/assets/images/error404.png" class="aligncenter topmargin-sm bottommargin-sm" />

                   <div class="clear"></div>

              </div>

         </div>



<?php get_footer(); ?>