<?php
/**
 * The template part for displaying content
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap">

				<div class="container clearfix">
					<ol class="breadcrumb">
						<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>">Home</a></li>
						<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>noticias/">Noticias</a></li>
						<li class="active"><?php echo get_the_title(); ?></li>
					</ol>

					<div class="single-post nobottommargin">

						<!-- Single Post
						============================================= -->
						<div class="entry clearfix">

							<!-- Entry Title
							============================================= -->
							<div class="entry-title">
								<h2 class="news"><?php echo get_the_title(); ?></h2>
							</div><!-- .entry-title end -->

							<!-- Entry Meta
							============================================= -->
							<ul class="entry-meta clearfix">
								<li><i class="icon-calendar3"></i> <?php echo get_the_date( 'd F \d\e Y', get_the_ID()); ?></li>
							</ul><!-- .entry-meta end -->

							<!-- Post Single - Share
							============================================= -->
							<div class="si-share noborder clearfix">
								<?php $image = get_field('cartel_torneo', get_the_ID()); ?>
								<div>
									<a target="_blank" href="https://www.facebook.com/share.php?u=<?php echo get_the_permalink(); ?>&title=<?php echo urlencode(get_the_title()); ?>" class="social-icon si-borderless si-facebook">
									<i class="icon-facebook"></i>
									<i class="icon-facebook"></i>
									</a>
									<a target="_blank" href="https://twitter.com/intent/tweet?url=<?php echo get_the_permalink(); ?>&status=<?php echo urlencode(get_the_title()); ?>+<?php echo get_the_permalink(); ?>" class="social-icon si-borderless si-twitter">
									<i class="icon-twitter"></i>
									<i class="icon-twitter"></i>
									</a>
									<a data-pin-do="buttonPin" href="https://www.pinterest.com/pin/create/button/?url=<?php echo get_the_permalink(); ?>&media=<?php echo $image['sizes']['thumb_cartel']; ?>&description=<?php echo urlencode(get_the_title()); ?>" data-pin-custom="true" class="social-icon si-borderless si-pinterest">
									<i class="icon-pinterest"></i>
									<i class="icon-pinterest"></i>
									</a>
									<a href="https://plus.google.com/share?url=<?php echo get_the_permalink(); ?>" class="social-icon si-borderless si-gplus">
									<i class="icon-gplus"></i>
									<i class="icon-gplus"></i>
									</a>
									<!--<a href="#" class="social-icon si-borderless si-rss">
									<i class="icon-rss"></i>
									<i class="icon-rss"></i>
									</a>
									<a href="#" class="social-icon si-borderless si-email3">
									<i class="icon-email3"></i>
									<i class="icon-email3"></i>
									</a>-->
								</div>
							</div><!-- Product Single - Share End -->

							<div class="clear"></div>

							<!-- Entry Image
							============================================= -->
							<div class="entry-image bottommargin">
								<?php $image = get_field('main_image', get_the_ID()); ?>
								<img src="<?php echo $image['sizes']['news_mainpage']; ?>" alt="<?php echo get_the_title(); ?>">
							</div><!-- .entry-image end -->

							<!-- Entry Content
							============================================= -->
							<div class="entry-content notopmargin">
								

								<?php the_content(); ?>
								<!-- Post Single - Content End -->

								<!-- Tag Cloud
								============================================= -->
								<div class="tagcloud clearfix bottommargin">
									<?php
									$posttags = get_the_tags();
									if ($posttags) {
									  foreach($posttags as $tag) {
									    //echo $tag->name . ' '; 
									    $term_link = get_term_link( $tag );
									    echo '<a href="'.esc_url($term_link).'">'.$tag->name.'</a>';
									  }
									}
									?>
								</div>

								<div class="clear"></div>

								<!-- Post Single - Share
							============================================= -->
							<div class="si-share noborder clearfix">
								<span>Comparte esta noticia:</span>
								<?php $image = get_field('cartel_torneo', get_the_ID()); ?>
								<div>
									<a target="_blank" href="https://www.facebook.com/share.php?u=<?php echo get_the_permalink(); ?>&title=<?php echo urlencode(get_the_title()); ?>" class="social-icon si-borderless si-facebook">
									<i class="icon-facebook"></i>
									<i class="icon-facebook"></i>
									</a>
									<a target="_blank" href="https://twitter.com/intent/tweet?url=<?php echo get_the_permalink(); ?>&status=<?php echo urlencode(get_the_title()); ?>+<?php echo get_the_permalink(); ?>" class="social-icon si-borderless si-twitter">
									<i class="icon-twitter"></i>
									<i class="icon-twitter"></i>
									</a>
									<a data-pin-do="buttonPin" href="https://www.pinterest.com/pin/create/button/?url=<?php echo get_the_permalink(); ?>&media=<?php echo $image['sizes']['thumb_cartel']; ?>&description=<?php echo urlencode(get_the_title()); ?>" data-pin-custom="true" class="social-icon si-borderless si-pinterest">
									<i class="icon-pinterest"></i>
									<i class="icon-pinterest"></i>
									</a>
									<a href="https://plus.google.com/share?url=<?php echo get_the_permalink(); ?>" class="social-icon si-borderless si-gplus">
									<i class="icon-gplus"></i>
									<i class="icon-gplus"></i>
									</a>
									<!--<a href="#" class="social-icon si-borderless si-rss">
									<i class="icon-rss"></i>
									<i class="icon-rss"></i>
									</a>
									<a href="#" class="social-icon si-borderless si-email3">
									<i class="icon-email3"></i>
									<i class="icon-email3"></i>
									</a>-->
								</div>
							</div><!-- Product Single - Share End -->
								

							</div>
						</div><!-- .entry end -->
			
						<?php get_template_part( 'template-parts/blocks/block', 'random-news' ); ?>
						
					</div>

				</div>

			</div>

		</section><!-- #content end -->
</article><!-- #post-## -->
