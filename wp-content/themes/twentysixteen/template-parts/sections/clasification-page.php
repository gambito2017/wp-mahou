<div class="clear"></div>

<?php
$MetaQueryType = array(
     'relation' => 'OR', // Optional, defaults to "AND"
     array(
          'key' => 'primera_cat_masc',
          'value' => '',
          'compare' => '!='
     ),
     array(
          'key' => 'imagen_categoria_primera_masculina',
          'value' => '',
          'compare' => '!='
     ),
);
$args = array( 
     'post_type' => 'torneo', 
     'post_status' => 'publish',
     'meta_query' => $MetaQueryType,
     'meta_key' => 'fecha_inicio',
     'orderby' => 'meta_value',
     'order'   => 'DESC'
);
$args['nopaging'] = true;
$loopTorneos = new WP_Query( $args );
$arrayTorneos = array();
switch($loopTorneos->post_count){
     case 1: $classPr = 'portfolio-3';$imgSize='news_thumbnail';break;
     case 2: $classPr = 'portfolio-2';$imgSize='news_thumbnail';break;
     case 3: $classPr = 'portfolio-3';$imgSize='news_thumbnail';break;
     case 4: $classPr = 'portfolio-4';$imgSize='news_thumbnail';break;
     default: $classPr = 'portfolio-5';$imgSize='news_thumbnail';
}

?>
<!-- Portfolio Items
============================================= -->
<div id="portfolio" class="portfolio grid-container <?php echo $classPr; ?> clearfix">
<?php
while ( $loopTorneos->have_posts() ) : $loopTorneos->the_post();
     $localizacion = get_field('localizacion', $post->ID);
     $fecha_torneo = get_field('fecha_torneo', $post->ID);
     $primera_cat_masc = get_field('primera_cat_masc', $post->ID);
     $imagen_categoria_primera_masculina = get_field('imagen_categoria_primera_masculina', $post->ID);
     $imagen_torneo_clasificaciones = get_field('imagen_torneo_clasificaciones', $post->ID);
     $background_image = get_field('imagen_fondo', $post->ID);
     if( $primera_cat_masc || $imagen_categoria_primera_masculina ):
?>
          <article class="portfolio-item pf-graphics pf-uielements">
               <div class="portfolio-image">
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>clasificacion/<?php echo $post->post_name; ?>/">
                         <?php if( $imagen_torneo_clasificaciones ): ?>
                              <img src="<?php echo $imagen_torneo_clasificaciones['sizes'][$imgSize]; ?>" alt="<?php echo $post->post_title; ?>">
                         <?php else: ?>
                              <img src="<?php echo $background_image['sizes'][$imgSize]; ?>" alt="<?php echo $post->post_title; ?>">
                         <?php endif; ?>
                    </a>                                    
               </div>
               <div class="portfolio-desc" style="background:#fff;">
                    <h3><a href="<?php echo esc_url( home_url( '/' ) ); ?>clasificacion/<?php echo $post->post_name; ?>/"><?php echo $post->post_title; ?></a></h3>
                    <span><?php echo $localizacion; ?></span>
                    <span><?php echo $fecha_torneo; ?></span>
               </div>
          </article>
<?php
     endif;
endwhile;
wp_reset_query();
?>
</div><!-- #portfolio end -->
<div class="clear topmargin-sm"></div>