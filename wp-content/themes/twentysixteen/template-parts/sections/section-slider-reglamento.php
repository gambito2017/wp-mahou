<section id="slider" class="slider-parallax swiper_wrapper full-screen clearfix">
	<div class="slider-parallax-inner">

		<div class="swiper-container swiper-parent">
			<div class="swiper-wrapper">
				<div class="swiper-slide dark" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets_temp/images/reglamento.jpg');">
					<div class="container clearfix">
						<div class="slider-caption slider-caption-right white custom" style="top:20%;">
							<h2 data-caption-animate="fadeInUp" style="background-color: rgba(0,0,0,0.7); width: -moz-fit-content;">Reglamento</h2>
						</div>
					</div>
				</div>
			</div>
			<!--<div id="slider-arrow-left"><i class="icon-angle-left"></i></div>
			<div id="slider-arrow-right"><i class="icon-angle-right"></i></div>-->
		</div>

		<a href="#" data-scrollto="#content" data-offset="100" class="dark one-page-arrow"><i class="icon-angle-down infinite animated fadeInDown"></i></a>

	</div>
</section>