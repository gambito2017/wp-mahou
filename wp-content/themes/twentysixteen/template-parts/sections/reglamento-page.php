<div class="container clearfix info">
     <div class="row clearfix">

          <div class="col-lg-6">
               <div class="heading-block topmargin">
                    <h1>Formato</h1>
               </div>
               <p class="lead">18 hoyos Stableford individual.</p>
          </div>

          <div class="col-lg-6">

               <div class="heading-block topmargin">
                    <h1>Participantes</h1>
               </div>
               <p class="lead">Pueden acceder al torneo jugadores mayores de edad que dispongan de licencia de la Federación Española de golf.</p>
          </div>

     </div>
     <div class="divider"></div>
</div>
<div class="event entry-image parallax nobottommargin" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets_temp/images/parallax.jpg'); height:200px" data-stellar-background-ratio="0.3">
</div>

<div class="container clear-bottommargin clearfix info">

     <div class="row topmargin-sm clearfix">
          <div class="heading-block topmargin">
               <h1>Categorías:</h1>

               
          </div>

          <div class="col-md-4 bottommargin">
               
               <div class="heading-block nobottomborder" style="margin-bottom: 15px;">
                    <h4>Primera</h4>
               </div>
               <p>Primera Categoría Masculina: <b style="color:#4f5d2a;">de 0 a 16,4</b></p>
          </div>

          <div class="col-md-4 bottommargin">
               
               <div class="heading-block nobottomborder" style="margin-bottom: 15px;">
                    <h4>Segunda</h4>
               </div>
               <p>Segunda Categoría Masculina: a partir de <b style="color:#4f5d2a;">16,5</b></p>
          </div>

          <div class="col-md-4 bottommargin">
               
               <div class="heading-block nobottomborder" style="margin-bottom: 15px;">
                    <h4>Femenina</h4>
               </div>
               <p>Categoría Femenina: <b style="color:#4f5d2a;">de 0 a limitación de hándicap</b> según Federación Española de Golf</p>
          </div>
          
          
          

     </div>

</div>


<div class="event entry-image parallax nobottommargin" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets_temp/images/parallax.jpg'); height:200px" data-stellar-background-ratio="0.3">
</div>


<div class="container clear-bottommargin clearfix info">

     <div class="row topmargin-sm clearfix">
          <div class="heading-block topmargin">
               <h1>Premios</h1>

               
          </div>

          <div class="col-md-4 bottommargin">
               
               <div class="heading-block nobottomborder" style="margin-bottom: 15px;">
                    <h4>Por categoría</h4>
               </div>
               <ul class="iconlist iconlist-color nobottommargin">
                    <li><i class="icon-ok"></i>Primer clasificado</li>
                    <li><i class="icon-ok"></i>Segundo clasificado</li>
                    
               </ul>
          </div>

          <div class="col-md-4 bottommargin">
               
               <div class="heading-block nobottomborder" style="margin-bottom: 15px;">
                    
                    <h4>Premios Especiales:</h4>
               </div>
               <ul class="iconlist iconlist-color nobottommargin">
                    <li><i class="icon-ok"></i>4 bolas más cercanas indistintas.</li>
                    <li><i class="icon-ok"></i>Drive más largo Masculino.</li>
                    <li><i class="icon-ok"></i>Drive más largo Femenino.</li>
                    
               </ul>
          </div>

          <div class="col-md-4 bottommargin">
               
               <div class="heading-block nobottomborder" style="margin-bottom: 15px;">
                    
                    <h4>Sorteo de regalos:</h4>
               </div>
               <ul class="iconlist iconlist-color nobottommargin">
                    <li><i class="icon-ok"></i>1  Driver </li>
                    <li><i class="icon-ok"></i>1  Hibrido  </li>
                    <li><i class="icon-ok"></i>2  Putts  </li>
                    <li><i class="icon-ok"></i>2  Wedges TaylorMade </li>
                    <li><i class="icon-ok"></i>4  Bolsas de palos con logo</li>
                    <li><i class="icon-ok"></i>4  Bolsas de deporte con logo</li>
                    <li><i class="icon-ok"></i>4  Mochilas con logo</li>
                    <li><i class="icon-ok"></i>4  Zapateros con logo</li>
                    <li><i class="icon-ok"></i>4  Players Rolling Carry on con logo</li>
                    
               </ul>
          </div>
          
          
          

     </div>

     <div class="row clearfix">

          <div class="col-lg-12">
               <p class="lead">Los primeros clasificados de las categorías primera damas y caballeros y segunda categoría caballeros, reciben una invitación para participar del PROAM de Gambito Golf Tour en Retamares Golf en Madrid.</p>
               <p class="lead">Y los segundos clasificados, reciben una invitación a la prueba del Circuito Premium en Sherry Golf, Cádiz.</p>
          </div>

     </div>

</div>