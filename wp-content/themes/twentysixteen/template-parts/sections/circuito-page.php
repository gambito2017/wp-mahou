<div class="container clearfix info">
     <div class="row clearfix">

          <div class="col-lg-5">
               <div class="heading-block topmargin">
                    <h1>El circuito</h1>
               </div>
               <p class="lead">El Circuito Mahou San Miguel Golf Club está compuesto por 25 pruebas de hasta 200 jugadores cada uno. Se disputa en 3 categorías: 1  femenina y 2 masculinas (primera y segunda) en formato a 18 hoyos Stableford individual.</p>
               <p class="lead">Se recorre toda la geografía española, en los mejores campos de golf con entornos naturales espectaculares.</p>
          </div>

          <div class="col-lg-7">

               <div style="position: relative; margin-bottom: -60px;" class="ohidden" data-height-lg="426" data-height-md="567" data-height-sm="470" data-height-xs="287" data-height-xxs="183">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets_temp/images/circuito-1.jpg" style="position: absolute; top: 0; left: 0;" data-animate="fadeInUp" data-delay="100" alt="Chrome">
               </div>

          </div>

     </div>
     <div class="divider"></div>
</div>
<div class="clear bottommargin-sm"></div>


<div class="event entry-image parallax nobottommargin" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets_temp/images/mahou.jpg'); height:200px" data-stellar-background-ratio="0.3">
</div>


<div class="container clear-bottommargin clearfix info">

     <div class="row topmargin-sm clearfix">
          <div class="heading-block topmargin">
               <h1>Welcome pack</h1>
               <span>Cada jugador recibe antes de comenzar el torneo un welcome pack:</span>
          </div>

          <div class="col-md-4 bottommargin">
               
               <div class="heading-block nobottomborder" style="margin-bottom: 15px;">
                    <h4>Polo</h4>
               </div>
               <p>Polo de algodón con el Logo de Mahou San Miguel Golf Club.</p>
          </div>

          <div class="col-md-4 bottommargin">
               
               <div class="heading-block nobottomborder" style="margin-bottom: 15px;">
                    
                    <h4>Bolas</h4>
               </div>
               <p>3 bolas Taylor Made con Logo</p>
          </div>

          <div class="col-md-4 bottommargin">
               
               <div class="heading-block nobottomborder" style="margin-bottom: 15px;">
                    
                    <h4>Tees</h4>
               </div>
               <p>Tees de madera con el Logo</p>
          </div>
          
     </div>

</div>


<div class="event entry-image parallax nobottommargin" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets_temp/images/mahou.jpg'); height:200px" data-stellar-background-ratio="0.3">
</div>

<div class="container clearfix info">
     <div class="row clearfix">
          <div class="divider"></div>
          <div class="col-lg-5">
               <div class="heading-block topmargin">
                    <h1>El catering hoyo 9</h1>
               </div>
                    <p class="lead">A mitad de recorrido, los jugadores se encuentran con del magnífico catering en el hoyo 9, donde pueden degustar bebidas del Grupo Mahou San Miguel, perfecto para coger fuerzas para la segunda parte del juego.</p>
          </div>

          <div class="col-lg-7">

          <div style="position: relative; margin-bottom: -60px;" class="ohidden" data-height-lg="426" data-height-md="567" data-height-sm="470" data-height-xs="287" data-height-xxs="183">
               <img src="<?php echo get_template_directory_uri(); ?>/assets_temp/images/circuito-hoyo9.jpg" style="position: absolute; top: 0; left: 0;" data-animate="fadeInUp" data-delay="100" alt="Chill out">
          
          </div>

          </div>
     </div>
</div>
<div class="clear bottommargin-lg"></div>



<div class="event entry-image parallax nobottommargin" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets_temp/images/mahou.jpg'); height:200px" data-stellar-background-ratio="0.3">
</div>

<div class="container clearfix info">
     <div class="row clearfix">
          <div class="divider"></div>
          <div class="col-lg-5">
               <div class="heading-block topmargin">
                    <h1>Chill out hoyo 18</h1>
               </div>
                    <p class="lead">Al finalizar, se accede a la zona chill out en unos entornos naturales espectaculares. </p>
          </div>

          <div class="col-lg-7">

          <div style="position: relative; margin-bottom: -60px;" class="ohidden" data-height-lg="426" data-height-md="567" data-height-sm="470" data-height-xs="287" data-height-xxs="183">
               <img src="<?php echo get_template_directory_uri(); ?>/assets_temp/images/circuito-hoyo18.jpg" style="position: absolute; top: 0; left: 0;" data-animate="fadeInUp" data-delay="100" alt="Chill out">
          </div>

          </div>
     </div>
</div>
<div class="clear bottommargin-lg"></div>



<div class="event entry-image parallax nobottommargin" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets_temp/images/mahou.jpg'); height:200px" data-stellar-background-ratio="0.3">
</div>

<div class="container clearfix info">
     <div class="row clearfix">
          <div class="divider"></div>
          <div class="col-lg-5">
               <div class="heading-block topmargin">
                    <h1>Los premios</h1>
               </div>
                    <p class="lead">La experiencia Mahou San Miguel Golf Club culmina con la ceremonia de entrega de premios y sorteo de regalos espectaculares. Los primeros clasificados de las categorías primera damas y caballeros y segunda categoría caballeros, reciben una invitación para participar del PROAM de Gambito Golf Tour en Retamares Golf en Madrid. Y los segundos clasificados, reciben una invitación a la prueba del Circuito Premium en Sherry Golf, Cádiz.</p>
          </div>

          <div class="col-lg-7">

          <div style="position: relative; margin-bottom: -60px;" class="ohidden" data-height-lg="426" data-height-md="567" data-height-sm="470" data-height-xs="287" data-height-xxs="183">
               <img src="<?php echo get_template_directory_uri(); ?>/assets_temp/images/circuito-premios.jpg" style="position: absolute; top: 0; left: 0;" data-animate="fadeInUp" data-delay="100" alt="Chill out">
          </div>

          </div>
     </div>
</div>
<div class="clear bottommargin-lg"></div>



<div class="event entry-image parallax nobottommargin" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets_temp/images/mahou.jpg'); height:200px" data-stellar-background-ratio="0.3">
</div>


<div class="container clearfix info">
     <div class="row clearfix">
          <div class="divider"></div>
          <div class="col-lg-5">
               <div class="heading-block topmargin">
                    <h1>Golpe Solidario</h1>
               </div>
                    <p class="lead">En un par 3 a decidir, Solán de Cabras invita a dar el <b style="color:#4f5d2a;">“Golpe más solidario del recorrido”</b>.</br>
                    Por 5€, podrás participar en dejar el golpe más cerca de la bandera. El ganador se llevará una caja de botellas Limited Edition y 1 docena de bolas Limited Edition.</br>
                    Todo lo recaudado se destina a la Asociación Española Contra el Cáncer como parte de la campaña <b style="color:#4f5d2a;">#gotasdesolidaridad</b>.</p>
          </div>

          <div class="col-lg-7">

          <div style="position: relative; margin-bottom: -60px;" class="ohidden" data-height-lg="426" data-height-md="567" data-height-sm="470" data-height-xs="287" data-height-xxs="183">
               <img src="<?php echo get_template_directory_uri(); ?>/assets_temp/images/banner-home.png" style="position: absolute; top: 0; left: 0;" data-animate="fadeInUp" data-delay="100" alt="Chill out">
          </div>

          </div>
     </div>
</div>
<div class="clear bottommargin-sm"></div>