<div class="clear"></div>

<?php
$type = get_query_var('type');
$tournament = get_page_by_path( $type, OBJECT, 'torneo' );

$args = array( 
	'p' => $tournament->ID, 
	'post_type' => 'torneo', 
	'post_status' => 'publish',
	'orderby' => 'meta_value',
	'order'	=> 'ASC'
);
$args['nopaging'] = true;
$loopTorneos = new WP_Query( $args );
$arrayTorneos = array();
$isMultimedia = false;
while ( $loopTorneos->have_posts() ) : $loopTorneos->the_post();
	$title_torneo = $post->post_title;
	$premio_bola_hoyo_dos = get_field('premio_bola_hoyo_dos', $post->ID);
	$premio_bola_hoyo_solidario = get_field('premio_bola_hoyo_solidario', $post->ID);
	$premio_bola_hoyo_trece = get_field('premio_bola_hoyo_trece', $post->ID);
	$premio_bola_hoyo_dieciseis = get_field('premio_bola_hoyo_dieciseis', $post->ID);
	$premio_drive_femenino = get_field('premio_drive_femenino', $post->ID);
	$premio_drive_masculino = get_field('premio_drive_masculino', $post->ID);

	$primera_cat_masc = get_field('primera_cat_masc', $post->ID);
	$seg_cat_masc = get_field('seg_cat_masc', $post->ID);
	$cat_femenina = get_field('cat_femenina', $post->ID);
	$cat_inv_mahou = get_field('cat_inv_mahou', $post->ID);

	$imagen_categoria_primera_masculina = get_field('imagen_categoria_primera_masculina', $post->ID);
	$imagen_categoria_segunda_masculina = get_field('imagen_categoria_segunda_masculina', $post->ID);
	$imagen_categoria_femenina = get_field('imagen_categoria_femenina', $post->ID);
	$imagen_categoria_invitados_mahou = get_field('imagen_categoria_invitados_mahou', $post->ID);

	$idTorneo = $post->ID;
endwhile;
wp_reset_query();
?>

<div class="si-share noborder clearfix">
    <?php $image = get_field('cartel_torneo', $idTorneo); ?>
    <div>
         <a target="_blank" href="https://www.facebook.com/share.php?u=<?php echo get_the_permalink($idTorneo); ?>&title=<?php echo urlencode(get_the_title($idTorneo)); ?>" class="social-icon si-borderless si-facebook">
         <i class="icon-facebook"></i>
         <i class="icon-facebook"></i>
         </a>
         <a target="_blank" href="https://twitter.com/intent/tweet?url=<?php echo get_the_permalink($idTorneo); ?>&status=<?php echo urlencode(get_the_title($idTorneo)); ?>+<?php echo get_the_permalink($idTorneo); ?>" class="social-icon si-borderless si-twitter">
         <i class="icon-twitter"></i>
         <i class="icon-twitter"></i>
         </a>
         <a target="_blank" data-pin-do="buttonPin" href="https://www.pinterest.com/pin/create/button/?url=<?php echo get_the_permalink($idTorneo); ?>&media=<?php echo $image['sizes']['thumb_cartel']; ?>&description=<?php echo urlencode(get_the_title($idTorneo)); ?>" data-pin-custom="true" class="social-icon si-borderless si-pinterest">
         <i class="icon-pinterest"></i>
         <i class="icon-pinterest"></i>
         </a>
         <a href="https://plus.google.com/share?url=<?php echo get_the_permalink($idTorneo); ?>" class="social-icon si-borderless si-gplus">
         <i class="icon-gplus"></i>
         <i class="icon-gplus"></i>
         </a>
         <!--<a href="#" class="social-icon si-borderless si-rss">
         <i class="icon-rss"></i>
         <i class="icon-rss"></i>
         </a>
         <a href="#" class="social-icon si-borderless si-email3">
         <i class="icon-email3"></i>
         <i class="icon-email3"></i>
         </a>-->
    </div>
</div><!-- Product Single - Share End -->
<div class="clear topmargin-sm"></div>

<!-- Portfolio Items
============================================= -->
<div id="portfolio" class="portfolio grid-container portfolio-5 clearfix">
	<?php if( $primera_cat_masc || $imagen_categoria_primera_masculina ): ?>

		<article class="portfolio-item pf-media pf-icons show-cat" data-cat="primera_cat_masc">
			<div class="portfolio-image">
				<img src="<?php echo get_template_directory_uri(); ?>/assets_temp/images/mahou.png" alt="<?php echo $title_torneo; ?> - 1ª Categoría Masculina">
				<div class="portfolio-overlay">
					<a class="center-icon"><i class="icon-line-ellipsis"></i></a>
				</div>
			</div>
			<div class="portfolio-desc" style="background:#fff;">
				<h3>1ª Categoría Masculina</h3>
			</div>
		</article>
	<?php endif; ?>



	<?php if( $seg_cat_masc || $imagen_categoria_segunda_masculina ): ?>

		<article class="portfolio-item pf-media pf-icons show-cat" data-cat="seg_cat_masc">
			<div class="portfolio-image">
				<img src="<?php echo get_template_directory_uri(); ?>/assets_temp/images/san-miguel.png" alt="<?php echo $title_torneo; ?> - 2ª Categoría Masculina">
				<div class="portfolio-overlay">
					<a class="center-icon"><i class="icon-line-ellipsis"></i></a>
				</div>
			</div>
			<div class="portfolio-desc" style="background:#fff;">
				<h3>2ª Categoría Masculina</h3>
			</div>
		</article>
	<?php endif; ?>


	<?php if( $cat_femenina || $imagen_categoria_femenina ): ?>

		<article class="portfolio-item pf-media pf-icons show-cat" data-cat="cat_femenina">
			<div class="portfolio-image">
				<img src="<?php echo get_template_directory_uri(); ?>/assets_temp/images/alhambra.png" alt="<?php echo $title_torneo; ?> - Categoría Femenina">
				<div class="portfolio-overlay">
					<a class="center-icon"><i class="icon-line-ellipsis"></i></a>
				</div>
			</div>
			<div class="portfolio-desc" style="background:#fff;">
				<h3>Categoría Femenina</h3>
			</div>
		</article>

	<?php endif; ?>


	<?php if( $cat_inv_mahou || $imagen_categoria_invitados_mahou ): ?>
		<article class="portfolio-item pf-media pf-icons show-cat" data-cat="cat_inv_mahou">
			<div class="portfolio-image">
				<img src="<?php echo get_template_directory_uri(); ?>/assets_temp/images/founders.png" alt="<?php echo $title_torneo; ?> - Categoría Invitados Mahou">
				<div class="portfolio-overlay">
					<a class="center-icon"><i class="icon-line-ellipsis"></i></a>
				</div>
			</div>
			<div class="portfolio-desc" style="background:#fff;">
				<h3>Categoría Invitados Mahou</h3>
			</div>
		</article>
		
	<?php endif; ?>

	<?php if( $premio_bola_hoyo_dos ): ?>
		<article class="portfolio-item pf-media pf-icons show-cat" data-cat="special_awards">
			<div class="portfolio-image">
				<img src="<?php echo get_template_directory_uri(); ?>/assets_temp/images/solan.png" alt="<?php echo $title_torneo; ?> - Premios Especiales">
				<div class="portfolio-overlay">
					<a class="center-icon"><i class="icon-line-ellipsis"></i></a>
				</div>
			</div>
			<div class="portfolio-desc" style="background:#fff;">
				<h3>Premios Especiales</h3>
			</div>
		</article>
		
	<?php endif; ?>


</div>
<div class="clear"></div>







<?php if( $primera_cat_masc || $imagen_categoria_primera_masculina ): ?>
		<div class="content-wrap cat-clasification" style="display:none;" data-cat="primera_cat_masc">
			<div class="clearfix">
				<div class="heading-block">
					<h2>1ª Categoría Masculina</h2>
				</div>
				<div class="table-responsive">
					<?php if( $imagen_categoria_primera_masculina ): ?>
						<?php foreach( $imagen_categoria_primera_masculina as $imgOrden ): ?>
							<a href="<?php echo $imgOrden['url']; ?>" data-lightbox="image">
	                          <img class="aligncenter bottommargin-sm" src="<?php echo $imgOrden['url']; ?>" >
	                        </a>
						<?php endforeach; ?>
					<?php endif; ?>
					<?php if( $primera_cat_masc ): ?>
						<table class="table nobottommargin">
							<thead>
								<td class="table-posicion" style="width:10%;">Pos</td>
								<td class="table-posicion" style="width:70%;">Nombre</td>
								<td class="table-posicion" style="width:20%;">Total</td>
							</thead>
							<tbody>
								<?php foreach( $primera_cat_masc as $keyP => $valPart ): ?>
									<tr>
										<td class="table-posicion" style="width:10%;"><?php echo $valPart['posicion_jugador'];?></td>
										<td class="table-nombre" style="width:70%;"><?php echo $valPart['nombre_jugador'];?></td>
										<td class="table-nombre" style="width:20%;"><?php echo $valPart['puntos_jugador'];?></td>
									</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					<?php endif; ?>
				</div>
				
			</div>
		</div>

	<?php endif; ?>


	<?php if( $seg_cat_masc || $imagen_categoria_segunda_masculina ): ?>
		<div class="content-wrap cat-clasification" style="display:none;" data-cat="seg_cat_masc">
			<div class="container clearfix">
				<div class="heading-block">
					<h2>2ª Categoría Masculina</h2>
				</div>
				<div class="table-responsive">
					<?php if( $imagen_categoria_segunda_masculina ): ?>
						<?php foreach( $imagen_categoria_segunda_masculina as $imgOrden ): ?>
							<a href="<?php echo $imgOrden['url']; ?>" data-lightbox="image">
	                          <img class="aligncenter bottommargin-sm" src="<?php echo $imgOrden['url']; ?>" >
	                        </a>
						<?php endforeach; ?>
					<?php endif; ?>
					<?php if( $seg_cat_masc ): ?>
						<table class="table nobottommargin">
							<thead>
								<td class="table-posicion" style="width:10%;">Pos</td>
								<td class="table-posicion" style="width:70%;">Nombre</td>
								<td class="table-posicion" style="width:20%;">Total</td>
							</thead>
							<tbody>
								<?php foreach( $seg_cat_masc as $keyP => $valPart ): ?>
									<tr>
										<td class="table-posicion" style="width:10%;"><?php echo $valPart['posicion_jugador'];?></td>
										<td class="table-nombre" style="width:70%;"><?php echo $valPart['nombre_jugador'];?></td>
										<td class="table-nombre" style="width:20%;"><?php echo $valPart['puntos_jugador'];?></td>
									</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					<?php endif; ?>	
				</div>
				
			</div>
		</div>

	<?php endif; ?>


	<?php if( $cat_femenina || $imagen_categoria_femenina ): ?>
		<div class="content-wrap cat-clasification" style="display:none;" data-cat="cat_femenina">
			<div class="container clearfix">
				<div class="heading-block">
					<h2>Categoría Femenina</h2>
				</div>
				<div class="table-responsive">
					<?php if( $imagen_categoria_femenina ): ?>
						<?php foreach( $imagen_categoria_femenina as $imgOrden ): ?>
							<a href="<?php echo $imgOrden['url']; ?>" data-lightbox="image">
	                          <img class="aligncenter bottommargin-sm" src="<?php echo $imgOrden['url']; ?>" >
	                        </a>
						<?php endforeach; ?>
					<?php endif; ?>
					<?php if( $cat_femenina ): ?>
						<table class="table nobottommargin">
							<thead>
								<td class="table-posicion" style="width:10%;">Pos</td>
								<td class="table-posicion" style="width:70%;">Nombre</td>
								<td class="table-posicion" style="width:20%;">Total</td>
							</thead>
							<tbody>
								<?php foreach( $cat_femenina as $keyP => $valPart ): ?>
									<tr>
										<td class="table-posicion" style="width:10%;"><?php echo $valPart['posicion_jugador'];?></td>
										<td class="table-nombre" style="width:70%;"><?php echo $valPart['nombre_jugador'];?></td>
										<td class="table-nombre" style="width:20%;"><?php echo $valPart['puntos_jugador'];?></td>
									</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					<?php endif; ?>
				</div>
				
			</div>
		</div>

	<?php endif; ?>



	<?php if( $cat_inv_mahou || $imagen_categoria_invitados_mahou ): ?>
		<div class="content-wrap cat-clasification" style="display:none;" data-cat="cat_inv_mahou">
			<div class="container clearfix">
				<div class="heading-block">
					<h2>Categoría Invitados Mahou</h2>
				</div>
				<div class="table-responsive">
					<?php if( $imagen_categoria_invitados_mahou ): ?>
						<?php foreach( $imagen_categoria_invitados_mahou as $imgOrden ): ?>
							<a href="<?php echo $imgOrden['url']; ?>" data-lightbox="image">
	                          <img class="aligncenter bottommargin-sm" src="<?php echo $imgOrden['url']; ?>" >
	                        </a>
						<?php endforeach; ?>
					<?php endif; ?>
					<?php if( $cat_inv_mahou ): ?>
						<table class="table nobottommargin">
							<thead>
								<td class="table-posicion" style="width:10%;">Pos</td>
								<td class="table-posicion" style="width:70%;">Nombre</td>
								<td class="table-posicion" style="width:20%;">Total</td>
							</thead>
							<tbody>
								<?php foreach( $cat_inv_mahou as $keyP => $valPart ): ?>
									<tr>
										<td class="table-posicion" style="width:10%;"><?php echo $valPart['posicion_jugador'];?></td>
										<td class="table-nombre" style="width:70%;"><?php echo $valPart['nombre_jugador'];?></td>
										<td class="table-nombre" style="width:20%;"><?php echo $valPart['puntos_jugador'];?></td>
									</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					<?php endif; ?>
				</div>
				
			</div>
		</div>

	<?php endif; ?>

	<?php if( $premio_bola_hoyo_dos ): ?>
		<div class="content-wrap cat-clasification" style="display:none;" data-cat="special_awards">
			<div class="container clearfix">
				<div class="heading-block">
					<h2>Premios Especiales</h2>
				</div>

				<div class="col-md-4 bottommargin">
					<div class="heading-block nobottomborder" style="margin-bottom: 15px;">
						<h4 style="font-size:16px;">Bola más cercana Hoyo 2</h4>
						<span><?php echo $premio_bola_hoyo_dos; ?></span>
					</div>
				</div>

				<div class="col-md-4 bottommargin">
					<div class="heading-block nobottomborder" style="margin-bottom: 15px;">
						<h4 style="font-size:16px;">Bola más cercana Hoyo Solidario</h4>
						<span><?php echo $premio_bola_hoyo_solidario; ?></span>
					</div>
				</div>

				<div class="col-md-4 bottommargin">
					<div class="heading-block nobottomborder" style="margin-bottom: 15px;">
						<h4 style="font-size:16px;">Bola más cercana Hoyo 13</h4>
						<span><?php echo $premio_bola_hoyo_trece; ?></span>
					</div>
				</div>				

			</div>
			<div class="row topmargin-sm clearfix">
				<div class="col-md-4 bottommargin">
					<div class="heading-block nobottomborder" style="margin-bottom: 15px;">
						<h4 style="font-size:16px;">Bola más cercana Hoyo 16</h4>
						<span><?php echo $premio_bola_hoyo_dieciseis; ?></span>
					</div>
				</div>

				<div class="col-md-4 bottommargin">
					<div class="heading-block nobottomborder" style="margin-bottom: 15px;">
						<h4 style="font-size:16px;">Drive más largo femenino</h4>
						<span><?php echo $premio_drive_femenino; ?></span>
					</div>
				</div>

				<div class="col-md-4 bottommargin">
					<div class="heading-block nobottomborder" style="margin-bottom: 15px;">
						<h4 style="font-size:16px;">Drive más largo masculino</h4>
						<span><?php echo $premio_drive_masculino; ?></span>
					</div>
				</div>				

			</div>
				
			</div>
		</div>

	<?php endif; ?>