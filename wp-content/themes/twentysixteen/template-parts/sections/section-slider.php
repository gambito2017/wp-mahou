<section id="slider" class="slider-parallax revslider-wrap ohidden clearfix">

	<!--
	#################################
		- THEMEPUNCH BANNER -
	#################################
	-->
	<div class="tp-banner-container">
		<div class="tp-banner" >
			<ul>    
				<?php
				$args = array( 
					'post_type' => 'slider', 
					'post_status' => 'publish',
					'orderby' => array( 'menu_order' => 'ASC', 'title' => 'ASC' )
				);

				$loopSliders = new WP_Query( $args );
				while ( $loopSliders->have_posts() ) : $loopSliders->the_post();
					$image = get_field('imagen_slider', $post->id);
					$pretitle = get_field('pretitle', $post->id);
					$subtitle = get_field('subtitle', $post->id);
					$info_slider = get_field('info_slider', $post->id);
				?>
					<!-- SLIDE  -->
					<li data-transition="fade" data-slotamount="1" data-masterspeed="1500" data-delay="10000" data-saveperformance="off" data-title="Latest Collections" style="background-image: url(<?php echo $image['sizes']['slider']; ?>);">
						<!-- LAYERS -->

						<!-- LAYER NR. 2 -->
						<div class="tp-caption customin ltl tp-resizeme revo-slider-caps-text uppercase"
						data-x="100"
						data-y="50"
						data-customin="x:-200;y:0;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
						data-speed="400"
						data-start="1000"
						data-easing="easeOutQuad"
						data-splitin="none"
						data-splitout="none"
						data-elementdelay="0.01"
						data-endelementdelay="0.1"
						data-endspeed="1000"
						data-endeasing="Power4.easeIn" style=""></div>

						<?php if($pretitle): ?>
							<div class="tp-caption customin ltl tp-resizeme revo-slider-caps-text uppercase"
							data-x="570"
							data-y="75"
							data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1.3;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
							data-speed="700"
							data-start="1000"
							data-easing="easeOutQuad"
							data-splitin="none"
							data-splitout="none"
							data-elementdelay="0.01"
							data-endelementdelay="0.1"
							data-endspeed="1000"
							data-endeasing="Power4.easeIn" style=" color: #fff;"><?php echo $pretitle; ?></div>
						<?php endif; ?>

						<div class="tp-caption customin ltl tp-resizeme revo-slider-emphasis-text nopadding noborder"
						data-x="570"
						data-y="155"
						data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1.3;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
						data-speed="700"
						data-start="1200"
						data-easing="easeOutQuad"
						data-splitin="none"
						data-splitout="none"
						data-elementdelay="0.01"
						data-endelementdelay="0.1"
						data-endspeed="1000"
						data-endeasing="Power4.easeIn" style=" color: #fff; max-width: 430px; line-height: 1.15;"><?php echo $post->post_title; ?></div>

						<?php if($subtitle): ?>
							<div class="tp-caption customin ltl tp-resizeme revo-slider-desc-text tleft"
							data-x="570"
							data-y="275"
							data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1.3;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
							data-speed="700"
							data-start="1400"
							data-easing="easeOutQuad"
							data-splitin="none"
							data-splitout="none"
							data-elementdelay="0.01"
							data-endelementdelay="0.1"
							data-endspeed="1000"
							data-endeasing="Power4.easeIn" style=" color: #fff; max-width: 500px;font-size:25px; white-space: normal;"><?php echo $subtitle; ?></div>
						<?php endif; ?>

						<?php if($info_slider): ?>
							<div class="tp-caption customin ltl tp-resizeme revo-slider-emphasis-text nopadding noborder"
							data-x="570"
							data-y="375"
							data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1.3;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
							data-speed="700"
							data-start="1550"
							data-easing="easeOutQuad"
							data-splitin="none"
							data-splitout="none"
							data-elementdelay="0.01"
							data-endelementdelay="0.1"
							data-endspeed="1000"
							data-endeasing="Power4.easeIn" style=" color: #fff; max-width: 430px; line-height: 1.15;"><?php echo $info_slider; ?></div>
						<?php endif; ?>

					</li>
					<!-- SLIDE  -->
				<?php
				endwhile;
				wp_reset_query();
				?>
			</ul>
		</div>
	</div>

	<!-- END REVOLUTION SLIDER -->

</section>
