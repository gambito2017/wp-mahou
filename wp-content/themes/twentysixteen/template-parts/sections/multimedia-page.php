<?php
$type = get_query_var('type');

if( empty($type) ):
	get_template_part( 'template-parts/sections/multimedia', 'list' );
else:
	get_template_part( 'template-parts/sections/multimedia', 'detail' );
endif;
?>	
