<?php
$type = get_query_var('type');
$tournament = get_page_by_path( $type, OBJECT, 'torneo' );

$args = array( 
	'p' => $tournament->ID, 
	'post_type' => 'torneo', 
	'post_status' => 'publish',
	'orderby' => 'meta_value',
	'order'	=> 'ASC'
);
$args['nopaging'] = true;
$loopTorneos = new WP_Query( $args );
$arrayTorneos = array();
$isMultimedia = false;
while ( $loopTorneos->have_posts() ) : $loopTorneos->the_post();
	$arrayMultimedia = array();
	$multimedia_images = get_field('multimedia_images', $post->ID);
	$cartel_torneo = get_field('cartel_torneo', $post->ID);
	$localizacion = get_field('localizacion', $post->ID);
	$fecha_torneo = get_field('fecha_torneo', $post->ID);
	if( $multimedia_images ):
		$isMultimedia = true;
		foreach ( $multimedia_images as $multImg ) :
			$arrayMultimedia[] = array(
				'type' => 'img',
				'thumb' => $multImg['sizes']['news_thumbnail'],
				'src' => $multImg['url'],
			);
		endforeach;
	endif;

	$multimedia_videos = get_field('multimedia_videos', $post->ID);
	if( $multimedia_videos ):
		$isMultimedia = true;
		foreach ( $multimedia_videos as $multVideos ) :
			$arrayMultimedia[] = array(
				'type' => 'video',
				'thumb' => $cartel_torneo['sizes']['news_thumbnail'],
				'src' => $multVideos['url_video'],
			);
		endforeach;
	endif;

	$arrayTorneos[] = array(
		'id' => $post->ID,
		'title' => $post->post_title,
		'date_torneo' => $fecha_torneo,
		'city_torneo' => $localizacion,
		'multimedia' => $arrayMultimedia
	);
endwhile;
wp_reset_query();
?>
<div class="si-share noborder clearfix">
    <?php $image = get_field('cartel_torneo', $tournament->ID); ?>
    <div>
         <a target="_blank" href="https://www.facebook.com/share.php?u=<?php echo get_the_permalink($tournament->ID); ?>&title=<?php echo urlencode(get_the_title($tournament->ID)); ?>" class="social-icon si-borderless si-facebook">
         <i class="icon-facebook"></i>
         <i class="icon-facebook"></i>
         </a>
         <a target="_blank" href="https://twitter.com/intent/tweet?url=<?php echo get_the_permalink($tournament->ID); ?>&status=<?php echo urlencode(get_the_title($tournament->ID)); ?>+<?php echo get_the_permalink($tournament->ID); ?>" class="social-icon si-borderless si-twitter">
         <i class="icon-twitter"></i>
         <i class="icon-twitter"></i>
         </a>
         <a target="_blank" data-pin-do="buttonPin" href="https://www.pinterest.com/pin/create/button/?url=<?php echo get_the_permalink($tournament->ID); ?>&media=<?php echo $image['sizes']['thumb_cartel']; ?>&description=<?php echo urlencode(get_the_title($tournament->ID)); ?>" data-pin-custom="true" class="social-icon si-borderless si-pinterest">
         <i class="icon-pinterest"></i>
         <i class="icon-pinterest"></i>
         </a>
         <a href="https://plus.google.com/share?url=<?php echo get_the_permalink($tournament->ID); ?>" class="social-icon si-borderless si-gplus">
         <i class="icon-gplus"></i>
         <i class="icon-gplus"></i>
         </a>
         <!--<a href="#" class="social-icon si-borderless si-rss">
         <i class="icon-rss"></i>
         <i class="icon-rss"></i>
         </a>
         <a href="#" class="social-icon si-borderless si-email3">
         <i class="icon-email3"></i>
         <i class="icon-email3"></i>
         </a>-->
    </div>
</div><!-- Product Single - Share End -->
<div class="clear topmargin-sm"></div>
<div class="clear"></div>
<div id="portfolio" class="portfolio grid-container portfolio-6 portfolio-nomargin portfolio-notitle clearfix">

	<!-- Portfolio Items
	============================================= -->
	<?php
	if( $isMultimedia ){
		foreach( $arrayTorneos as $torneo ){
			$multimedia = $torneo['multimedia'];
			foreach( $multimedia as $item ){
				if( $item['type'] == 'img'):
				?>
					<article class="portfolio-item torneo1">
						<div class="portfolio-image">
							<a href="<?php echo $item['src']; ?>" class="left-icon" data-lightbox="image" style="cursor:zoom-in;">
								<img src="<?php echo $item['thumb']; ?>" alt="<?php echo $torneo['title']; ?>">
							</a>
						</div>
					</article>

					<!--
					<article class="portfolio-item pf-media pf-icons torneo<?php echo $torneo['id']; ?> image">
						<div class="portfolio-image">
							<a href="<?php echo $item['src']; ?>">
								<img src="<?php echo $item['thumb']; ?>" alt="<?php echo $torneo['title']; ?>">
							</a>
							<div class="portfolio-overlay">
								<a href="<?php echo $item['src']; ?>" class="center-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
							</div>
						</div>
						<div class="portfolio-desc" style="background:#fff;">
							<h3><?php echo $torneo['title']; ?></h3>
							<span><?php echo $torneo['city_torneo']; ?></span>
							<span><?php echo $torneo['date_torneo']; ?></span> 
						</div>
					</article>
					-->
				<?php
				endif;

				if( $item['type'] == 'video'):
				?>
					<article class="portfolio-item pf-media pf-icons torneo<?php echo $torneo['id']; ?> image">
						<div class="portfolio-image">
							<a style="cursor:pointer;">
								<img src="<?php echo $item['thumb']; ?>" alt="<?php echo $torneo['title']; ?>">
							</a>
							<div class="portfolio-overlay">
								<a href="<?php echo $item['src']; ?>" class="left-icon" data-lightbox="iframe"><i class="icon-line-play"></i></a>
							</div>
						</div>
						<div class="portfolio-desc" style="background:#fff;">
							<h3><?php echo $torneo['title']; ?></h3>
							<span>Ciudad</span>
							<span>Fecha</span> 
						</div>
					</article>
				<?php
				endif;
			}
		?>
		
		<?php
		}
	}
	else{
		echo '<h2>Lo sentimos</h2>';
		echo '<p>Aún no hay ningún contenido multimedia para esta sección.</p>';
	}
	?>

</div><!-- #portfolio end -->