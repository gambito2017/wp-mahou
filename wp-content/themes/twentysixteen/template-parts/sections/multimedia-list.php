<div id="posts" class="portfolio grid-container portfolio-4">
	<?php
	$MetaQueryType = array(
		array(
	          'key' => 'multimedia_images',
	          'value' => '',
	          'compare' => '!='
	     ),
	);
	$args = array( 
		'post_type' => 'torneo', 
		'post_status' => 'publish',
		'meta_query' => $MetaQueryType,
		'meta_key' => 'fecha_inicio',
		'orderby' => 'meta_value',
		'order'	=> 'DESC'
	);
	$args['nopaging'] = true;
	$loopTorneos = new WP_Query( $args );
	$arrayTorneos = array();
	$isMultimedia = false;
	while ( $loopTorneos->have_posts() ) : $loopTorneos->the_post();
		$multimedia_images = get_field('multimedia_images', $post->ID);
		$multimedia_videos = get_field('multimedia_videos', $post->ID);
		//if( $multimedia_images || $multimedia_videos ):
	?>
			<?php
			$isMultimedia = true;
			$image = get_field('cartel_torneo', $post->ID);
			$rango_fecha = get_field('fecha_torneo', $post->ID);
			$localizacion = get_field('localizacion', $post->ID);
			?>

			<article class="portfolio-item pf-graphics pf-uielements">
               <div class="portfolio-image">
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>multimedia/<?php echo $post->post_name; ?>/">
                        <img src="<?php echo $image['sizes']['news_thumbnail']; ?>" alt="<?php echo $post->post_title; ?>">
                    </a>                                    
               </div>
               <div class="portfolio-desc" style="background:#fff;">
                    <h3><a href="<?php echo esc_url( home_url( '/' ) ); ?>multimedia/<?php echo $post->post_name; ?>/"><?php echo $post->post_title; ?></a></h3>
                    <span><?php echo $localizacion; ?></span>
                    <span><?php echo $rango_fecha; ?></span>
               </div>
	         </article>
	<?php
		//endif;
	endwhile;
	wp_reset_query();

	?>

	<?php
	if( !$isMultimedia ){
		echo '<h2>Lo sentimos</h2>';
		echo '<p>Aún no hay ningún contenido multimedia para esta sección.</p>';
	}
	?>
</div>