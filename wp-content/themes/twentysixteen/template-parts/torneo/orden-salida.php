<div class="content-wrap">
	<div class="container clearfix">
		<div class="heading-block">
			<h2>Orden de Salida</h2>
			<span>Jornada 1</span>
		</div>
		<div class="table-responsive">
			<table class="table table-bordered nobottommargin">
				<thead>
					<tr>
						<td class="table-posicion-orden">Posición</td>
						<td class="table-posicion-orden">TEE</td>
						<td class="table-posicion-orden">Hora</td>
						<td class="table-porcentaje-premios">Jugadores</td>
						
					</tr>
				</thead>
				
				<tbody>
					<tr>
						<td class="table-posicion-orden">1</td>
						<td class="table-posicion">1</td>
						<td class="table-hora">08:00</td>
						<td class="table-nombre-orden">Pedro Pérez<br>
						Pedro Pérez<br>
						Pedro Pérez</td>
						
						
					</tr>
					<tr>
						<td class="table-posicion-orden">2</td>
						<td class="table-posicion">1</td>
						<td class="table-hora">08:00</td>
						<td class="table-nombre-orden">Pedro Pérez<br>
						Pedro Pérez<br>
						Pedro Pérez</td>
						
					</tr>
					<tr>
						<td class="table-posicion-orden">3</td>
						<td class="table-posicion">1</td>
						<td class="table-hora">08:00</td>
						<td class="table-nombre-orden">Pedro Pérez<br>
						Pedro Pérez<br>
						Pedro Pérez</td>
						
					</tr>
				</tbody>
			</table>
			<table class="table table-bordered nobottommargin">
				<thead>
					<tr>
						<td class="table-posicion-orden">Posición</td>
						<td class="table-posicion-orden">TEE</td>
						<td class="table-posicion-orden">Hora</td>
						<td class="table-porcentaje-premios">Jugadores</td>
						
					</tr>
				</thead>
				
				<tbody>
					<tr>
						<td class="table-posicion-orden">10</td>
						<td class="table-posicion">10</td>
						<td class="table-hora">08:00</td>
						<td class="table-nombre-orden">Pedro Pérez<br>
						Pedro Pérez<br>
						Pedro Pérez</td>
						
						
					</tr>
					<tr>
						<td class="table-posicion-orden">11</td>
						<td class="table-posicion">10</td>
						<td class="table-hora">08:00</td>
						<td class="table-nombre-orden">Pedro Pérez<br>
						Pedro Pérez<br>
						Pedro Pérez</td>
						
					</tr>
					<tr>
						<td class="table-posicion-orden">12</td>
						<td class="table-posicion">10</td>
						<td class="table-hora">08:00</td>
						<td class="table-nombre-orden">Pedro Pérez<br>
						Pedro Pérez<br>
						Pedro Pérez</td>
						
					</tr>
				</tbody>
			</table>
		</div>
		
		<img src="<?php echo get_template_directory_uri(); ?>/assets/banners/ad.jpg" alt="Ad" class="aligncenter topmargin-lg bottommargin-lg">
		
		<div class="heading-block">
			<h3>Jornada 2</h3>
		</div>
		<div class="table-responsive">
			<table class="table table-bordered nobottommargin">
				<thead>
					<tr>
						<td class="table-posicion-orden">Posición</td>
						<td class="table-posicion-orden">TEE</td>
						<td class="table-posicion-orden">Hora</td>
						<td class="table-porcentaje-premios">Jugadores</td>
						
					</tr>
				</thead>
				
				<tbody>
					<tr>
						<td class="table-posicion-orden">1</td>
						<td class="table-posicion">1</td>
						<td class="table-hora">08:00</td>
						<td class="table-nombre-orden">Pedro Pérez<br>
						Pedro Pérez<br>
						Pedro Pérez</td>
						
						
					</tr>
					<tr>
						<td class="table-posicion-orden">2</td>
						<td class="table-posicion">1</td>
						<td class="table-hora">08:00</td>
						<td class="table-nombre-orden">Pedro Pérez<br>
						Pedro Pérez<br>
						Pedro Pérez</td>
						
					</tr>
					<tr>
						<td class="table-posicion-orden">3</td>
						<td class="table-posicion">1</td>
						<td class="table-hora">08:00</td>
						<td class="table-nombre-orden">Pedro Pérez<br>
						Pedro Pérez<br>
						Pedro Pérez</td>
						
					</tr>
				</tbody>
			</table>
			<table class="table table-bordered nobottommargin">
				<thead>
					<tr>
						<td class="table-posicion-orden">Posición</td>
						<td class="table-posicion-orden">TEE</td>
						<td class="table-posicion-orden">Hora</td>
						<td class="table-porcentaje-premios">Jugadores</td>
						
					</tr>
				</thead>
				
				<tbody>
					<tr>
						<td class="table-posicion-orden">1</td>
						<td class="table-posicion">1</td>
						<td class="table-hora">08:00</td>
						<td class="table-nombre-orden">Pedro Pérez<br>
						Pedro Pérez<br>
						Pedro Pérez</td>
						
						
					</tr>
					<tr>
						<td class="table-posicion-orden">2</td>
						<td class="table-posicion">1</td>
						<td class="table-hora">08:00</td>
						<td class="table-nombre-orden">Pedro Pérez<br>
						Pedro Pérez<br>
						Pedro Pérez</td>
						
					</tr>
					<tr>
						<td class="table-posicion-orden">3</td>
						<td class="table-posicion">1</td>
						<td class="table-hora">08:00</td>
						<td class="table-nombre-orden">Pedro Pérez<br>
						Pedro Pérez<br>
						Pedro Pérez</td>
						
					</tr>
				</tbody>
			</table>
		</div>
		<img src="<?php echo get_template_directory_uri(); ?>/assets/banners/ad.jpg" alt="Ad" class="aligncenter topmargin-lg bottommargin-lg">
		
		<?php $patrocinadores = get_field('patrocinadores', get_the_ID()); ?>

		<?php if($patrocinadores): ?>
			<div class="clear"></div>
			<div class="fancy-title title-border title-center topmargin-sm">
				<h4>Patrocinadores</h4>
			</div>
			<ul class="clients-grid grid-6 nobottommargin clearfix">
				<?php foreach ( $patrocinadores as $patr ) : ?>
					<li><img src="<?php echo $patr['sizes']['big_sponsor']; ?>" alt="Clients"></li>
				<?php endforeach; ?>
			</ul>
		<?php endif; ?>
	</div>
</div>