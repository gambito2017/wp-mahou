<div class="content-wrap">
	<div class="container clearfix">
		<div class="heading-block">
			<h2>Listado definitivo de inscritos en <?php echo get_the_title(); ?></h2>
			
		</div>
		<div class="table-responsive">
			<table class="table  nobottommargin">
				<tbody>
					<?php $players = get_field('sign_players', get_the_ID()); ?>
					<?php foreach( $players as $number => $pl ): ?>
						<?php $ranking_actual = get_field('ranking_actual', $pl); ?>
						<tr>
							<td class="table-posicion"><?php echo $number+1; ?></td>
							<td class="table-nombre"><?php echo get_the_title($pl); ?></td>
							<td class="table-puntuacion"><?php echo ($ranking_actual) ? $ranking_actual : '---'; ?></td>
							
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
		<img src="<?php echo get_template_directory_uri(); ?>/assets/banners/ad.jpg" alt="Ad" class="aligncenter topmargin-lg bottommargin-lg">
		
		<?php $patrocinadores = get_field('patrocinadores', get_the_ID()); ?>

		<?php if($patrocinadores): ?>
			<div class="clear"></div>
			<div class="fancy-title title-border title-center topmargin-sm">
				<h4>Patrocinadores</h4>
			</div>
			<ul class="clients-grid grid-6 nobottommargin clearfix">
				<?php foreach ( $patrocinadores as $patr ) : ?>
					<li><img src="<?php echo $patr['sizes']['big_sponsor']; ?>" alt="Clients"></li>
				<?php endforeach; ?>
			</ul>
		<?php endif; ?>
	</div>
</div>