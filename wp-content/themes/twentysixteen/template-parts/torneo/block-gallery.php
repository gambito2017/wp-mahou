
<div class="col_two_fifth nobottommargin col_last">
	<h4>Gallery</h4>
	<div class="masonry-thumbs col-4" data-lightbox="gallery">
		<a href="../assets/images/events/1.jpg" data-lightbox="gallery-item"><img class="image_fade" src="../assets/images/events/thumbs/1.jpg" alt="Gallery Thumb 1"></a>
		<a href="../assets/images/events/2.jpg" data-lightbox="gallery-item"><img class="image_fade" src="../assets/images/events/thumbs/2.jpg" alt="Gallery Thumb 2"></a>
		<a href="../assets/images/events/3.jpg" data-lightbox="gallery-item"><img class="image_fade" src="../assets/images/events/thumbs/3.jpg" alt="Gallery Thumb 3"></a>
		<a href="../assets/images/events/4.jpg" data-lightbox="gallery-item"><img class="image_fade" src="../assets/images/events/thumbs/4.jpg" alt="Gallery Thumb 4"></a>
		<a href="../assets/images/events/5.jpg" data-lightbox="gallery-item"><img class="image_fade" src="../assets/images/events/thumbs/5.jpg" alt="Gallery Thumb 5"></a>
		<a href="../assets/images/events/6.jpg" data-lightbox="gallery-item"><img class="image_fade" src="../assets/images/events/thumbs/6.jpg" alt="Gallery Thumb 6"></a>
		<a href="../assets/images/events/7.jpg" data-lightbox="gallery-item"><img class="image_fade" src="../assets/images/events/thumbs/7.jpg" alt="Gallery Thumb 7"></a>
		<a href="../assets/images/events/8.jpg" data-lightbox="gallery-item"><img class="image_fade" src="../assets/images/events/thumbs/8.jpg" alt="Gallery Thumb 8"></a>
        <a href="../assets/images/events/1.jpg" data-lightbox="gallery-item"><img class="image_fade" src="../assets/images/events/thumbs/1.jpg" alt="Gallery Thumb 1"></a>
		<a href="../assets/images/events/2.jpg" data-lightbox="gallery-item"><img class="image_fade" src="../assets/images/events/thumbs/2.jpg" alt="Gallery Thumb 2"></a>
		<a href="../assets/images/events/3.jpg" data-lightbox="gallery-item"><img class="image_fade" src="../assets/images/events/thumbs/3.jpg" alt="Gallery Thumb 3"></a>
		<a href="../assets/images/events/4.jpg" data-lightbox="gallery-item"><img class="image_fade" src="../assets/images/events/thumbs/4.jpg" alt="Gallery Thumb 4"></a>
		<a href="../assets/images/events/5.jpg" data-lightbox="gallery-item"><img class="image_fade" src="../assets/images/events/thumbs/5.jpg" alt="Gallery Thumb 5"></a>
		<a href="../assets/images/events/6.jpg" data-lightbox="gallery-item"><img class="image_fade" src="../assets/images/events/thumbs/6.jpg" alt="Gallery Thumb 6"></a>
		<a href="../assets/images/events/7.jpg" data-lightbox="gallery-item"><img class="image_fade" src="../assets/images/events/thumbs/7.jpg" alt="Gallery Thumb 7"></a>
		<a href="../assets/images/events/8.jpg" data-lightbox="gallery-item"><img class="image_fade" src="../assets/images/events/thumbs/8.jpg" alt="Gallery Thumb 8"></a>
	</div>
</div>
