<?php $golf_field_title = get_field('golf_field_title', get_the_ID()); ?>
<?php $golf_field_desc = get_field('golf_field_desc', get_the_ID()); ?>
<?php $golf_field_image = get_field('golf_field_image', get_the_ID()); ?>
<?php $golf_field_url = get_field('golf_field_url', get_the_ID()); ?>

<div class="container clearfix">
	<div class="col_half nobottommargin ">
		<div class="heading-block" style="padding-top: 40px;">
			<h2><?php echo $golf_field_title; ?></h2>
		</div>
		<?php echo $golf_field_desc; ?>
		<?php if($golf_field_url): ?>
			<a target="_blank" href="<?php echo $golf_field_url; ?>" class="button button-border button-large button-rounded topmargin-sm noleftmargin">Más información</a>
		<?php endif; ?>
		
	</div>
	<div class="col_half nobottommargin center col_last">
		<img src="<?php echo $golf_field_image['url']; ?>" alt="<?php echo $golf_field_title; ?>" data-animate="fadeInLeft">
	</div>
	
</div>