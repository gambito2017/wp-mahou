<?php
$MetaQueryType = array(
	array(
		'key' => 'tournament_asociate',
		'value' =>  get_the_ID(),
		'compare' => 'LIKE'
	),
);
$args = array( 
	'post_type' => 'livescoring', 
	'post_status' => 'publish',
	'meta_query' => $MetaQueryType,
);

$loopLiveScoring = new WP_Query( $args );
if( $loopLiveScoring->have_posts() ){
	$loopLiveScoring->the_post();
	$live_scoring = $loopLiveScoring->posts[0]->ID;
}
wp_reset_query();
?>

<div class="product">
	<div class="col_two_fifth">
		<!--Fotos descripción torneo--
		============================================ -->
		<div class="product-image">
			<div class="fslider" data-pagi="false" data-arrows="false" data-thumbs="true">
				<div class="flexslider">
					<div class="slider-wrap" data-lightbox="gallery">
						<?php $galleryImages = get_field('image_gallery', get_the_ID()); ?>
						<?php foreach ( $galleryImages as $gallery_images ) : ?>
							<div class="slide" data-thumb="<?php echo $gallery_images['sizes']['thumb_cartel']; ?>">
								<a href="<?php echo $gallery_images['sizes']['thumb_cartel']; ?>" title="<?php echo get_the_title(); ?>" data-lightbox="gallery-item">
									<img src="<?php echo $gallery_images['sizes']['thumb_full']; ?>" alt="<?php echo get_the_title(); ?>">
								</a>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>

			<?php
			$fecha_inicio_original = get_field('fecha_inicio', get_the_ID());
			$fechaStr = strtotime($fecha_inicio_original);
			?>
	     	<div class="sale-flash"><?php echo date('Y',$fechaStr); ?></div>
	     </div><!--End Fotos descripción torneo -->

	</div>


    <div class="col_two_fifth product-desc">
		<!-- Título torneo
		============================================ -->
		<div class="fancy-title"><h3><?php echo get_the_title(); ?> (<?php echo date("d",$fechaStr); ?> <?php echo ucfirst(utf8_encode(strftime("%B",$fechaStr))); ?> <?php echo date("Y",$fechaStr); ?>)</h3></div>
		<div class="clear"></div>
		<div class="line"></div>
		<!-- Descripción campo
		============================================ -->
		<?php the_content(); ?>
		<!-- Dirección campo
		============================================= -->
		<div class="panel panel-default product-meta">
			<div class="panel-body">
				<?php $address = get_field('full_address', get_the_ID()); ?>
				<?php echo $address; ?>
			</div>
		</div><!-- Product Single - Meta End -->
		<!-- Redes sociales - Share
		============================================= -->
		<div class="si-share noborder clearfix">
			<?php $image = get_field('cartel_torneo', get_the_ID()); ?>
			<div>
				<a target="_blank" href="https://www.facebook.com/share.php?u=<?php echo get_the_permalink(); ?>&title=<?php echo urlencode(get_the_title()); ?>" class="social-icon si-borderless si-facebook">
				<i class="icon-facebook"></i>
				<i class="icon-facebook"></i>
				</a>
				<a target="_blank" href="https://twitter.com/intent/tweet?url=<?php echo get_the_permalink(); ?>&status=<?php echo urlencode(get_the_title()); ?>+<?php echo get_the_permalink(); ?>" class="social-icon si-borderless si-twitter">
				<i class="icon-twitter"></i>
				<i class="icon-twitter"></i>
				</a>
				<a data-pin-do="buttonPin" href="https://www.pinterest.com/pin/create/button/?url=<?php echo get_the_permalink(); ?>&media=<?php echo $image['sizes']['thumb_cartel']; ?>&description=<?php echo urlencode(get_the_title()); ?>" data-pin-custom="true" class="social-icon si-borderless si-pinterest">
				<i class="icon-pinterest"></i>
				<i class="icon-pinterest"></i>
				</a>
				<a href="https://plus.google.com/share?url=<?php echo get_the_permalink(); ?>" class="social-icon si-borderless si-gplus">
				<i class="icon-gplus"></i>
				<i class="icon-gplus"></i>
				</a>
				<!--<a href="#" class="social-icon si-borderless si-rss">
				<i class="icon-rss"></i>
				<i class="icon-rss"></i>
				</a>
				<a href="#" class="social-icon si-borderless si-email3">
				<i class="icon-email3"></i>
				<i class="icon-email3"></i>
				</a>-->
			</div>
		</div><!-- Product Single - Share End -->
	</div>


    <div class="col_one_fifth col_last">
    	<?php $players = get_field('sign_players', get_the_ID()); ?>
    	<?php if($players): ?>
		<div class="feature-box fbox-plain fbox-dark fbox-small">
			<div class="fbox-icon">
			<i class="icon-users"></i>
			</div>
			<h3>Participantes</h3>
			<p class="notopmargin"><?php echo (($players)) ? count($players) : '0'; ?> participante/s inscrito/s</p>
		</div>
		<?php endif; ?>

		<?php $premios = get_field('premios', $post->id); ?>
		<?php if($premios): ?>
			<div class="feature-box fbox-plain fbox-dark fbox-small">
				<div class="fbox-icon">
				<i class="icon-credit-cards"></i>
				</div>
				<h3>Premios</h3>
				<p class="notopmargin"><?php echo $premios; ?></p>
			</div>
		<?php endif; ?>

		<?php if($live_scoring): ?>
			<div class="feature-box fbox-plain fbox-dark fbox-small">
				<div class="fbox-icon">
				<i class="icon-bell"></i>
				</div>
				<h3>Resultados</h3>
				<p class="notopmargin">Siga el live-scoring del torneo</p>
			</div>
		<?php endif; ?>

		<?php $calendar = get_field('tournament_calendar', get_the_ID()); ?>
		<?php if($calendar): ?>
			<div class="feature-box fbox-plain fbox-dark fbox-small">
				<div class="fbox-icon">
				<i class="icon-calendar"></i>
				</div>
				<h3><?php echo count($calendar); ?> días de competición</h3>
				<p class="notopmargin">Ver todo el programa del torneo</p>
			</div>
		<?php endif; ?>

	</div>
</div>

<?php if($calendar): ?>

	<div class="col_full">
		<div class="tabs clearfix nobottommargin" id="tab-1">
			<ul class="tab-nav clearfix">
				<?php foreach ( $calendar as $journeys => $j ) : ?>
					<?php
					$journey_date = strtotime($j['journey_date']);
					?>
					<li>
						<a href="#tabs-<?php echo $journeys; ?>">
							<i class="icon-calendar"></i>
							<span class="hidden-xs"> <?php echo ($journeys+1).'ª Jornada: ';?> <?php echo ucfirst(utf8_encode(strftime("%A",$journey_date)));?></span>
						</a>
					</li>
	            <?php endforeach; ?>
			</ul>
	    
			<div class="tab-container">
				<?php foreach ( $calendar as $journeys => $j ) : ?>
					<?php
					$journey_date = strtotime($j['journey_date']);
					?>
					<div class="tab-content clearfix" id="tabs-<?php echo $journeys; ?>">
						<div class="panel panel-default events-meta">
							<div class="panel-heading">
								<h3 class="panel-title"><i class="icon-calendar3"></i> <?php echo utf8_encode(strftime("%d %B, %Y",$journey_date));?></h3>
							</div>
							<div class="panel-body">
								<ul class="iconlist nobottommargin">
									<li><h2><?php echo $j['journey_title']; ?></h2></li>
								</ul>
							</div>
		                    <div class="panel-body">
		                    	<?php echo $j['journey_text']; ?>
							</div>
						</div>
					</div>
	            <?php endforeach; ?>
			</div>
		</div>
	</div>
<?php endif; ?>

<?php //get_template_part( 'template-parts/torneo/block', 'gallery' ); ?>


<?php //$patrocinadores = get_field('patrocinadores', get_the_ID()); ?>

<?php //if($patrocinadores): ?>
	<!--<div class="clear"></div>
	<div class="fancy-title title-border title-center topmargin-sm">
		<h4>Patrocinadores</h4>
	</div>
	<ul class="clients-grid grid-6 nobottommargin clearfix">
		<?php foreach ( $patrocinadores as $patr ) : ?>
			<li><img src="<?php echo $patr['sizes']['big_sponsor']; ?>" alt="Clients"></li>
		<?php endforeach; ?>
	</ul>-->
<?php //endif; ?>