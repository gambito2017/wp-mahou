<div class="content-wrap">
	<div class="container clearfix">
		<div class="heading-block">
			<h2>Reparto de premios</h2>
			
		</div>
		<div class="table-responsive">
			<table class="table  nobottommargin">
				<thead>
					<tr>
						<td class="table-posicion-premios">Posición</td>
						<td class="table-porcentaje-premios">Porcentaje</td>
						<td class="table-puntuacion-premios">Importe</td>
					</tr>
				</thead>
				
				<tbody>
					<?php
					$limitOp = get_option('number_prizes_total');
					$totalPrize = get_option('prize_total_quantity');
					for( $contOp = 1; $contOp <= $limitOp; $contOp++):
						$percentid = 'prize_percent_'.$contOp;
        				$getPercent = get_option($percentid);
        				$getPercent = str_replace(',','.',$getPercent);

        				$prize = (float)(($totalPrize*$getPercent)/100);
        				$prize = number_format($prize, 2, ',', '.');
						?>
						<tr>
							<td class="table-posicion"><?php echo $contOp; ?></td>
							<td class="table-nombre"><?php echo $getPercent; ?>%</td>
							<td class="table-puntuacion"><?php echo $prize; ?>€</td>
						</tr>
						<?php
					endfor;
					?>
				</tbody>
			</table>
		</div>
		<img src="<?php echo get_template_directory_uri(); ?>/assets/banners/ad.jpg" alt="Ad" class="aligncenter topmargin-lg bottommargin-lg">
		
		<?php $patrocinadores = get_field('patrocinadores', get_the_ID()); ?>

		<?php if($patrocinadores): ?>
			<div class="clear"></div>
			<div class="fancy-title title-border title-center topmargin-sm">
				<h4>Patrocinadores</h4>
			</div>
			<ul class="clients-grid grid-6 nobottommargin clearfix">
				<?php foreach ( $patrocinadores as $patr ) : ?>
					<li><img src="<?php echo $patr['sizes']['big_sponsor']; ?>" alt="Clients"></li>
				<?php endforeach; ?>
			</ul>
		<?php endif; ?>
	</div>
</div>