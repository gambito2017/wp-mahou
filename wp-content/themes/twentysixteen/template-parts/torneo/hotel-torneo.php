<?php $hotel = get_field('official_hotel', get_the_ID()); ?>
<?php $short_description = get_field('short_description',$hotel); ?>
<?php $hotel_image = get_field('hotel_image',$hotel); ?>
<?php $hotel_url = get_field('hotel_url',$hotel); ?>
<?php $content = get_post_field('post_content', $hotel); ?>
<div class="container clearfix">
	<div class="section nobg topmargin-lg nobottommargin">
		<div class="container clearfix">
			<div class="col_half nobottommargin center">
				<img src="<?php echo $hotel_image['url']; ?>" alt="<?php echo get_the_title($hotel); ?>" data-animate="fadeInLeft">
			</div>
			<div class="col_half nobottommargin col_last">
				<div class="heading-block" style="padding-top: 40px;">
					<h2><?php echo get_the_title($hotel); ?></h2>
					<span><?php echo $short_description; ?></span>
				</div>
					<?php echo $content; ?>
					<?php if($hotel_url): ?>
						<a target="_blank" href="<?php echo $hotel_url; ?>" class="button button-border button-large button-rounded topmargin-sm noleftmargin">Más información</a>
					<?php endif; ?>
			</div>
		</div>
	</div>
</div>