<?php
$MetaQueryType = array(
	array(
		'key' => 'tournament_asociate',
		'value' =>  get_the_ID(),
		'compare' => 'LIKE'
	),
);
$args = array( 
	'post_type' => 'livescoring', 
	'post_status' => 'publish',
	'meta_query' => $MetaQueryType,
);

$loopLiveScoring = new WP_Query( $args );
if( $loopLiveScoring->have_posts() ){
	$loopLiveScoring->the_post();
	$live_scoring = $loopLiveScoring->posts[0]->ID;

	$jornadas_totales = get_field('ranking_actual', $live_scoring);
	$status_live_scoring = get_field('status_live_scoring', $live_scoring);
	$clasificacion = get_field('clasificacion', $live_scoring);

	?>
	<div class="content-wrap">
		<div class="container clearfix">
			<div class="heading-block">
				<h2>Live Scoring</h2>
			</div>
			<div class="table-responsive">
				<table class="table  nobottommargin">
					<thead>
						<tr>
							<?php
							switch( count($clasificacion[0]['puntuacion_jornada']) ){
								case 1:$classJourneys = 'two-journeys';break;
								case 2:$classJourneys = 'three-journeys';break;
								case 3:$classJourneys = 'four-journeys';break;
								default:$classJourneys = 'four-journeys';break;
							}
							?>
							<td class="table-posicion">Posición</td>
							<td class="table-nombre <?php echo $classJourneys; ?>">Jugador</td>
							<td class="table-puntuacion <?php echo $classJourneys; ?>">Total</td>
							<?php foreach( $clasificacion[0]['puntuacion_jornada'] as $num_jornada => $jornada ):
								?>
								<td class="table-puntuacion <?php echo $classJourneys; ?>">J<?php echo $num_jornada+1;?></td>
								<?php
							endforeach; ?>
						</tr>
					</thead>
					<tbody>
						<?php foreach( $clasificacion as $number => $pl ): ?>
							<tr>
								<td class="table-posicion"><?php echo $pl['posicion_jugador']; ?></td>
								<td class="table-nombre <?php echo $classJourneys; ?>"><?php echo $pl['jugador']; ?></td>
								<td class="table-puntuacion <?php echo $classJourneys; ?>"><?php echo $pl['puntuacion_total']; ?></td>
								<?php foreach( $pl['puntuacion_jornada'] as $num_jornada => $jornada ):
									?>
									<td class="table-puntuacion <?php echo $classJourneys; ?>"><?php echo $jornada['puntuacion'];?></td>
									<?php
								endforeach; ?>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			</div>
			<img src="<?php echo get_template_directory_uri(); ?>/assets/banners/ad.jpg" alt="Ad" class="aligncenter topmargin-lg bottommargin-lg">
		</div>
	</div>
	<?php
}
wp_reset_query();


?>

