<?php
$image = get_field('main_image', get_the_ID());
$desc = get_field('short_description', get_the_ID());
?>

<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 noleftpadding bottommargin-sm">
	<div class="featured-news clearfix">
		<a href="<?php echo get_the_permalink(); ?>">
			<img src="<?php echo $image['sizes']['news']; ?>" alt="<?php echo get_the_title(); ?>">
		</a>
        <div class="entry-title">
			<h3><a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title(); ?></a></h3>
		</div>
		<ul class="entry-meta clearfix">
			<li><i class="icon-calendar3"></i> <?php echo get_the_date( 'd F \d\e Y', get_the_ID()); ?></li>
		</ul>
		<div class="entry-content">
			<p><?php echo $desc; ?></p>
		</div>
	</div>
</div>