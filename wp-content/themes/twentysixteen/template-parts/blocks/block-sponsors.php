<div class="clear"></div>
<div class="fancy-title title-border title-center topmargin-sm">
	<h4>Patrocinadores</h4>
</div>

<ul class="clients-grid nobottommargin clearfix">
	<li><a><img src="<?php echo get_template_directory_uri(); ?>/assets_temp/sponsors/mahou.jpg" alt="Clients"></a></li>
	<li><a><img src="<?php echo get_template_directory_uri(); ?>/assets_temp/sponsors/sanmiguel.jpg" alt="Clients"></a></li>
	<li><a><img src="<?php echo get_template_directory_uri(); ?>/assets_temp/sponsors/alhambra.jpg" alt="Clients"></a></li>
	<li><a><img src="<?php echo get_template_directory_uri(); ?>/assets_temp/sponsors/founders.jpg" alt="Clients"></a></li>
	<li><a><img src="<?php echo get_template_directory_uri(); ?>/assets_temp/sponsors/solan.jpg" alt="Clients"></a></li>

</ul>

<div class="clear"></div>