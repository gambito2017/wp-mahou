<?php
$args = array( 
  'post_type' => 'player', 
  'post_status' => 'publish',
  'nopaging' => true
);

$loopRanking = new WP_Query( $args );
$arrayRanking = array();
while ( $loopRanking->have_posts() ) : $loopRanking->the_post();
  $arrayRanking[] = array('id'=>$post->ID,'ranking'=>get_field('ranking_actual', $post->ID));
endwhile;
wp_reset_query();

usort($arrayRanking,function($a,$b){
    $a = $a['ranking'];
    $b = $b['ranking'];

    $a = str_replace('.','',$a);
    $a = str_replace('€','',$a);
    $b = str_replace('.','',$b);
    $b = str_replace('€','',$b);
    return ($a*10) - ($b*10);
});
//wp_die('<pre>'.print_r(array_reverse($arrayRanking),true).'</pre>');
$arrayRanking = array_reverse($arrayRanking);

foreach ( $arrayRanking as $keyR => $valueR ) : 
  $count = $keyR+1;
  $image = get_field('player_image', $valueR['id']);
  $nacionalidad = get_field('nacionalidad', $valueR['id']);
  $ranking_actual = get_field('ranking_actual', $valueR['id']);
?>
  
    <article class="portfolio-item ">
         <div class="portfolio-image">
              <a href="<?php echo get_the_permalink($valueR['id']); ?>">
                   <img src="<?php echo $image['sizes']['player_thumb']; ?>" alt="<?php echo get_the_title($valueR['id']); ?>">
              </a>
         </div>
         <div class="col_three_fourth_player">
              <div class="player-data">
                   <h3> <?php echo get_the_title($valueR['id']); ?></h3>            
              </div>
              <div class="player-ranking">
                   <h3> <?php echo $nacionalidad; ?></h3>              
              </div>
         </div>
         <div class="col_one_fourth_player col_last">
              <div class="player-position">
                   <strong> <?php echo $count; ?></strong>           
              </div>
              <div class="player-ranking">
                   <h2> <?php echo $ranking_actual; ?></h2>               
              </div>
         </div>
    </article>

<?php
  $count++;
endforeach;
?>