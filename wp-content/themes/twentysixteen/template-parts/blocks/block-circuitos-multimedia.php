<!-- Portfolio Items
============================================= -->
<div id="portfolio" class="portfolio grid-container  clearfix">

	<?php
	$args = array( 
		'post_type' => 'page', 
		'post_status' => 'publish',
		'orderby' => 'date',
		'order'	=> 'DESC'
	);

	$loopTorneos = new WP_Query( $args );
	while ( $loopTorneos->have_posts() ) : $loopTorneos->the_post();
		$tournament_type = get_field('tournament_type', $post->id);
		$imagen_pagina = get_field('imagen_pagina', $post->id);
		if( $tournament_type ):
	?>
			<article class="portfolio-item pf-media pf-icons">
				<div class="portfolio-image">
					<a href="<?php echo $imagen_pagina['sizes']['thumb_cartel']; ?>">
						<img src="<?php echo $imagen_pagina['sizes']['thumb_cartel']; ?>" alt="<?php echo $post->post_title; ?>">
					</a>
					<div class="portfolio-overlay">
						<a href="<?php echo $imagen_pagina['url']; ?>" class="left-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>galeria?t=<?php echo strtolower($tournament_type); ?>" class="right-icon"><i class="icon-line-ellipsis"></i></a>
					</div>
				</div>
				<div class="portfolio-desc">
					<h3><a href="<?php echo esc_url( home_url( '/' ) ); ?>galeria?t=<?php echo strtolower($tournament_type); ?>"><?php echo $post->post_title; ?></a></h3>
					<span><a href="<?php echo esc_url( home_url( '/' ) ); ?>galeria?t=<?php echo strtolower($tournament_type); ?>">Circuito <?php echo $tournament_type; ?> <?php echo date('Y'); ?></a></span>
				</div>
			</article>
	<?php
		endif;
	endwhile;
	wp_reset_query();
	?>

</div><!-- #portfolio end -->


		
		