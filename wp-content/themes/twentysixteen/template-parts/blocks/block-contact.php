<!-- Postcontent
============================================= -->
<div class="nobottommargin">

	<h3 class="color">¿Podemos ayudarte?</h3>

	<div class="contact-widget">

		<div class="contact-form-result"></div>

		<?php
		global $wp;
		$current_url = home_url(add_query_arg(array(),$wp->request));
		?>
		<form class="nobottommargin" id="template-contactform" name="template-contactform" action="<?php echo $current_url; ?>" method="post">

			<div class="form-process"></div>

			<div class="col_one_third">
				<label for="template-contactform-name">Nombre <small>*</small></label>
				<input type="text" id="template-contactform-name" name="template-contactform-name" value="" class="sm-form-control required show-error-msg" />
			</div>

			<div class="col_one_third">
				<label for="template-contactform-email">Email <small>*</small></label>
				<input type="email" id="template-contactform-email" name="template-contactform-email" value="" class="required email sm-form-control" />
			</div>

			<div class="col_one_third col_last">
				<label for="template-contactform-phone">Teléfono</label>
				<input type="text" id="template-contactform-phone" name="template-contactform-phone" value="" class="sm-form-control" />
			</div>

			<div class="clear"></div>

			<div class="col_two_third">
				<label for="template-contactform-subject">Asunto <small>*</small></label>
				<input type="text" id="template-contactform-subject" name="template-contactform-subject" value="" class="required sm-form-control" />
			</div>

			<div class="col_one_third col_last">
				<label for="template-contactform-service">Servicios</label>
				<select id="template-contactform-service" name="template-contactform-service" class="sm-form-control">
					<option value="">-- Elegir --</option>
					<option value="Torneos">Torneos</option>
					<option value="Inscripciones">Inscripciones</option>
					<option value="Publicidad">Publicidad</option>
					<option value="Otros">Otros</option>
				</select>
			</div>

			<div class="clear"></div>

			<div class="col_full">
				<label for="template-contactform-message">Mensaje <small>*</small></label>
				<textarea class="required sm-form-control" id="template-contactform-message" name="template-contactform-message" rows="6" cols="30"></textarea>
			</div>

			<div class="col_full hidden">
				<input type="text" id="template-contactform-botcheck" name="template-contactform-botcheck" value="" class="sm-form-control" />
			</div>

			<div class="col_full">
				<button class="button button-3d nomargin" type="submit" id="template-contactform-submit" name="template-contactform-submit" value="submit">Enviar</button>
			</div>

			<input type="hidden" id="type" name="type" value="contact" />
		</form>
	</div>

</div><!-- .postcontent end -->


<?php //get_template_part( 'template-parts/blocks/sidebar', 'contact' ); ?>
