<!-- Sidebar
============================================= -->
<div class="sidebar col_last nobottommargin topmargin">

	<?php
	$contact_emails = explode(',',get_option('contact_email'));
	$field_emails = '';
	foreach( $contact_emails as $emails ):
		$field_emails .= '<a href="mailto:'.$emails.'">'.$emails.'</a> ';
	endforeach;
	?>

	<address>
		<strong>Oficinas</strong><br>
		C/ Mallorca 230<br>
		08008 Barcelona<br>
	</address>
	<p><strong>Teléfono:</strong> <?php echo get_option('contact_phone');?><br>
	<strong>Email:</strong> <?php echo $field_emails; ?></p>

	

	<div class="widget noborder notoppadding">

		<a target="_blank" href="https://www.facebook.com/mahou.es" class="social-icon si-small si-dark si-facebook">
			<i class="icon-facebook"></i>
			<i class="icon-facebook"></i>
		</a>

		<a target="_blank" href="https://twitter.com/mahou_es" class="social-icon si-small si-dark si-twitter">
			<i class="icon-twitter"></i>
			<i class="icon-twitter"></i>
		</a>

		<a target="_blank" href="https://www.instagram.com/mahou_es/" class="social-icon si-small si-dark si-instagram">
			<i class="icon-instagram"></i>
			<i class="icon-instagram"></i>
		</a>

		<a target="_blank" href="https://www.youtube.com/user/MahouTV" class="social-icon si-small si-dark si-youtube">
			<i class="icon-youtube"></i>
			<i class="icon-youtube"></i>
		</a>

		

	</div>

</div><!-- .sidebar end -->
