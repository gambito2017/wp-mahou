<div class="col-md-12 bottommargin-lg clearfix">

	<div class="fancy-title title-border">
		<h3>Otras Noticias</h3>
	</div>

	<?php
	wp_reset_query();
	$args = array( 
		'post_type' => 'post', 
		'post_status' => 'publish',
		'post__not_in' => array(get_the_ID()),
		'orderby' => 'rand'
	);
	$args['posts_per_page'] = 4;

	$loopNoticias = new WP_Query( $args );
	$count = 0;
	while ( $loopNoticias->have_posts() ) : $loopNoticias->the_post();
		$image = get_field('main_image', $post->id);
		$desc = get_field('short_description', $post->id);
	?>
		<div class="col-md-3">
			<div class="ipost clearfix">
				<div class="entry-image">
					<a href="<?php echo get_the_permalink(); ?>">
						<img class="image_fade" src="<?php echo $image['sizes']['news_thumbnail']; ?>" alt="<?php echo $post->post_title; ?>">
					</a>
				</div>
				<div class="entry-title">
					<h3><a href="<?php echo get_the_permalink(); ?>"><?php echo $post->post_title; ?></a></h3>
				</div>
				<ul class="entry-meta clearfix">
					<li><i class="icon-calendar3"></i><?php echo get_the_date( 'd F Y', $post->id); ?></li>
				</ul>
			</div>
		</div>
		<?php
        $count++;
        if( $count%4 == 0 && $count > 0 ){
            echo '<div class="clear"></div>';
        }
        ?>
	<?php
	endwhile;
	wp_reset_query();
	?>

</div>