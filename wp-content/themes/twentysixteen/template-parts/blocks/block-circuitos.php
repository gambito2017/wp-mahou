<div class="col-md-12 nopadding">
	
	<div class="col_two_fifth nobottommargin">			
		<img src="<?php echo get_template_directory_uri(); ?>/assets_temp/images/circuito-home.jpg" alt="El circuito">
	</div>

	<div class="col_three_fifth nobottommargin col_last">
		<div class="heading-block">
			<h2>El circuito Mahou San Miguel</h2>
		</div>
		<p>El Circuito Mahou San Miguel Golf Club está compuesto por 25 pruebas de hasta 200 jugadores cada uno. Se disputa en 3 categorías: 1 femenina y 2 masculinas (primera y segunda) en formato a 18 hoyos Stableford individual.</p>
		<a href="<?php echo get_permalink( get_page_by_path( 'el-circuito' ) ); ?>" class="button button-3d button-rounded"><i class="icon-plus"></i>Ver todo</a>
	</div>
	<div class="clear bottommargin-lg"></div>


	<?php $image = wp_get_attachment_image( 1782, 'news_thumbnail', false, array('alt' => 'El calendario') ); ?>
	<div class="col_three_fifth nobottommargin">		
		
		<div class="heading-block">
			<h2>Calendario del circuito</h2>
		</div>
		<p>Las pruebas se disputan en los mejores campos de toda la geografía española. Consulta el calendario y súmate al Circuito Mahou San Miguel Golf Club.</p>
		<a href="<?php echo get_permalink( get_page_by_path( 'calendario' ) ); ?>" class="button button-3d button-rounded"><i class="icon-calendar"></i>Ver todo</a>
	</div>

	<div class="col_two_fifth nobottommargin col_last">
		<?php echo $image; ?>
	</div>
	<div class="clear bottommargin-lg"></div>


</div>
<div class="divider divider-center"><i class="icon-circle-blank"></i></div>