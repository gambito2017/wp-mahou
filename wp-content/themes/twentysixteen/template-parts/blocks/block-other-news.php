<?php
$MetaQueryObs = array(array('key' => 'featured', 'value' => '0', 'compare' => 'LIKE'));
$args = array( 
	'post_type' => 'post', 
	'post_status' => 'publish',
	'meta_query' => $MetaQueryObs,
	'orderby' => array( 'date' => 'DESC', 'title' => 'ASC' )
);
$args['posts_per_page'] = 8;

$loopNoticias = new WP_Query( $args );
$count = 0;

if( $loopNoticias->have_posts() ):
	?>
	<div class="divider divider-center"><i class="icon-circle-blank"></i></div>
	<div class="col-md-12 bottommargin-lg clearfix">
		<div class="fancy-title title-border">
			<h3>Otras Noticias</h3>
		</div>
		<?php
		while ( $loopNoticias->have_posts() ) : $loopNoticias->the_post();
			$image = get_field('main_image', $post->id);
			$desc = get_field('short_description', $post->id);
		?>
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
				<div class="ipost clearfix">
					<div class="entry-image">
						<a href="<?php echo get_the_permalink(); ?>">
							<img class="image_fade" src="<?php echo $image['sizes']['news_thumbnail']; ?>" alt="<?php echo $post->post_title; ?>">
						</a>
					</div>
					<div class="entry-title">
						<h3><a href="<?php echo get_the_permalink(); ?>"><?php echo $post->post_title; ?></a></h3>
					</div>
					<ul class="entry-meta clearfix">
						<li><i class="icon-calendar3"></i><?php echo get_the_date( 'd F Y', $post->id); ?></li>
					</ul>
					<div class="entry-content">
						<p><?php echo $desc; ?></p>
					</div>
				</div>
			</div>
			<?php
	        $count++;
	        if( $count%4 == 0 && $count > 0 ){
	            echo '<div class="clear"></div>';
	        }
	        ?>
		<?php
		endwhile;
		wp_reset_query();
		?>
	</div>
<?php
endif;
?>
