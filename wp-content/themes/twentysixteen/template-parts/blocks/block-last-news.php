<div class="fancy-title title-border topmargin-sm">
	<h3>Últimas noticias</h3>
</div>
<div class="col-md-12 nopadding">
	
	<?php
	$args = array( 
		'post_type' => 'post', 
		'post_status' => 'publish',
		'orderby' => array( 'date' => 'DESC', 'title' => 'ASC' )
	);

	if ( ! is_front_page() ) :
		if( get_field('show_news_number', $post->ID ) > 0 ):

			$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;

			$args['paged'] = $paged;
			$args['posts_per_page'] = get_field('show_news_number', $post->ID );
		else:
			$args['nopaging'] = true;
		endif;
	else:
		$MetaQueryObs = array(array('key' => 'featured', 'value' => '1', 'compare' => 'LIKE'));
		$args['meta_query'] = $MetaQueryObs;
		$args['posts_per_page'] = 6;
	endif;


	$loopNoticias = new WP_Query( $args );
	$count = 0;
	while ( $loopNoticias->have_posts() ) : $loopNoticias->the_post();
		$image = get_field('main_image', $post->id);
		$desc = get_field('short_description', $post->id);
	?>
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 noleftpadding bottommargin-sm">
			<div class="featured-news clearfix">
				<a href="<?php echo get_the_permalink(); ?>">
					<img src="<?php echo $image['sizes']['news']; ?>" alt="<?php echo $post->post_title; ?>">
				</a>
		        <div class="entry-title">
					<h3><a href="<?php echo get_the_permalink(); ?>"><?php echo $post->post_title; ?></a></h3>
				</div>
				<ul class="entry-meta clearfix">
					<li><i class="icon-calendar3"></i> <?php echo get_the_date( 'd F Y', $post->id); ?></li>
				</ul>
				<div class="entry-content">
					<p><?php echo $desc; ?></p>
				</div>
			</div>
		</div>
		<?php
        $count++;
        if( $count%3 == 0 && $count > 0 ){
            echo '<div class="clear"></div>';
        }
        ?>
	<?php
	endwhile;
	
	?>

	<?php
	/* ------------------------------------------------------------------*/
	/* PAGINATION */
	/* ------------------------------------------------------------------*/

	//paste this where the pagination must appear
	$total = $loopNoticias->max_num_pages;
	// only bother with the rest if we have more than 1 page!
	if ( $total > 1 )  {
	    // get the current page
	    if ( !$current_page = get_query_var('paged') )
	          $current_page = 1;
	    
		echo '<div class="center">';
		    echo '<div class="pagination-list">';
			    echo paginate_links(array(
			        'base'     => get_pagenum_link(1) . '%_%',
			        'format'   => '?paged=%#%',
			        'current'  => $current_page,
			        'total'    => $total,
			        'mid_size' => 4,
			        'type'     => 'plain'
			    ));
			echo '</div>';
		echo '</div>';
	}
	wp_reset_query();
	?>

</div>
