<h3>Próximos Torneos</h3>
<div class="divider divider-center"><i class="icon-circle-blank"></i></div>

<div id="posts" class="events small-thumbs">
	<?php $type = get_field('tournament_type', $post->ID ); ?>

	<?php
	$MetaQueryType = array(
		array(
			'key' => 'tournament_type',
			'value' => $type,
			'compare' => 'LIKE'
		),
		array(
			'key' => 'fecha_inicio',
			'value' => date('Ymd',strtotime("-1 week")),
			'compare' => '>='
		),
	);
	$args = array( 
		'post_type' => 'torneo', 
		'post_status' => 'publish',
		'meta_query' => $MetaQueryType,
		'meta_key' => 'fecha_inicio',
		'orderby' => 'meta_value',
		'order'	=> 'ASC'
	);
	$args['nopaging'] = true;


	$loopTorneos = new WP_Query( $args );
	while ( $loopTorneos->have_posts() ) : $loopTorneos->the_post();
		$status = get_field('status_torneo', $post->id);
		$image = get_field('cartel_torneo', $post->id);
		$background = get_field('imagen_fondo', $post->id);
		$desc = get_field('short_description', $post->id);
		$type = get_field('tournament_type', $post->id);
		$rango_fecha = get_field('fecha_torneo', $post->id);
		$localizacion = get_field('localizacion', $post->id);
		$fecha_inicio_original = get_field('fecha_inicio', $post->id);
		$fechaStr = strtotime($fecha_inicio_original);

		$day = date('d',$fechaStr);
		$month = ucfirst(strftime("%h",$fechaStr));
	?>

		<div class="torneo clearfix" style="background-image: url(<?php echo $background['sizes']['background_tournament']; ?>);">
			<div class="entry-image">
				<?php if( $status ): ?>
					<a href="<?php echo get_the_permalink(); ?>">
						<img src="<?php echo $image['sizes']['thumb_cartel']; ?>" alt="<?php echo $post->post_title; ?>">
						<div class="entry-date"><?php echo $day; ?><span><?php echo $month; ?></span></div>
					</a>
				<?php else: ?>
					<img src="<?php echo $image['sizes']['thumb_cartel']; ?>" alt="<?php echo $post->post_title; ?>">
					<div class="entry-date"><?php echo $day; ?><span><?php echo $month; ?></span></div>
				<?php endif; ?>
				
			</div>
			<div class="col_two_fifth clearfix" style="background-color:rgba(224,133,65,0.8)">
				<div class="entry-title">
					<?php if( $status ): ?>
						<h2><a href="<?php echo get_the_permalink(); ?>"><?php echo $post->post_title; ?></a></h2>
					<?php else: ?>
						<h2><?php echo $post->post_title; ?></h2>
					<?php endif; ?>
				</div>
					<ul class="entry-meta clearfix">
						<li><span class="label label-warning" style="background:#000;"><?php echo $type; ?></span></li>
						<li><span><i class="icon-time"></i> <?php echo $rango_fecha; ?></span></li>
						<li><span><i class="icon-map-marker2"></i> <?php echo $localizacion; ?></span></li>
					</ul>
				<div class="entry-content">
					<p style="color:#fff;"><?php echo $desc; ?></p>
					<?php if( $status ): ?>
						<a href="<?php echo get_the_permalink(); ?>" class="btn btn-danger">Ver Torneo</a>
					<?php else: ?>
						<span class="btn btn-danger">Próximamente</span>
					<?php endif; ?>
				</div>
			</div>
		</div>

	<?php
	endwhile;

	?>
	
	
</div>

<?php
$MetaQueryType = array(array('key' => 'tournament_type', 'value' => $type, 'compare' => 'LIKE'));
$args = array( 
	'post_type' => 'torneo', 
	'post_status' => 'publish',
	'meta_query' => $MetaQueryType,
	'meta_key' => 'fecha_inicio',
	'orderby' => 'meta_value',
	'order'	=> 'ASC'
);
$args['nopaging'] = true;

$loopTorneos = new WP_Query( $args );
$auxEvents = [];
while ( $loopTorneos->have_posts() ) : $loopTorneos->the_post();
	$status = get_field('status_torneo', $post->id);
	$fecha_inicio_original = get_field('fecha_inicio', $post->id);
	$fecha_fin = get_field('fecha_fin', $post->id);

	if( $fecha_fin ){

		$begin = new DateTime( date('Y-m-d',strtotime($fecha_inicio_original)) );
		$end = new DateTime( date('Y-m-d',strtotime($fecha_fin)) );
		$end = $end->modify( '+1 day' );

		$interval = DateInterval::createFromDateString('1 day');
		$period = new DatePeriod($begin, $interval, $end);

		foreach ( $period as $dt ){
			$fechaCanvas = $dt->format( 'm-d-Y' );
			if( $status ) $auxEvents[$fechaCanvas][] = '<a class="'.strtolower($type).'" href="'.get_the_permalink().'">'.$post->post_title.'</a>';
			else $auxEvents[$fechaCanvas][] = '<span class="'.strtolower($type).'">'.$post->post_title.'</span>';
		}
	}
	else{
		$fechaCanvas = date('m-d-Y',strtotime($fecha_inicio_original));
		if( $status ) $auxEvents[$fechaCanvas][] = '<a class="'.strtolower($type).'" href="'.get_the_permalink().'">'.$post->post_title.'</a>';
		else $auxEvents[$fechaCanvas][] = '<span class="'.strtolower($type).'">'.$post->post_title.'</span>';
	}

endwhile;
wp_reset_query();

$canvasEvents = [];
foreach ( $auxEvents as $fechaEv => $listEv ) {
	$auxString = '';
	foreach( $listEv as $keyList => $itemList ){
		$auxString .= $itemList.' ';
	}
	$canvasEvents[$fechaEv] = $auxString;
}
?>
<script>
var canvasEvents = <?php echo json_encode($canvasEvents); ?>;
</script>

<?php get_template_part( 'template-parts/blocks/block', 'last-tournaments' ); ?>

<?php if($type != 'Mahou' ): ?>
	<?php get_template_part( 'template-parts/blocks/block', 'sponsors-big' ); ?>
    <?php get_template_part( 'template-parts/blocks/block', 'sponsors' ); ?>
<?php endif; ?>