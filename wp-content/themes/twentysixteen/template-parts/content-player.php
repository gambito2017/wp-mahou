<?php
$image = get_field('player_image', get_the_ID());
$nacionalidad = get_field('nacionalidad', get_the_ID());
$ranking_actual = get_field('ranking_actual', get_the_ID());
$birth_date = get_field('birth_date', get_the_ID());
$birth_date_str = strtotime($birth_date);
$birth_place = get_field('birth_place', get_the_ID());
$player_club = get_field('player_club', get_the_ID());
$player_size = get_field('player_size', get_the_ID());
$playerID = get_the_ID();

$player_position = '';
$args = array( 
	'post_type' => 'player', 
	'post_status' => 'publish',
	'nopaging' => true,
	'meta_key' => 'ranking_actual',
	'orderby' => array(
		'ranking_actual' => 'DESC',
		'title' => 'ASC',
	)
);
$loopRanking = new WP_Query( $args );
$count = 1;
while ( $loopRanking->have_posts() ) : $loopRanking->the_post();
	if( $playerID == get_the_ID() ):
		$player_position = $count;
	endif;
	$count++;
endwhile;
wp_reset_query();
?>
<!-- Page Title
============================================= -->
<section id="page-title" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets_temp/sliders/fondo.jpg)">
	<div class="cover">
		<div class="container clearfix">
			<h1><?php echo get_the_title(); ?></h1>
			<ol class="breadcrumb">
				<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>">Home</a></li>
				<li class="active"><?php echo get_the_title(); ?></li>
			</ol>
		</div>
	</div>
</section><!-- #page-title end -->

<!-- Content
============================================= -->

<section id="content">

	<div class="content-wrap">

		<div class="container clearfix">

			<!-- Portfolio Single Image
			============================================= -->
			<div class="col_two_third portfolio-single-image nobottommargin">
				<img src="<?php echo $image['sizes']['player_detail']; ?>" alt="<?php echo get_the_title(); ?>">
			</div><!-- .portfolio-single-image end -->

			<!-- Portfolio Single Content
			============================================= -->
			<div class="col_one_third portfolio-single-content col_last nobottommargin">

				<!-- Portfolio Single - Description
				============================================= -->
				<div class="fancy-title title-bottom-border">
					<h3><?php echo get_the_title(); ?></h3>
				</div>
				<div class="table-responsive">
					<table class="table  nobottommargin">
						<thead>
							<td> Ranking</td>
							<td style="text-align:right;"> Puntos</td>
						</thead>
						<tbody>
							<tr>
								<td class="table-posicion"><?php echo $player_position; ?></td>
								<td class="table-puntuacion"><?php echo $ranking_actual; ?></td>
							</tr>
						</tbody>
					</table>
				</div>

				<?php echo get_the_content(); ?>
				<!-- Portfolio Single - Description End -->

				<div class="line"></div>

		


				<!-- Portfolio Single - Meta
				============================================= -->
				<ul class="portfolio-meta bottommargin">
					<li><span><i class="icon-map-marker"></i>Lugar de nacimiento</span> <?php echo $birth_place; ?></li>
					<li><span><i class="icon-calendar3"></i>Fecha:</span> <?php echo date('d',$birth_date_str); ?> <?php echo ucfirst(strftime("%B",$birth_date_str)); ?> <?php echo date('Y',$birth_date_str); ?></li>
					<?php if( $player_size ): ?>
						<li><span><i class="icon-user"></i>Altura</span> <?php echo $player_size; ?></li>
					<?php endif; ?>							
					<li><span><i class="icon-map"></i>Club:</span> <?php echo $player_club; ?></li>
					
				</ul>
				<!-- Portfolio Single - Meta End -->

				<!-- Portfolio Single - Share
				============================================= -->
				<div class="si-share clearfix">
					<span>Compartir:</span>
					<div>
						<a target="_blank" href="https://www.facebook.com/share.php?u=<?php echo get_the_permalink(); ?>&title=<?php echo urlencode(get_the_title()); ?>" class="social-icon si-borderless si-facebook">
						<i class="icon-facebook"></i>
						<i class="icon-facebook"></i>
						</a>
						<a target="_blank" href="https://twitter.com/intent/tweet?url=<?php echo get_the_permalink(); ?>&status=<?php echo urlencode(get_the_title()); ?>+<?php echo get_the_permalink(); ?>" class="social-icon si-borderless si-twitter">
						<i class="icon-twitter"></i>
						<i class="icon-twitter"></i>
						</a>
						<a data-pin-do="buttonPin" href="https://www.pinterest.com/pin/create/button/?url=<?php echo get_the_permalink(); ?>&media=<?php echo $image['sizes']['player_thumb']; ?>&description=<?php echo urlencode(get_the_title()); ?>" data-pin-custom="true" class="social-icon si-borderless si-pinterest">
						<i class="icon-pinterest"></i>
						<i class="icon-pinterest"></i>
						</a>
						<a href="https://plus.google.com/share?url=<?php echo get_the_permalink(); ?>" class="social-icon si-borderless si-gplus">
						<i class="icon-gplus"></i>
						<i class="icon-gplus"></i>
						</a>
						<!--<a href="#" class="social-icon si-borderless si-rss">
						<i class="icon-rss"></i>
						<i class="icon-rss"></i>
						</a>
						<a href="#" class="social-icon si-borderless si-email3">
						<i class="icon-email3"></i>
						<i class="icon-email3"></i>
						</a>-->
					</div>
				</div>
				<!-- Portfolio Single - Share End -->

			</div><!-- .portfolio-single-content end -->

			<div class="clear"></div>

			<div class="divider divider-center"><i class="icon-circle"></i></div>

			<?php //get_template_part( 'template-parts/player/block', 'statistics' ); ?>

		</div>

	</div>

</section><!-- #content end -->
	