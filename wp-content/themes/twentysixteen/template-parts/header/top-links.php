<ul>
	<li><a href="#">ES</a>
		<ul>
			<li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/flags/french.png" alt="French"> FR</a></li>
			<li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/flags/italian.png" alt="Italian"> IT</a></li>
			<li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/flags/german.png" alt="German"> DE</a></li>
		</ul>
	</li>
	<li><a href="#">Login</a>
		<div class="top-link-section">
			<form id="top-login" role="form">
				<div class="input-group" id="top-login-username">
					<span class="input-group-addon"><i class="icon-user"></i></span>
					<input type="email" class="form-control" placeholder="Email" required>
				</div>
				<div class="input-group" id="top-login-password">
					<span class="input-group-addon"><i class="icon-key"></i></span>
					<input type="password" class="form-control" placeholder="Password" required>
				</div>
				<label class="checkbox">
				  <input type="checkbox" value="remember-me"> Recordarme
				</label>
				<button class="btn btn-danger btn-block" type="submit">Entrar</button>
			</form>
		</div>
	</li>
</ul>