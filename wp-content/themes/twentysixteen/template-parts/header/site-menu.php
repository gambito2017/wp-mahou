<!-- Header
============================================= -->
<header id="header">

	<div id="header-wrap">

		<div class="container clearfix">

			<div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

			<!-- Logo
			============================================= -->
			<div id="logo">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="standard-logo" data-dark-logo="<?php echo get_option('standard_logo');?>"><img src="<?php echo get_option('standard_logo');?>" alt="<?php bloginfo( 'name' ); ?>"></a>
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="retina-logo" data-dark-logo="<?php echo get_option('retina_logo');?>"><img src="<?php echo get_option('retina_logo');?>" alt="<?php bloginfo( 'name' ); ?>"></a>
			</div>
            <!-- #logo end -->

			<!-- Primary Navigation
			============================================= -->
			<nav id="primary-menu" class="sub-title">
				<?php 
				if ( has_nav_menu( 'primary' ) ) : ?>
					<?php
						wp_nav_menu( array(
							'theme_location' => 'primary',
							'menu_class'     => '',
							'depth'          => 1,
							'container'          => false,
							//'link_before'    => '<span class="screen-reader-text">',
							//'link_after'     => '</span>',
						) );
					?>
				<?php endif; ?>

				<!-- Top Search
				============================================= -->
				<?php //get_template_part( 'template-parts/header/top', 'search' ); ?>

			</nav><!-- #primary-menu end -->

		</div>

	</div>

</header><!-- #header end -->