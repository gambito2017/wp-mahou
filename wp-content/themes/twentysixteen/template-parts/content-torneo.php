<?php
/**
 * The template part for displaying single posts
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
setlocale(LC_ALL,"es_ES@euro","es_ES","esp");
?>
<?php $calendar = get_field('tournament_calendar', get_the_ID()); ?>
<?php //die('<pre>'.print_r($calendar,true).'</pre>'); ?>
<?php $background = get_field('imagen_fondo', get_the_ID()); ?>
<?php $hotel = get_field('official_hotel', get_the_ID()); ?>
<?php $campo = get_field('golf_field_title', get_the_ID()); ?>
<?php $tournament_type = get_field('tournament_type', get_the_ID()); ?>
<?php $orden_salida = false; ?>
<?php $players = get_field('sign_players', get_the_ID()); ?>
<?php $awards = (get_option('number_prizes_total') && get_option('prize_total_quantity')) ? true : false; ?>

<?php
$MetaQueryType = array(
	array(
		'key' => 'tournament_asociate',
		'value' =>  get_the_ID(),
		'compare' => 'LIKE'
	),
);
$args = array( 
	'post_type' => 'livescoring', 
	'post_status' => 'publish',
	'meta_query' => $MetaQueryType,
);

$loopLiveScoring = new WP_Query( $args );
if( $loopLiveScoring->have_posts() ){
	$loopLiveScoring->the_post();
	$live_scoring = $loopLiveScoring->posts[0]->ID;
}
wp_reset_query();
?>

<!-- Page Title
============================================= -->
<section id="page-title" style="background-image:url(<?php echo $background['sizes']['background_tournament']; ?>)">
	<div class="cover">
		<div class="container clearfix">
			<h1><?php echo get_the_title(); ?></h1>
			<ol class="breadcrumb">
				<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>">Home</a></li>
				<li class="active"><?php echo get_the_title(); ?></li>
			</ol>
		</div>
	</div>
</section><!-- #page-title end -->

<!-- Content
============================================= -->
<div class="tabs clearfix" id="tab-3">


	<div class="container tab-container">

		<ul class="tab-nav tab-nav2 clearfix">
			<li><a href="#tabs-9"><i class="icon-home2 norightmargin"></i></a></li>
			<?php if( $players ): ?>
				<li><a href="#tabs-10">Participantes</a></li>
			<?php endif; ?>

			<?php if( $tournament_type == 'Profesionales' && $awards ): ?>
				<li><a href="#tabs-11">Premios</a></li>
			<?php endif; ?>

			<?php if( $orden_salida ): ?>
				<li><a href="#tabs-12">Orden de salida</a></li>
			<?php endif; ?>

			<?php if( $live_scoring ): ?>
				<li><a href="#tabs-14">Live Scoring</a></li>
			<?php endif; ?>
		</ul>

		<div class="tab-content clearfix" id="tabs-9">
			<section id="content">
				<div class="content-wrap">
					<div class="container clearfix">
						<div class="single-product">
							<?php get_template_part( 'template-parts/torneo/info', 'torneo' ); ?>
						</div>
						<?php if($hotel) get_template_part( 'template-parts/torneo/hotel', 'torneo' ); ?>
						<?php if($campo) get_template_part( 'template-parts/torneo/campo', 'torneo' ); ?>

						<?php if($tournament_type != 'Mahou' ): ?>
							<?php get_template_part( 'template-parts/blocks/block', 'sponsors-big' ); ?>
    						<?php get_template_part( 'template-parts/blocks/block', 'sponsors' ); ?>
						<?php endif; ?>

					</div>
					<div class="clear"></div>
				</div>    
			</section>
		</div>
		<div class="tab-content clearfix" id="tabs-10">
			<?php if( $players ) get_template_part( 'template-parts/torneo/participantes', 'torneo' ); ?>
		</div>
		<div class="tab-content clearfix" id="tabs-11">
			<?php if( $tournament_type == 'Profesionales' && $awards ) get_template_part( 'template-parts/torneo/premios', 'torneo' ); ?>
		</div>
		<div class="tab-content clearfix" id="tabs-12">
			<?php if( $orden_salida ) get_template_part( 'template-parts/torneo/orden', 'salida' ); ?>
		</div>

		<div class="tab-content clearfix" id="tabs-14">
			<?php if( $live_scoring ) get_template_part( 'template-parts/torneo/live', 'scoring' ); ?>
		</div>
	
	</div>
	<div class="clear"></div>
	<?php get_template_part( 'template-parts/blocks/block', 'sponsors' ); ?>
	<div class="clear"></div>
	<div class="line"></div>
	<?php wp_reset_query(); ?>

	
</div>
<!-- #content end -->




<script type="text/javascript">
	jQuery(document).ready( function($){

		var eventStartDate = new Date();
		eventStartDate = new Date(2017, 2, 27);
		$('#event-countdown').countdown({until: eventStartDate});

	});

	jQuery('#event-location').gMap({
		address: 'Ibiza, Spain',
		maptype: 'ROADMAP',
		zoom: 15,
		markers: [
			{
				address: "Ibiza, Spain"
			}
		],
		doubleclickzoom: false,
		controls: {
			panControl: true,
			zoomControl: true,
			mapTypeControl: true,
			scaleControl: false,
			streetViewControl: false,
			overviewMapControl: false
		}
	});

</script>