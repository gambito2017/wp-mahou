<?php

/**

 * The template for displaying archive pages

 *

 * Used to display archive-type pages if nothing more specific matches a query.

 * For example, puts together date-based pages if no date.php file exists.

 *

 * If you'd like to further customize these archive views, you may create a

 * new template file for each one. For example, tag.php (Tag archives),

 * category.php (Category archives), author.php (Author archives), etc.

 *

 * @link https://codex.wordpress.org/Template_Hierarchy

 *

 * @package WordPress

 * @subpackage Twenty_Sixteen

 * @since Twenty Sixteen 1.0

 */



get_header(); ?>





		<?php if ( have_posts() ) : ?>

          <div class="content-wrap">

           

              <div class="container">

              <?php

                the_archive_title( '<h1 class="page-title topmargin-sm">', '</h1>' );

                the_archive_description( '<div class="taxonomy-description">', '</div>' );

              ?>

              <?php

              // Start the Loop.

              while ( have_posts() ) : the_post();

                  ?>
                        <?php

                				/*

                				 * Include the Post-Format-specific template for the content.

                				 * If you want to override this in a child theme, then include a file

                				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.

                				 */

                				get_template_part( 'template-parts/summary', get_post_format() );


                      // End the loop.

                endwhile;
                        ?>

                <div class="clear"></div>

             </div>

        </div>

        <?php



		endif;

		?>



<?php get_footer(); ?>

