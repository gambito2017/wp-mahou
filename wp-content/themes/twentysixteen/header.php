<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<?php wp_head(); ?>
</head>

<body <?php body_class('stretched no-transition'); ?>>
	<div id="wrapper" class="clearfix">

		<?php get_template_part( 'template-parts/header/top', 'bar' ); ?>
		
		<?php get_template_part( 'template-parts/header/site', 'menu' ); ?>

		<?php if( is_front_page() ) putRevSlider("mahouhome"); ?>

		<?php if( is_page_template('templates/circuito-template.php') ) get_template_part( 'template-parts/sections/section', 'slider-circuito' ); ?>
		<?php if( is_page_template('templates/reglamento-template.php') ) get_template_part( 'template-parts/sections/section', 'slider-reglamento' ); ?>
		<section id="content">